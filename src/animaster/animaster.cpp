/*
 * app.cpp
 *
 *  Created on: 16 Oct 2016
 *      Author: MikoKuta
 */
#include <gl/GL.h>
#include <cstdlib>
#include <cstdio>
#include <cerrno>
#include <cmath>
#include "animaster.h"
#include "animaster_ui.h"
#include "resources/am_texture_manager.h"
#include "resources/am_model_manager.h"
#include "resources/am_model.h"

#include "../imgui/imgui.h"
#include "../pugixml/pugixml.hpp"
#include "../shared/camera.h"
#include "../shared/types.h"
#include "../shared/utils.h"
#include "../shared/memory_manager.h"
#include "../shared/platform_utils.h"
#include "../shared/profiler.h"
#include "../shared/safe_string.h"
#include "../math/vec3f.h"
#include "../math/quaternionf.h"
#include "../math/matrix4x4f.h"
#include "../math/vec_math_utils.h"

#include <IL/il.h>
#include <IL/ilut.h>

//Compilation unit
#include "animaster_ui.cpp"
#include "resources/am_texture_manager.cpp"
#include "resources/am_model.cpp"
#include "resources/am_model_manager.cpp"

/*
 * What is Animaster:
 * 	Animaster is a simple animation library with fixed memory footprint, that allows the user to easily
 * 	load and animate their characters at runtime. It supports Animation Trees, IK, Animation events and
 * 	ragdoll physics to animation allowing limp limbs, as well as physical animation.
 *
 * 	Animaster also comes with its own tool that allows configuration of the animations, such as creating
 * 	the animation tree, attaching events and setting up the IK and physics.
 *
 * TODO:
 * Animaster: (Expected duration: 42 hours)
 * 	-Available Node types
 * 		-Animation clip (15 minutes design, 1 hour implementation, longer impl because first)
 * 		-2 Blend nodes (15 minutes design, 30 minute implementation)
 * 		-Directional blenders ->a series of linear blends as if it were a circle (pretty much a simplified way of having a series of linear blends) (15 minutes design, 1 hour implementation)
 * 		-Triangle blend node (15 minutes design, 15 minute implementation)
 * 		-Quad blenders ->4 nodes, 3 blends, A-B C-D and then AB-CD (15 minutes design, 15 minute implementation)
 * 		-Graph nodes ->a 2d triangular blend mesh, give a position, and it will return a blend of 3 nodes according to that position. In theory can have loads of nodes (1 hours design, 2 hours implementation)
 * 	-Synchronization of different animations -> separate timeline control? (3 hours design, 1 hour implementation)
 * 	-IK (30 min design, 3 hours implementation)
 * 		-newtonian IK (actually, all my variations on IK)
 * 		-CCD
 * 		-analytical solver for simple IK
 * 		-Pose Ik solver, kinda like Mech Warrior did ->might have issues, but works well for aiming entire char
 * 	-Not updating nodes that are inactive. (1 hour design, 1 hour implementation)
 * 	-Choice of using local or global transforms (3 hours design, 1 hour implementation)
 * 	-Control the anim tree using a behaviour graph (4 hours design, 2 hours implementation)
 * 		-both automatic and manual transitions
 * 	-Simple ragdoll (2 hours design, 3 hours implementation)
 * 		-First: basic newtonian physics with joint limits
 * 		-Second: Bullet physics integration
 * 	-Ragdoll+anim blend (apply forces to the ragdoll based on animations) (2 hour design, 1.5 hour implementation)
 * 	-jigglebones (1 hour design, 30 min implementation)
 *	-Import animation tree from XML (1 hour design, 2 hours implementation)
 *	-Events (2 hour design, 1 hours implementation)
 * 		-Setup in XML file
 * 		-enable/disable events based on blend weights
 *
 * 	Demo app (Expected duration after Animaster finished: 22.5 hours):
 * 		-Dinosaur or dragon character, (rig setup: 2.5 Hours, animations total: 12.5 Hours)
 * 			-idle (0.5 Hours)
 * 			-walk NSWE (3.5 Hours)
 * 			-walk turn 90deg L/R + 180deg (2.5 Hours)
 * 			-Sprint N (1.5 Hour)
 * 			-Eat (1 hour)
 * 			-Jump (jump, mid air and land anim) (1.5 Hours)
 * 			-Attack combo (3 attacks) (2 hours)
 * 		-Demonstrate smooth transition between different walking directions and rotations (15 minutes to setup, 1 hour to debug)
 * 		-Foot placement IK (15 minutes to setup, 2 hours to debug)
 * 		-Animation events (15 minutes to setup, 1 hour to debug)
 * 			-Eating
 * 			-Foot placement sound (will we have sound?)
 * 			-Attack events
 * 		-Show ability to go partially ragdoll (get tail pinned, go completely ragdoll but not completely limp) (1 hour to setup, 4 hours to debug)
 *
 * 	Animaster Tool:
 * 		-Load a model and animation files
 * 		-Preview animations
 * 		-View and select individual bones
 * 		-Attach events to various animations
 * 		-Setup IK
 * 		-Create and modify multiple animation trees
 * 		-Have subgroups of animation trees to help with organization
 * 		-Make space work just like in blender
 * 		-Have a play-mode (in this case the animation demo)
 * 			-Ability to enter into a demo-mode, which is basically an external DLL that gets loaded
 * 				and which uses the animation tree just created. the external DLL can chose if it handles
 * 				changing the tree in real time or not.
 */

/*
 * Problems to solve:
 * 	-we need to be able to synchronize animations, but also be able to blend unsynchronized ones
 *  	-should we keep timelines separate from blend tree?
 *  		-if yes: what about blend weights affecting animation speed? Would be very complicated to set up
 *  		-If no: how do we synchronize separate branches of the tree? eg, how do we synchronize ducking and walking branch?
 *	-how to best organize the animation tree in memory?
 */

/*
 * Current TODO:
 * -Create animation tree:
 * 		-Can be modified at runtime -> Done
 * 		-Constant memory usage if animation tree not modified -> Done
 * 		-can be reused for multiple characters
 * 			-Store state in a separate object
 * 		-Optimize
 * 			-Only update bones affected by the current anim node
 * 			-Only update anim nodes howe ratio isn't 0
 * 		-Features
 * 			-Bone Masks (blueprint version)
 * 			-Synchronization groups
 */

/*
 * Utilities
 */

struct temp_memory_section
{
	uint64 oldMemoryUsed;
	stack_memory_manager* tempMemory;

	temp_memory_section(stack_memory_manager* memory)
	{
		tempMemory = memory;
		oldMemoryUsed = memory->memoryUsed;
	}

	~temp_memory_section()
	{
		tempMemory->memoryUsed = oldMemoryUsed;
	}
};

#define TEMP_MEMORY_BLOCK(memory) temp_memory_section APPEND_PROTECTED(temp_memory_section_store,__COUNTER__)(memory)

#define PROFILE_BLOCK(name) profiler_block APPEND_PROTECTED(temp_profiler_block,__COUNTER__)(&sProfiler,name)

profiler_state sProfiler;

/*
 * Animation Blueprint
 */

enum AnimationNodeType
{
	AnimationNodeType_None = 0,
	AnimationNodeType_Pose,
	AnimationNodeType_Clip,
	AnimationNodeType_Blender2,
	AnimationNodeType_Blender3,
	AnimationNodeType_Count,
};

const char *AnimationNodeTypeNames[AnimationNodeType_Count] =
		{
				"None",
				"Pose",
				"Clip",
				"Blend 2",
				"Blend 3"
		};

typedef uint32 atbNodeHandle; //animation tree node handle
typedef uint32 atbSyncGroupHandle; //animation tree sync group handle
struct animation_tree_blueprint;

struct pose_node_blueprint
{
	static const AnimationNodeType type = AnimationNodeType_Pose;
	static const char* getDefaultName()
	{
		return "pose";
	}

	int32 selectedClip;
	atbSyncGroupHandle syncGroup;

	void reset()
	{
		selectedClip = -1;
		syncGroup = 0;
	}

	int getNodeInputCount()
	{
		return 0;
	}

	atbNodeHandle* getInputs()
	{
		return NULL;
	}

	static void displayUI(pose_node_blueprint* node, atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model);

	static void processNode(am_transform_buffer* dst, const pose_node_blueprint* node, atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model, am_transform_buffer* resultBuffers,
			stack_memory_manager* tempMemory);
};

struct clip_node_blueprint
{
	static const AnimationNodeType type = AnimationNodeType_Clip;
	static const char* getDefaultName()
	{
		return "clip";
	}

	int32 selectedClip;
	real32 animationTime;
	atbSyncGroupHandle syncGroup;

	void reset()
	{
		selectedClip = -1;
		animationTime = 0.0f;
		syncGroup = 0;
	}

	int getNodeInputCount()
	{
		return 0;
	}

	atbNodeHandle* getInputs()
	{
		return NULL;
	}

	static void displayUI(clip_node_blueprint* node, atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model);

	static void processNode(am_transform_buffer* dst, const clip_node_blueprint* node, atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model, am_transform_buffer* resultBuffers,
			stack_memory_manager* tempMemory);
};

struct blend2_node_blueprint
{
	static const AnimationNodeType type = AnimationNodeType_Blender2;
	static const char* getDefaultName()
	{
		return "blend2";
	}

	atbNodeHandle input[2];
	real32 ratio;

	void reset()
	{
		input[0] = 0;
		input[1] = 0;
		ratio = 0.0f;
	}

	int getNodeInputCount()
	{
		return 2;
	}

	atbNodeHandle* getInputs()
	{
		return input;
	}

	static void displayUI(blend2_node_blueprint* node, atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model);

	static void processNode(am_transform_buffer* dst, const blend2_node_blueprint* node, atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model, am_transform_buffer* resultBuffers,
			stack_memory_manager* tempMemory);
};

struct blend3_node_blueprint
{
	static const AnimationNodeType type = AnimationNodeType_Blender3;
	static const char* getDefaultName()
	{
		return "blend3";
	}

	atbNodeHandle input[3];
	vec2f uv;
	void reset()
	{
		input[0] = 0;
		input[1] = 0;
		input[2] = 0;
		uv.set(0.0f, 0.0f);
	}

	int getNodeInputCount()
	{
		return 3;
	}

	atbNodeHandle* getInputs()
	{
		return input;
	}

	static void displayUI(blend3_node_blueprint* node, atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model);

	static void processNode(am_transform_buffer* dst, const blend3_node_blueprint* node, atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model, am_transform_buffer* resultBuffers,
			stack_memory_manager* tempMemory);
};

struct sync_group_blueprint
{
	float animRatio;
	float totalAnimLength; //used to calculate
	float totalNodeRatio; //total ratios of all nodes that contributed to this.
};

struct animation_tree_blueprint
{
	/*
	 * Animation tree blueprint, is basically an animation tree
	 * optimised for ease of use rather than performance.
	 * The final, performance and size optimized animation tree
	 * should be used in the game
	 * */

	uint32 maxAnimNodeCount;
	uint32 nodeCount;

	atbNodeHandle outputNodeHandle;

	am_name *nodeNames;
	AnimationNodeType *nodeTypes;
	uint32 *nodeDataSize;
	void** nodeDatas; //note: if a node is removed, move all subsequent nodes down, to free space at the top
	int16* nodeIndices;

	real32 *nodeRatios;

	atbNodeHandle* nodeHandleList;

	uint8* nodeMemory; //note: all nodes are on 16 byte aligned margin, so we can move them when needed
	uint64 nodeMemoryUsed;
	uint64 nodeMemorySize;

	//Synch groups synchronise several animations according to their weights on the final pose
	//If an animation clip has no synch group assigned, it's gonna get updated separately
	uint32 maxSyncGroupCount;
	uint32 syncGroupCount;
	am_name *syncGroupNames;
	sync_group_blueprint* syncGroups;
	int16* syncGroupIndices;
	atbSyncGroupHandle* syncGroupHandleList;

	struct node_widget
	{
		vec2f pos;
		vec2f size;
		uint32 inputCount;
		atbNodeHandle* inputs;

		vec2f outputPos;
	};
	struct
	{
		node_widget *nodeWidgets;	//note: each widget is associated to the node with the same handle/index
		vec2f lastCreatedPosition;

		node_widget outputWidget;

		int32 draggingInput;
		int32 draggingOutput;
		atbNodeHandle draggingConnection;

		vec2f screenOffset;

		void addWidget(atbNodeHandle node, float x, float y);
	} uiState;

	void init(void* externMemory, uint32 memorySize, uint32 maxNodes, uint32 maxSyncGroups);
	atbNodeHandle createAnimNode(AnimationNodeType type, float x, float y, const char* name = NULL);
	template<typename T>
	atbNodeHandle createAnimNode(float x, float y, const char* name = NULL);
	void destroyAnimNode(uint32 handle);

	void displayUI(am_model* model, stack_memory_manager *tempMemory);

	void processTree(am_transform_buffer* dst, am_model* model, stack_memory_manager* tempMemory, real32 dtf);

	bool checkForLoops(stack_memory_manager* tempMemory);
};

//Animation Tree Blueprint Nodes

class AmAnimTreeBlueprint;

class amAnimBlueprintNode
{
public:
	atbNodeHandle handle;
	std::string name;

	virtual ~amAnimBlueprintNode()
	{
	}
	virtual void Reset()
	{
		handle = 0;
		name = "";
	}

	virtual int32 GetPoseInputCount()
	{
		return 0;
	}
	virtual atbNodeHandle GetPoseInput(uint32 index)
	{
		return 0;
	}
	virtual bool32 SetPoseInput(uint32 index, atbNodeHandle handle)
	{
		return false;
	}

	virtual void CalculateInputRatios(real32 *inputRatios){}

	virtual bool32 HasSyncGroup()
	{
		return false;
	}
	virtual atbSyncGroupHandle GetSyncGroup()
	{
		return 0;
	}
	virtual bool32 SetSyncGroup(atbSyncGroupHandle syncGroup)
	{
		return false;
	}

	virtual real32 GetAnimLength(){return 0.0f;}

	virtual void Update(am_model* model, AmAnimTreeBlueprint* blueprint, real32 dtf){}
	virtual void ProcessOutput(am_transform_buffer* dst, am_model* model, AmAnimTreeBlueprint* blueprint, am_transform_buffer* resultBuffers, stack_memory_manager* tempMemory)=0;
};


class amAnimBlueprintSyncGroup
{
	float animRatio;
	float totalAnimLength; //used to calculate
	float totalNodeRatio; //total ratios of all nodes that contributed to this.
};

class AmAnimTreeBlueprint
{
public:

	/*
	 * Animation tree blueprint, is basically an animation tree
	 * optimised for ease of use rather than performance.
	 * The final, performance and size optimized animation tree
	 * should be used in the game
	 * */
	atbNodeHandle outputNodeHandle;

	std::vector<amAnimBlueprintNode*> nodes;
	std::vector<uint32> nodeIndices;
	std::vector<real32> nodeRatios;
	std::vector<atbNodeHandle> activeNodeHandles;

	//Synch groups synchronise several animations according to their weights on the final pose
	//If an animation clip has no synch group assigned, it's gonna get updated separately

	std::vector<sync_group_blueprint> syncGroups;
	std::vector<int16> syncGroupIndices;
	std::vector<atbSyncGroupHandle> syncGroupHandleList;


	void RecursiveCalculateNodeRatiosAndSyncGroups(atbNodeHandle handle, float curRatio, am_model* model, stack_memory_manager* tempMemory)
	{
		if (handle > 0)
		{
			uint32 nodeIndex = handle - 1;
			//Note(miko): since an animation clip or pose might be used by multiple nodes, it's final ratio might vary, so we use the ratio difference to add to the sync groups
			real32 prevRatioDif = curRatio - nodeRatios[nodeIndex];
			nodeRatios[nodeIndex] = maxf(curRatio, nodeRatios[nodeIndex]);	//Note: since a node can output into several nodes, we use the highest ratio used

			amAnimBlueprintNode* node = nodes[nodeIndex];

			atbSyncGroupHandle syncGroup = node->GetSyncGroup();
			if(syncGroup && prevRatioDif > 0)
			{
				syncGroups[syncGroup - 1].totalAnimLength += node->GetAnimLength() * prevRatioDif;
				syncGroups[syncGroup - 1].totalNodeRatio += prevRatioDif;
			}

			uint32 inputCount = node->GetPoseInputCount();
			if(inputCount>0)
			{
				real32 *inputRatios = pushStructArray(tempMemory,real32,inputCount);
				node->CalculateInputRatios(inputRatios);
				for(uint32 inputIndex = 0; inputIndex<inputCount; inputIndex++)
				{
					RecursiveCalculateNodeRatiosAndSyncGroups(node->GetPoseInput(inputIndex), inputRatios[inputIndex], model, tempMemory);
				}
			}
		}
	}

	void CalculateNodeRatios(am_model* model, stack_memory_manager* tempMemory)
	{
		uint32 nodeCount = nodes.size();
		real32 *curRatio = &nodeRatios[0];
		for (uint32 ratioIndex = 0; ratioIndex < nodeCount; ++ratioIndex, ++curRatio)
		{
			curRatio = 0;
		}

		uint32 syncGroupCount = syncGroups.size();
		sync_group_blueprint* curGroup = &syncGroups[0];
		for (uint32 syncGroupIndex = 0; syncGroupIndex < syncGroupCount; ++syncGroupIndex, ++curGroup)
		{
			curGroup->totalAnimLength = 0;
			curGroup->totalNodeRatio = 0;
		}

		RecursiveCalculateNodeRatiosAndSyncGroups(outputNodeHandle, 1.0f, model, tempMemory);
	}

	//TODO(miko): optimise this, it does not have to be recursive since all nodes can be updated independently
	void RecursiveUpdateNodes(atbNodeHandle handle, am_model* model, real32 dtf)
	{
		if (handle > 0)
		{
			uint32 nodeIndex = handle - 1;

			amAnimBlueprintNode* node = nodes[nodeIndex];

			node->Update(model,this,dtf);

			uint32 inputCount = node->GetPoseInputCount();
			for(uint32 inputIndex = 0; inputIndex<inputCount; inputIndex++)
			{
				RecursiveUpdateNodes(node->GetPoseInput(inputIndex), model, dtf);
			}
		}
	}

	//TODO(miko): add looping toggle to anim groups and anim clips
	void UpdateSyncGroupsAndNodes(am_model* model, real32 dtf)
	{
		uint32 syncGroupCount = syncGroups.size();
		sync_group_blueprint* curGroup = &syncGroups[0];
		for (uint32 syncGroupIndex = 0; syncGroupIndex < syncGroupCount; ++syncGroupIndex, ++curGroup)
		{
			if (curGroup->totalNodeRatio > FLT_EPSILON && curGroup->totalAnimLength > FLT_EPSILON)
			{
				real32 groupDuration = curGroup->totalAnimLength / curGroup->totalNodeRatio;
				curGroup->animRatio += dtf / groupDuration;
				curGroup->animRatio = fmodf(curGroup->animRatio, 1.0f);
			}
			else
			{
				curGroup->animRatio = 0.0f;
			}
		}

		RecursiveUpdateNodes(outputNodeHandle, model, dtf);
	}

	void RecursiveCalculatePose(atbNodeHandle handle, am_model* model, am_transform_buffer* resultBuffers, stack_memory_manager* tempMemory)
	{
		if (handle > 0)
		{
			uint32 nodeIndex = handle - 1;

			amAnimBlueprintNode* node = nodes[nodeIndex];

			node->ProcessOutput(resultBuffers + nodeIndex, model,this,resultBuffers,tempMemory);

			uint32 inputCount = node->GetPoseInputCount();
			for(uint32 inputIndex = 0; inputIndex<inputCount; inputIndex++)
			{
				RecursiveCalculatePose(node->GetPoseInput(inputIndex), model, resultBuffers, tempMemory);
			}
		}
	}

	void ProcessTree(am_transform_buffer* dst, am_model* model, stack_memory_manager* tempMemory, real32 fdt)
	{
		Assert(dst && model && tempMemory);
		Assert(dst->transforms && dst->nodeCount == model->nodes.nodeCount);
		if (outputNodeHandle > 0)
		{
			TEMP_MEMORY_BLOCK(tempMemory);
			uint32 bufferCount = nodes.size();
			uint32 sceneNodeCount = model->nodes.nodeCount;

			am_transform_buffer *resultBuffers = pushStructArray(tempMemory, am_transform_buffer, bufferCount);

			for (uint32 bufferIndex = 0; bufferIndex < bufferCount; bufferIndex++)
			{
				if (nodes[bufferIndex])
				{
					resultBuffers[bufferIndex].nodeCount = sceneNodeCount;
					resultBuffers[bufferIndex].transforms = pushStructArray(tempMemory, am_transform, sceneNodeCount);
				}
				else
				{
					resultBuffers[bufferIndex].nodeCount = 0;
					resultBuffers[bufferIndex].transforms = NULL;
				}
			}

			CalculateNodeRatios(model, tempMemory);
			UpdateSyncGroupsAndNodes(model, fdt);
			RecursiveCalculatePose(outputNodeHandle, model, resultBuffers, tempMemory);

			memcpy(dst->transforms, resultBuffers[outputNodeHandle - 1].transforms, sizeof(am_transform) * sceneNodeCount);
		}
		else
		{
			memcpy(dst->transforms, model->nodes.transforms, sizeof(am_transform) * model->nodes.nodeCount);
		}
	}
};


class amAnimPoseBlueprintNode: public amAnimBlueprintNode
{
public:
	int32 poseClip;

	virtual void ProcessOutput(am_transform_buffer* dst, am_model* model, AmAnimTreeBlueprint* blueprint, am_transform_buffer* resultBuffers, stack_memory_manager* tempMemory)
	{
		memcpy(dst->transforms, model->nodes.transforms, sizeof(am_transform) * model->nodes.nodeCount);
		if (poseClip >= 0)
		{
			am_anim_clip_info_packed packedAnimInfo = amGetPackedAnimationClipInfo(model, poseClip);
			amCalculatePoseIntoBuffer(dst, &packedAnimInfo);
		}
	}
};

class amAnimClipBlueprintNode: public amAnimBlueprintNode
{
public:
	int32 animationClip;
	real32 animationTime;
	atbSyncGroupHandle syncGroup;

	virtual bool32 HasSyncGroup()
	{
		return true;
	}
	virtual atbSyncGroupHandle GetSyncGroup()
	{
		return syncGroup;
	}
	virtual bool32 SetSyncGroup(atbSyncGroupHandle syncGroup)
	{
		return this->syncGroup = syncGroup;
	}

	virtual real32 GetAnimLength()
	{
		return 0.0f;
	}

	void Reset()
	{
		amAnimBlueprintNode::Reset();
		animationClip = -1;
		animationTime = 0.0f;
		syncGroup = 0;
	}

	virtual void Update(am_model* model, AmAnimTreeBlueprint* blueprint, real32 dtf)
	{
		if (animationClip >= 0)
		{
			am_animation_info* animInfo = model->animation.info + animationClip;
			real32 animDuration = (1.0f / animInfo->fps) * animInfo->frameCount;
			if (syncGroup)
			{
				animationTime = blueprint->syncGroups[syncGroup - 1].animRatio * animDuration;
			}
			else
			{
				if (animDuration > FLT_EPSILON)
				{
					animationTime = fmodf(animationTime + dtf, animDuration);
				}
				else
				{
					animationTime = 0;
				}
			}
		}
	}

	virtual void ProcessOutput(am_transform_buffer* dst, am_model* model, AmAnimTreeBlueprint* blueprint, am_transform_buffer* resultBuffers, stack_memory_manager* tempMemory)
	{
		memcpy(dst->transforms, model->nodes.transforms, sizeof(am_transform) * model->nodes.nodeCount);
		if (animationClip >= 0)
		{
			am_anim_clip_info_packed packedAnimInfo = amGetPackedAnimationClipInfo(model, animationClip);
			amCalculateAnimatedPoseIntoBuffer(dst, &packedAnimInfo, animationTime);
		}
	}
};

class amAnimBlend2BlueprintNode: public amAnimBlueprintNode
{
public:
	atbNodeHandle input[2];
	real32 ratio;

	void Reset()
	{
		amAnimBlueprintNode::Reset();
		input[0] = 0;
		input[1] = 0;
		ratio = 0.0f;
	}

	virtual void ProcessOutput(am_transform_buffer* dst, am_model* model, AmAnimTreeBlueprint* blueprint, am_transform_buffer* resultBuffers, stack_memory_manager* tempMemory)
	{
		const am_transform* src1 = input[0] > 0 ? resultBuffers[input[0] - 1].transforms : model->nodes.transforms;
		const am_transform* src2 = input[1] > 0 ? resultBuffers[input[1] - 1].transforms : model->nodes.transforms;

		am_transform* dstTransforms = dst->transforms;

		uint32 nodeCount = model->nodes.nodeCount;
		real32 blendRatio = ratio;
		for (uint32 nodeIndex = 0; nodeIndex < nodeCount; ++nodeIndex)
		{
			amBlendTransform(dstTransforms[nodeIndex], src1[nodeIndex], src2[nodeIndex], blendRatio);
		}
	}

	virtual void CalculateInputRatios(real32 *inputRatios)
	{
		inputRatios[0] = 1.0f-ratio;
		inputRatios[1] = ratio;
	}
};

class amAnimBlend3BlueprintNode: public amAnimBlueprintNode
{
public:
	atbNodeHandle input[3];
	vec2f uv;

	void Reset()
	{
		amAnimBlueprintNode::Reset();
		input[0] = 0;
		input[1] = 0;
		uv.set(0.0f,0.0f);
	}

	virtual void ProcessOutput(am_transform_buffer* dst, am_model* model, AmAnimTreeBlueprint* blueprint, am_transform_buffer* resultBuffers, stack_memory_manager* tempMemory)
	{
		const am_transform* src1 = input[0] > 0 ? resultBuffers[input[0] - 1].transforms : model->nodes.transforms;
		const am_transform* src2 = input[1] > 0 ? resultBuffers[input[1] - 1].transforms : model->nodes.transforms;
		const am_transform* src3 = input[2] > 0 ? resultBuffers[input[2] - 1].transforms : model->nodes.transforms;

		am_transform* dstTransforms = dst->transforms;

		uint32 nodeCount = model->nodes.nodeCount;
		real32 u = uv.x;
		real32 v = uv.y;
		for (uint32 nodeIndex = 0; nodeIndex < nodeCount; ++nodeIndex)
		{
			amBlendTransform(dstTransforms[nodeIndex], src1[nodeIndex], src2[nodeIndex], u);
			amBlendTransform(dstTransforms[nodeIndex], dstTransforms[nodeIndex], src3[nodeIndex], v);
		}
	}

	virtual void CalculateInputRatios(real32 *inputRatios)
	{
		real32 u = uv.x;
		real32 v = uv.y;
		inputRatios[0] = u * (1 - v);
		inputRatios[1] = (1 - u) * (1 - v);
		inputRatios[2] = v;
	}
};









static const char* NoneModelAnim = "<None>";
bool modelAnimClipNameGetterForUI(void* modelData, int index, const char** outAnimName)
{
	if (modelData)
	{
		am_model* model = (am_model*) modelData;
		int32 animIndex = index - 1;
		if (animIndex < 0)
		{
			*outAnimName = NoneModelAnim;
			return true;
		}
		else if (animIndex < (int32) (model->animation.animCount))
		{
			*outAnimName = model->animation.names[animIndex].name;
			return true;
		}
	}
	return false;
}

static const char* NoneAnimTreeName = "<None>";
bool AnimTreeNodeNameGetterForUI(void* treeBlueprintData, int index, const char** outNodeName)
{
	if (treeBlueprintData)
	{
		animation_tree_blueprint* treeBlueprint = (animation_tree_blueprint*) treeBlueprintData;
		int32 nodeIndex = index - 1;
		if (nodeIndex < 0)
		{
			*outNodeName = NoneAnimTreeName;
			return true;
		}
		else if (nodeIndex < (int32) (treeBlueprint->nodeCount))
		{
			*outNodeName = treeBlueprint->nodeNames[treeBlueprint->nodeHandleList[nodeIndex] - 1].name;
			return true;
		}
	}
	return false;
}

bool AnimTreeNodeTypeNameGetterForUI(void*, int index, const char** outNodeName)
{
	*outNodeName = AnimationNodeTypeNames[index - 1];
	return true;
}

/*
 * selectorLabel: label for the imgui to reference the selector by, must be unique
 * selectedClip: pointer to the index of the clip to be changed according to selection (-1 if not selected anything)
 * model: pointer to model from which to select the animation clips
 *
 * returns true if user selected something, false otherwise
 */
bool AnimClipSelector(const char* selectorLabel, int32 *selectedClip, am_model* model)
{
	int32 curClip = (int32) *selectedClip + 1;
	if (ImGui::Combo(selectorLabel, &curClip, modelAnimClipNameGetterForUI, model, model->animation.animCount + 1))
	{
		if (curClip > 0)
		{
			*selectedClip = curClip - 1;
		}
		else
		{
			*selectedClip = -1;
		}

		return true;
	}

	return false;
}

/*
 * selectorLabel: label for the imgui to reference the selector by, must be unique
 * selectedHandle: pointer to handle to be changed according to selection (0 if no node was selected)
 * ignoreNodeHandle: if the selected handle is this, ignore the change (utility for nodes so they don't select themselves as input)
 * blueprint: the animation tree blueprint from which to select the nodes
 *
 * returns true if a selection was made, false otherwise
 */
bool AnimNodeHandleSelector(const char* selectorLabel, atbNodeHandle *selectedHandle, atbNodeHandle ignoreNodeHandle, animation_tree_blueprint* blueprint)
{
	int32 selectedNode;
	if (*selectedHandle > 0)
	{
		selectedNode = (int32) (blueprint->nodeIndices[*selectedHandle - 1]) + 1;
	}
	else
	{
		selectedNode = 0;
	}
	if (ImGui::Combo(selectorLabel, &selectedNode, AnimTreeNodeNameGetterForUI, blueprint, blueprint->nodeCount + 1))
	{
		if (selectedNode > 0)
		{
			atbNodeHandle chosenHandle = blueprint->nodeHandleList[selectedNode - 1];
			if (chosenHandle != ignoreNodeHandle || ignoreNodeHandle == 0)
			{
				*selectedHandle = chosenHandle;
				return true;
			}
		}
		else
		{
			*selectedHandle = 0;
			return true;
		}
	}
	return false;
}

/*
 * nameBuffer: buffer into which the id will be written
 * nameBufferSize: size of the buffer, must be big enough to accommodate the label "##" and an integer handle
 * label: the label of the object you want to create
 * handle: the handle of the node, it's concatenated to the label to make it a unique identifier.
 */

char* makeIdLabel(char* nameBuffer, uint32 nameBufferSize, const char* label, atbNodeHandle handle)
{
	_snprintf_s(nameBuffer, (size_t) nameBufferSize, nameBufferSize, "%s##%d", label, handle);
	//_snprintf_s(nodeNames[nodeIndex].name, ANIMASTER_NODE_NAME_MAX_LENGTH, "%s[%d]", T::getDefaultName(), nodeIndex + 1);
	return nameBuffer;
}

void pose_node_blueprint::displayUI(pose_node_blueprint* node, atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model)
{
	char nameBuffer[ANIMASTER_NODE_NAME_MAX_LENGTH + 16];
	AnimClipSelector(makeIdLabel(nameBuffer, ARRAYSIZE(nameBuffer), "pose", handle), &node->selectedClip, model);
}

void clip_node_blueprint::displayUI(clip_node_blueprint* node, atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model)
{
	char nameBuffer[ANIMASTER_NODE_NAME_MAX_LENGTH + 16];
	AnimClipSelector(makeIdLabel(nameBuffer, ARRAYSIZE(nameBuffer), "animation", handle), &node->selectedClip, model);
}

void blend2_node_blueprint::displayUI(blend2_node_blueprint* node, atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model)
{
	char nameBuffer[ANIMASTER_NODE_NAME_MAX_LENGTH + 16];

	AnimNodeHandleSelector(makeIdLabel(nameBuffer, ARRAYSIZE(nameBuffer), "input 1", handle), &node->input[0], handle, blueprint);
	AnimNodeHandleSelector(makeIdLabel(nameBuffer, ARRAYSIZE(nameBuffer), "input 2", handle), &node->input[1], handle, blueprint);

	ImGui::SliderFloat("Ratio", &node->ratio, 0.0f, 1.0f);
}

void blend3_node_blueprint::displayUI(blend3_node_blueprint* node, atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model)
{
	char nameBuffer[ANIMASTER_NODE_NAME_MAX_LENGTH + 16];

	AnimNodeHandleSelector(makeIdLabel(nameBuffer, ARRAYSIZE(nameBuffer), "input 1", handle), &node->input[0], handle, blueprint);
	AnimNodeHandleSelector(makeIdLabel(nameBuffer, ARRAYSIZE(nameBuffer), "input 2", handle), &node->input[1], handle, blueprint);
	AnimNodeHandleSelector(makeIdLabel(nameBuffer, ARRAYSIZE(nameBuffer), "input 3", handle), &node->input[2], handle, blueprint);

	ImGuiIO &io = ImGui::GetIO();

	real32 width = 100.0f;
	real32 height = 100.0f;

	vec2f mousePos = io.MousePos;
	vec2f selectorMin = ImGui::GetCursorScreenPos();
	vec2f selectorMax(selectorMin.x + width, selectorMin.y + height);

	ImDrawList* drawList = ImGui::GetWindowDrawList();
	vec2f triangularBlendPoints[3] = {
			selectorMin + vec2f(width * 0.0f, height * 1.0f),
			selectorMin + vec2f { width * 1.0f, height * 1.0f },
			selectorMin + vec2f { width * 0.5f, height * 0.0f },
	};
	ImGui::InvisibleButton("WalkSelector", ImVec2(100.0f, 100.0f));
	vec3f walkBlendUVW;
	if (ImGui::IsItemActive())
	{
		walkBlendUVW = cartesianToBarycentric(mousePos, triangularBlendPoints[0], triangularBlendPoints[1], triangularBlendPoints[2]);
		node->uv.x = walkBlendUVW.x;
		node->uv.y = walkBlendUVW.y;
	}
	else
	{
		walkBlendUVW.x = node->uv.x;
		walkBlendUVW.y = node->uv.y;
		walkBlendUVW.z = 1.0f - node->uv.x - node->uv.y;
	}

	vec2f walkBlendSelectorPos = barycentricToCartesian(walkBlendUVW, triangularBlendPoints[0], triangularBlendPoints[1], triangularBlendPoints[2]) - selectorMin; //mousePos - selectorMin;

	drawList->PushClipRect(selectorMin, selectorMax, true);

	if (ImGui::IsItemHovered())
	{
		drawList->AddRectFilled(selectorMin, selectorMax, 0xffaaaaaa);
	}
	else
	{
		drawList->AddRectFilled(selectorMin, selectorMax, 0xff888888);
	}

	drawList->AddPolyline((ImVec2*) triangularBlendPoints, 3, 0xffffffff, true, 2, false);

	vec2f selectorWorldPos = walkBlendSelectorPos + selectorMin;
	drawList->AddCircle(selectorWorldPos, 3, 0xff000000, 8, 2);

	drawList->PopClipRect();

	ImGui::Text("UV: %f, %f", node->uv.x, node->uv.y);
}

void pose_node_blueprint::processNode(am_transform_buffer* dst, const pose_node_blueprint* node, atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model,
		am_transform_buffer* resultBuffers, stack_memory_manager* tempMemory)
{
	memcpy(dst->transforms, model->nodes.transforms, sizeof(am_transform) * model->nodes.nodeCount);
	if (node->selectedClip >= 0)
	{
		am_anim_clip_info_packed packedAnimInfo = amGetPackedAnimationClipInfo(model, node->selectedClip);
		amCalculatePoseIntoBuffer(dst, &packedAnimInfo);
	}
}

void clip_node_blueprint::processNode(am_transform_buffer* dst, const clip_node_blueprint* node, atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model,
		am_transform_buffer* resultBuffers, stack_memory_manager* tempMemory)
{
	memcpy(dst->transforms, model->nodes.transforms, sizeof(am_transform) * model->nodes.nodeCount);
	if (node->selectedClip >= 0)
	{
		am_anim_clip_info_packed packedAnimInfo = amGetPackedAnimationClipInfo(model, node->selectedClip);
		amCalculateAnimatedPoseIntoBuffer(dst, &packedAnimInfo, node->animationTime);
	}
}

void blend2_node_blueprint::processNode(am_transform_buffer* dst, const blend2_node_blueprint* node, atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model,
		am_transform_buffer* resultBuffers, stack_memory_manager* tempMemory)
{

	const am_transform* src1 = node->input[0] > 0 ? resultBuffers[node->input[0] - 1].transforms : model->nodes.transforms;
	const am_transform* src2 = node->input[1] > 0 ? resultBuffers[node->input[1] - 1].transforms : model->nodes.transforms;

	am_transform* dstTransforms = dst->transforms;

	uint32 nodeCount = model->nodes.nodeCount;
	real32 ratio = node->ratio;
	for (uint32 nodeIndex = 0; nodeIndex < nodeCount; ++nodeIndex)
	{
		amBlendTransform(dstTransforms[nodeIndex], src1[nodeIndex], src2[nodeIndex], ratio);
	}
}

void blend3_node_blueprint::processNode(am_transform_buffer* dst, const blend3_node_blueprint* node, atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model,
		am_transform_buffer* resultBuffers, stack_memory_manager* tempMemory)
{

	const am_transform* src1 = node->input[0] > 0 ? resultBuffers[node->input[0] - 1].transforms : model->nodes.transforms;
	const am_transform* src2 = node->input[1] > 0 ? resultBuffers[node->input[1] - 1].transforms : model->nodes.transforms;
	const am_transform* src3 = node->input[2] > 0 ? resultBuffers[node->input[2] - 1].transforms : model->nodes.transforms;

	am_transform* dstTransforms = dst->transforms;

	uint32 nodeCount = model->nodes.nodeCount;
	real32 u = node->uv.x;
	real32 v = node->uv.y;
	for (uint32 nodeIndex = 0; nodeIndex < nodeCount; ++nodeIndex)
	{
		amBlendTransform(dstTransforms[nodeIndex], src1[nodeIndex], src2[nodeIndex], u);
		amBlendTransform(dstTransforms[nodeIndex], dstTransforms[nodeIndex], src3[nodeIndex], v);
	}
}

void animation_tree_blueprint::init(void* externMemory, uint32 memorySize, uint32 maxNodes, uint32 maxSyncGroups)
{
	stack_memory_manager memory;
	memory.memory = (uint8*) externMemory;
	memory.memorySize = memorySize;
	memory.memoryUsed = 0;

	outputNodeHandle = 0;

	maxAnimNodeCount = maxNodes;
	nodeCount = 0;
	nodeNames = pushStructArray(&memory, am_name, maxNodes);
	ZERO_STRUCT_ARRAY(am_name, nodeNames, maxNodes);

	nodeTypes = pushStructArray(&memory, AnimationNodeType, maxNodes);
	ZERO_STRUCT_ARRAY(AnimationNodeType, nodeTypes, maxNodes);

	nodeDatas = pushStructArray(&memory, void*, maxNodes);
	ZERO_STRUCT_ARRAY(void*, nodeDatas, maxNodes);

	nodeDataSize = pushStructArray(&memory, uint32, maxNodes);
	ZERO_STRUCT_ARRAY(uint32, nodeDataSize, maxNodes);

	nodeHandleList = pushStructArray(&memory, atbNodeHandle, maxNodes);
	ZERO_STRUCT_ARRAY(atbNodeHandle, nodeHandleList, maxNodes);

	nodeIndices = pushStructArray(&memory, int16, maxNodes);
	ZERO_STRUCT_ARRAY(int16, nodeIndices, maxNodes);

	nodeRatios = pushStructArray(&memory, real32, maxNodes);
	ZERO_STRUCT_ARRAY(real32, nodeRatios, maxNodes);

	maxSyncGroupCount = maxSyncGroups;
	syncGroupCount = 0;

	syncGroupNames = pushStructArray(&memory, am_name, maxSyncGroupCount);
	ZERO_STRUCT_ARRAY(am_name, syncGroupNames, maxSyncGroupCount);

	syncGroups = pushStructArray(&memory, sync_group_blueprint, maxSyncGroupCount);
	ZERO_STRUCT_ARRAY(sync_group_blueprint, syncGroups, maxSyncGroupCount);

	syncGroupHandleList = pushStructArray(&memory, atbSyncGroupHandle, maxSyncGroupCount);
	ZERO_STRUCT_ARRAY(atbSyncGroupHandle, syncGroupHandleList, maxSyncGroupCount);

	syncGroupIndices = pushStructArray(&memory, int16, maxSyncGroupCount);
	ZERO_STRUCT_ARRAY(int16, syncGroupIndices, maxSyncGroupCount);

	uiState.nodeWidgets = pushStructArray(&memory, node_widget, maxAnimNodeCount);

	uiState.outputWidget.inputCount = 1;
	uiState.outputWidget.pos.set(650, 250);
	uiState.outputWidget.size.set(0.0f, 0.0f);

	nodeMemory = memory.memory + memory.memoryUsed;
	nodeMemorySize = memory.memorySize - memory.memoryUsed;
	nodeMemoryUsed = 0;
}

atbNodeHandle animation_tree_blueprint::createAnimNode(AnimationNodeType type, float x, float y, const char* name)
{
	switch (type)
	{
		case AnimationNodeType_Pose:
			return createAnimNode<pose_node_blueprint>(x, y, name);
		case AnimationNodeType_Clip:
			return createAnimNode<clip_node_blueprint>(x, y, name);
		case AnimationNodeType_Blender2:
			return createAnimNode<blend2_node_blueprint>(x, y, name);
		case AnimationNodeType_Blender3:
			return createAnimNode<blend3_node_blueprint>(x, y, name);
		default:
			return 0;
	}
}

template<typename T> atbNodeHandle animation_tree_blueprint::createAnimNode(float x, float y, const char* name)
{
	atbNodeHandle handle = 0;
	uint32 memoryNeeded = ((sizeof(T) + 15) / 16) * 16;
	if (nodeMemoryUsed + memoryNeeded < nodeMemorySize)
	{
		for (uint32 nodeIndex = 0; nodeIndex < maxAnimNodeCount; nodeIndex++)
		{
			if (!nodeDatas[nodeIndex])
			{
				T* node = NULL;
				node = (T*) (nodeMemory + nodeMemoryUsed);
				nodeMemoryUsed += memoryNeeded;
				node->reset();

				uiState.nodeWidgets[nodeIndex].pos.set(x, y);
				uiState.nodeWidgets[nodeIndex].size.set(0.0f, 0.0f); //will be calculated on next frame
				uiState.nodeWidgets[nodeIndex].inputCount = 0;
				uiState.nodeWidgets[nodeIndex].inputs = NULL;
				uiState.nodeWidgets[nodeIndex].outputPos.set(0.0f, 0.0f);

				nodeNames[nodeIndex].name[0];
				if (name)
				{
					strncpy_s(nodeNames[nodeIndex].name, name, ANIMASTER_NODE_NAME_MAX_LENGTH);
				}
				else
				{
					_snprintf_s(nodeNames[nodeIndex].name, ANIMASTER_NODE_NAME_MAX_LENGTH, "%s[%d]", T::getDefaultName(), nodeIndex + 1);
				}
				nodeTypes[nodeIndex] = T::type;
				nodeDataSize[nodeIndex] = memoryNeeded;
				nodeDatas[nodeIndex] = (void*) node;
				nodeIndices[nodeIndex] = (int16) nodeCount;

				nodeHandleList[nodeCount] = (uint16) nodeIndex + 1;
				nodeCount++;
				handle = nodeIndex + 1;
				break;
			}
		}
	}

	return handle;
}

void animation_tree_blueprint::destroyAnimNode(atbNodeHandle handle)
{
	if (handle >= 1 && handle <= maxAnimNodeCount)
	{
		uint32 index = handle - 1;
		if (nodeDatas[index])
		{
			if (outputNodeHandle == handle)
			{
				outputNodeHandle = 0;
			}
			uint8* freedLocation = (uint8*) nodeDatas[index];
			uint64 freedSpace = nodeDataSize[index];
			uint8* nextDataLocation = freedLocation + freedSpace;

			uint64 memoryOffset = ((uint64) nextDataLocation - (uint64) nodeMemory);
			Assert(nodeMemoryUsed >= memoryOffset);

			memmove(freedLocation, nextDataLocation, (size_t) (nodeMemoryUsed - memoryOffset));
			nodeMemoryUsed -= freedSpace;

			nodeTypes[index] = AnimationNodeType_None;
			nodeDatas[index] = 0;
			nodeNames[index].name[0] = 0;
			nodeDataSize[index] = 0;

			//TODO(miko): find the packed handles and shift all of them
			int32 destroyedNodeIndex = nodeIndices[index];
			for (uint32 handleIndex = destroyedNodeIndex; handleIndex < nodeCount - 1; handleIndex++)
			{
				nodeHandleList[handleIndex] = nodeHandleList[handleIndex + 1];
			}
			//memmove(nodeHandleList + destroyedNodeIndex, nodeHandleList + destroyedNodeIndex + 1, sizeof(uint16) * (nodeCount - destroyedNodeIndex - 1));
			nodeIndices[index] = -1;

			nodeCount--;

			for (uint32 nodeIndex = 0; nodeIndex < maxAnimNodeCount; nodeIndex++)
			{
				if (nodeTypes[nodeIndex] != AnimationNodeType_None)
				{
					if (nodeDatas[nodeIndex] > freedLocation)
					{
						nodeDatas[nodeIndex] = (void*) ((uint8*) (nodeDatas[nodeIndex]) - freedSpace);
					}

					switch (nodeTypes[nodeIndex])
					{
						case AnimationNodeType_Blender2:
							{
							blend2_node_blueprint* node = (blend2_node_blueprint*) nodeDatas[nodeIndex];
							if (node->input[0] == handle)
							{
								node->input[0] = 0;
							}
							if (node->input[1] == handle)
							{
								node->input[1] = 0;
							}
						}
						break;
						case AnimationNodeType_Blender3:
							{

							blend3_node_blueprint* node = (blend3_node_blueprint*) nodeDatas[nodeIndex];
							if (node->input[0] == handle)
							{
								node->input[0] = 0;
							}
							if (node->input[1] == handle)
							{
								node->input[1] = 0;
							}
							if (node->input[2] == handle)
							{
								node->input[2] = 0;
							}
						}
						break;
					}

					if (nodeIndices[nodeIndex] > destroyedNodeIndex)
					{
						--nodeIndices[nodeIndex];
					}
				}
			}
		}
	}
}

void animation_tree_blueprint::displayUI(am_model* model, stack_memory_manager *tempMemory)
{
	Assert(model && tempMemory);

	char nameBuffer[ANIMASTER_NODE_NAME_MAX_LENGTH + 16];

	TEMP_MEMORY_BLOCK(tempMemory);

	atbNodeHandle* handlesToDestroy = pushStructArray(tempMemory, atbNodeHandle, nodeCount);
	uint32 handlesToDestroyCount = 0;

	ImGuiIO &io = ImGui::GetIO();

	vec2f widgetPadding(10.0f, 10.0f);

	ImDrawList *drawList = ImGui::GetWindowDrawList();

	uint32 maxWidgetCount = maxAnimNodeCount;

	vec2f windowPos = ImGui::GetCursorScreenPos();
	vec2f mousePosGlobal = io.MousePos;
	vec2f mousePosBlueprint = mousePosGlobal;

	bool createdNode = false;
	if (ImGui::BeginPopupContextWindow(true, "test", 1))
	{
		vec2f popupMousePos = ImGui::GetMousePosOnOpeningCurrentPopup();
		vec2f newNodeMousePos = popupMousePos - windowPos - uiState.screenOffset;

		ImGui::Text("Create");
		ImGui::Indent();
		if (ImGui::Selectable("Pose Node"))
		{
			createdNode = true;
			createAnimNode(AnimationNodeType_Pose, newNodeMousePos.x, newNodeMousePos.y);
		}
		if (ImGui::Selectable("Anim Clip Node"))
		{
			createdNode = true;
			createAnimNode(AnimationNodeType_Clip, newNodeMousePos.x, newNodeMousePos.y);
		}
		if (ImGui::Selectable("Blend Node"))
		{
			createdNode = true;
			createAnimNode(AnimationNodeType_Blender2, newNodeMousePos.x, newNodeMousePos.y);
		}
		if (ImGui::Selectable("Triple Blend Node"))
		{
			createdNode = true;
			createAnimNode(AnimationNodeType_Blender3, newNodeMousePos.x, newNodeMousePos.y);
		}
		ImGui::Unindent();
		ImGui::EndPopup();
	}

	struct link
	{
		atbNodeHandle input;
		atbNodeHandle output;
		vec2f inputPos;
	};

	uint32 maxLinkCount = maxAnimNodeCount * 3;
	uint32 linkCount = 0;
	link* links = pushStructArray(tempMemory, link, maxAnimNodeCount*3); //*3 since that's the max number of inputs we can have

#undef PUSH_LINK
#define PUSH_LINK(inputHandle, outputHandle, inputPos)\
		if(linkCount<maxLinkCount){links[linkCount].input=inputHandle;links[linkCount].output=outputHandle;links[linkCount].inputPos=inputPos;linkCount++;}

	bool32 isDraggingNode = false;

	bool32 canHoverOverInputsAndOutputs = true;
	int32 inputHovered = -1;
	int32 outputHovered = -1;
	atbNodeHandle nodeHovered = 0;

	bool32 attachInput = false;
	bool32 attachOutput = false;
	//Potential connection is used to make the connection curve snap to targets
	atbNodeHandle potentialConnectionOutputNode = 0;
	atbNodeHandle potentialConnectionInputNode = 0;
	int32 potentialConnectionInputIndex = -1;

	vec2f potentialConnectionOutputPos(mousePosBlueprint);
	vec2f potentialConnectionInputPos(mousePosBlueprint);

	if (uiState.draggingConnection)
	{
		potentialConnectionOutputNode = uiState.draggingOutput >= 0 ? uiState.draggingConnection : 0;
		potentialConnectionInputNode = uiState.draggingInput >= 0 ? uiState.draggingConnection : 0;
		potentialConnectionInputIndex = uiState.draggingInput;

		if (!ImGui::IsMouseDown(0))
		{
			if (potentialConnectionInputNode)
			{
				attachOutput = true;
			}
			else if (potentialConnectionOutputNode)
			{
				attachInput = true;
			}
			uiState.draggingConnection = 0;
			uiState.draggingOutput = -1;
			uiState.draggingInput = -1;
		}
	}
	for (uint32 widgetIndex = 0; widgetIndex < nodeCount; widgetIndex++)
	{
		atbNodeHandle handle = nodeHandleList[widgetIndex];
		uint32 nodeIndex = handle - 1;
		node_widget* widget = uiState.nodeWidgets + nodeIndex;

		void* data = nodeDatas[nodeIndex];
		switch (nodeTypes[nodeIndex])
		{
			case AnimationNodeType_Pose:
				{
				pose_node_blueprint* node = (pose_node_blueprint*) data;
				uiState.nodeWidgets[nodeIndex].inputCount = node->getNodeInputCount();
				uiState.nodeWidgets[nodeIndex].inputs = node->getInputs();
			}
			break;
			case AnimationNodeType_Clip:
				{
				clip_node_blueprint* node = (clip_node_blueprint*) data;
				uiState.nodeWidgets[nodeIndex].inputCount = node->getNodeInputCount();
				uiState.nodeWidgets[nodeIndex].inputs = node->getInputs();
			}
			break;
			case AnimationNodeType_Blender2:
				{
				blend2_node_blueprint* node = (blend2_node_blueprint*) data;
				uiState.nodeWidgets[nodeIndex].inputCount = node->getNodeInputCount();
				uiState.nodeWidgets[nodeIndex].inputs = node->getInputs();
			}
			break;
			case AnimationNodeType_Blender3:
				{
				blend3_node_blueprint* node = (blend3_node_blueprint*) data;
				uiState.nodeWidgets[nodeIndex].inputCount = node->getNodeInputCount();
				uiState.nodeWidgets[nodeIndex].inputs = node->getInputs();
			}
			break;
		}
	}

	float inputOutputCircleRadius = ImGui::GetItemsLineHeightWithSpacing() * 0.4f;

	for (uint32 widgetIndex = 0; widgetIndex < nodeCount; widgetIndex++)
	{
		atbNodeHandle handle = nodeHandleList[widgetIndex];
		uint32 nodeIndex = handle - 1;
		node_widget* widget = uiState.nodeWidgets + nodeIndex;

		vec2f widgetBoxMin = windowPos + uiState.screenOffset + widget->pos;
		ImGui::SetCursorScreenPos(widgetBoxMin);

		ImGui::BeginChild(handle, widget->size);
//		ImGui::PushID(handle);
//		ImGui::BeginGroup();
		{
			vec2f inputsStartPos = vec2f(0.0, 20.0f);
			vec2f outputsStartPos = vec2f(widget->size.x, widget->size.y * 0.5f);

			ImGui::PushItemWidth(120);

			ImGui::SetCursorScreenPos(widgetBoxMin + widgetPadding);

			ImDrawList *childDrawList = ImGui::GetWindowDrawList();

			childDrawList->ChannelsSplit(2);
			childDrawList->ChannelsSetCurrent(1);
			//ImGui::PushID(handle);

			void* data = nodeDatas[nodeIndex];
			uint32 inputCount = widget->inputCount;

			//Contents
			ImGui::BeginGroup();
			{
				real32 itemSpacing = ImGui::GetStyle().ItemSpacing.x;

				//Header
				ImGui::InputText(makeIdLabel(nameBuffer, ARRAYSIZE(nameBuffer), "name", handle), nodeNames[handle - 1].name, ANIMASTER_NODE_NAME_MAX_LENGTH);
				ImGui::SameLine();
				if (ImGui::Button(makeIdLabel(nameBuffer, ARRAY_LENGTH(nameBuffer), "Destroy", handle)))
				{
					handlesToDestroy[handlesToDestroyCount] = handle;
					handlesToDestroyCount++;
				}

				vec2f bodyPos = ImGui::GetCursorPos();
				bodyPos.x += widgetPadding.x;

				vec2f columnPos = bodyPos;
				//Inputs
				ImGui::BeginGroup();
				{
					inputsStartPos = columnPos;

					for (uint32 inputIndex = 0; inputIndex < inputCount; inputIndex++)
					{
						vec2f inputRelPos = inputsStartPos + vec2f(0.0, inputOutputCircleRadius + inputIndex * ImGui::GetItemsLineHeightWithSpacing());
						vec2f inputPos = widgetBoxMin + inputRelPos;

						ImU32 inputColor = ImColor(100, 100, 128);

						if (potentialConnectionOutputNode != handle && potentialConnectionInputNode == 0)
						{
							//We make the hover area bigger than the display for ease of use
							vec2f inputColMin = inputPos + vec2f(-inputOutputCircleRadius, -inputOutputCircleRadius);
							vec2f inputColMax = inputPos + vec2f(+inputOutputCircleRadius, +inputOutputCircleRadius);

							if (mousePosBlueprint.x > inputColMin.x && mousePosBlueprint.y > inputColMin.y
									&& mousePosBlueprint.x < inputColMax.x && mousePosBlueprint.y < inputColMax.y
											)
							{
								outputHovered = -1;
								inputHovered = inputIndex;
								nodeHovered = handle;
								inputColor = ImColor(200, 200, 255);

								if (attachInput)
								{
									attachInput = false;
									widget->inputs[inputIndex] = potentialConnectionOutputNode;
								}
								else if (potentialConnectionOutputNode)
								{
									potentialConnectionInputNode = handle;
									potentialConnectionInputIndex = inputIndex;
									//PUSH_LINK(widget->inputs[inputIndex],handle,inputRelPos);
								}
							}
						}

						if (potentialConnectionInputNode == handle && potentialConnectionInputIndex == (int32) inputIndex)
						{
							potentialConnectionInputPos = inputPos;
						}

						PUSH_LINK(widget->inputs[inputIndex], handle, inputPos);

						childDrawList->AddCircleFilled(inputPos, inputOutputCircleRadius, inputColor, 16);
					}
				}
				ImGui::EndGroup();

				columnPos.x += inputOutputCircleRadius * 2.0f;
				ImGui::SetCursorPos(columnPos);

				//Content
				ImGui::BeginGroup();
				{
					switch (nodeTypes[nodeIndex])
					{
						case AnimationNodeType_Pose:
							{
							pose_node_blueprint* node = (pose_node_blueprint*) data;
							node->displayUI(node, handle, this, model);
						}
						break;
						case AnimationNodeType_Clip:
							{
							clip_node_blueprint* node = (clip_node_blueprint*) data;
							node->displayUI(node, handle, this, model);
						}
						break;
						case AnimationNodeType_Blender2:
							{
							blend2_node_blueprint* node = (blend2_node_blueprint*) data;
							node->displayUI(node, handle, this, model);
						}
						break;
						case AnimationNodeType_Blender3:
							{
							blend3_node_blueprint* node = (blend3_node_blueprint*) data;
							node->displayUI(node, handle, this, model);
						}
						break;
					}
				}
				ImGui::EndGroup();

				vec2f contentSize = ImGui::GetItemRectSize();
				columnPos.x += contentSize.x + itemSpacing;

				ImGui::SetCursorPos(columnPos);

				//Outputs
				ImGui::BeginGroup();
				ImGui::Dummy(vec2f(inputOutputCircleRadius * 2.0f, inputOutputCircleRadius * 2.0f));
				{
					outputsStartPos = columnPos + vec2f(inputOutputCircleRadius, inputOutputCircleRadius);
					vec2f outputPos = widgetBoxMin + outputsStartPos;

					//We make the hover area bigger than the display for ease of use
					vec2f outputColMin = outputPos + vec2f(-inputOutputCircleRadius, -inputOutputCircleRadius * 2);
					vec2f outputColMax = outputPos + vec2f(+inputOutputCircleRadius, +inputOutputCircleRadius * 2);

					ImU32 outputColor = ImColor(100, 100, 128);
					if (potentialConnectionInputNode != handle && potentialConnectionOutputNode == 0)
					{
						if (mousePosBlueprint.x > outputColMin.x && mousePosBlueprint.y > outputColMin.y
								&& mousePosBlueprint.x < outputColMax.x && mousePosBlueprint.y < outputColMax.y
										)
						{
							outputHovered = 0;
							inputHovered = -1;
							nodeHovered = handle;
							outputColor = ImColor(200, 200, 255);

							if (attachOutput)
							{
								attachOutput = false;
								if (potentialConnectionInputNode == atbNodeHandle(-1))
								{
									outputNodeHandle = handle;
								}
								else
								{
									uiState.nodeWidgets[potentialConnectionInputNode - 1].inputs[potentialConnectionInputIndex] = handle;
								}
							}
							else if (potentialConnectionInputNode)
							{
								potentialConnectionOutputNode = handle;
							}
						}
					}

					childDrawList->AddCircleFilled(outputPos, inputOutputCircleRadius, outputColor, 16);

					if (potentialConnectionOutputNode == handle)
					{
						potentialConnectionOutputPos = outputPos;
					}

					widget->outputPos = outputPos;
				}
				ImGui::EndGroup();

			}
			ImGui::EndGroup();

			vec2f widgetSize = ImGui::GetItemRectSize();
			vec2f widgetBoxMax = widgetBoxMin + widgetSize + widgetPadding * 2;
			widgetBoxMax.x = floorf(widgetBoxMax.x);
			widgetBoxMax.y = floorf(widgetBoxMax.y);

			if (widgetSize.x > 0 && widgetSize.y > 0)
			{
				widget->size = widgetSize + widgetPadding * 2;
			}

			childDrawList->ChannelsSetCurrent(0);
			ImGui::SetCursorScreenPos(widgetBoxMin);
			ImGui::InvisibleButton("node", widget->size);

			bool32 isDraggingActive = false;
			if (ImGui::IsItemActive() && !uiState.draggingConnection)
			{
				isDraggingActive = true;
			}

			childDrawList->AddRectFilled(widgetBoxMin, widgetBoxMax, ImColor(40, 40, 40), 5);
			childDrawList->AddRect(widgetBoxMin, widgetBoxMax, ImColor(200, 200, 200), 5);

			childDrawList->ChannelsMerge();
			//ImGui::PopID();

			ImGui::PopItemWidth();

			if (isDraggingActive && ImGui::IsMouseDragging(0))
			{
				widget->pos += io.MouseDelta;
				isDraggingNode = true;
			}
		}
		ImGui::EndChild();

//		ImGui::EndGroup();
//		ImGui::PopID();
	}

	bool hoveringOverNodeOutput = false;
	;
	{
		atbNodeHandle handle = atbNodeHandle(-1);
		node_widget* widget = &uiState.outputWidget;

		widget->inputCount = 1;
		widget->inputs = &outputNodeHandle;

		vec2f widgetBoxMin = windowPos + uiState.screenOffset + widget->pos;
		ImGui::SetCursorScreenPos(widgetBoxMin);

		ImGui::BeginChild(handle, widget->size);
		{
			vec2f inputsStartPos = vec2f(0.0, 20.0f);
			vec2f outputsStartPos = vec2f(widget->size.x, widget->size.y * 0.5f);

			ImGui::PushItemWidth(120);

			ImGui::SetCursorScreenPos(widgetBoxMin + widgetPadding);

			ImDrawList *childDrawList = ImGui::GetWindowDrawList();

			childDrawList->ChannelsSplit(2);
			childDrawList->ChannelsSetCurrent(1);

			uint32 inputCount = widget->inputCount;

			//Contents
			ImGui::BeginGroup();
			{
				real32 itemSpacing = ImGui::GetStyle().ItemSpacing.x;

				//Header
				ImGui::Text("Output");

				vec2f bodyPos = ImGui::GetCursorPos();
				bodyPos.x += widgetPadding.x;

				vec2f columnPos = bodyPos;
				//Inputs
				ImGui::BeginGroup();
				{
					inputsStartPos = columnPos;

					for (uint32 inputIndex = 0; inputIndex < inputCount; inputIndex++)
					{
						vec2f inputRelPos = inputsStartPos + vec2f(0.0, inputOutputCircleRadius + inputIndex * ImGui::GetItemsLineHeightWithSpacing());
						vec2f inputPos = widgetBoxMin + inputRelPos;

						ImU32 inputColor = ImColor(100, 100, 128);

						if (potentialConnectionOutputNode != handle && potentialConnectionInputNode == 0)
						{
							//We make the hover area bigger than the display for ease of use
							vec2f inputColMin = inputPos + vec2f(-inputOutputCircleRadius, -inputOutputCircleRadius);
							vec2f inputColMax = inputPos + vec2f(+inputOutputCircleRadius, +inputOutputCircleRadius);

							if (mousePosBlueprint.x > inputColMin.x && mousePosBlueprint.y > inputColMin.y
									&& mousePosBlueprint.x < inputColMax.x && mousePosBlueprint.y < inputColMax.y
											)
							{
								outputHovered = -1;
								inputHovered = inputIndex;
								nodeHovered = handle;
								inputColor = ImColor(200, 200, 255);

								if (attachInput)
								{
									attachInput = false;
									widget->inputs[inputIndex] = potentialConnectionOutputNode;
								}
								else if (potentialConnectionOutputNode)
								{
									potentialConnectionInputNode = handle;
									potentialConnectionInputIndex = inputIndex;
									//PUSH_LINK(widget->inputs[inputIndex],handle,inputRelPos);
								}
							}
						}

						if (potentialConnectionInputNode == handle && potentialConnectionInputIndex == (int32) inputIndex)
						{
							potentialConnectionInputPos = inputPos;
						}

						PUSH_LINK(widget->inputs[inputIndex], handle, inputPos);

						childDrawList->AddCircleFilled(inputPos, inputOutputCircleRadius, inputColor, 16);
					}
				}
				ImGui::EndGroup();

				columnPos.x += inputOutputCircleRadius * 2.0f;
				ImGui::SetCursorPos(columnPos);

				//Content
				ImGui::BeginGroup();
				{
					AnimNodeHandleSelector("output Node##output", &outputNodeHandle, 0, this);
				}
				ImGui::EndGroup();

				vec2f contentSize = ImGui::GetItemRectSize();
				columnPos.x += contentSize.x + itemSpacing;

				ImGui::SetCursorPos(columnPos);
			}
			ImGui::EndGroup();

			vec2f widgetSize = ImGui::GetItemRectSize();
			vec2f widgetBoxMax = widgetBoxMin + widgetSize + widgetPadding * 2;
			if (widgetSize.x > 0 && widgetSize.y > 0)
			{
				widget->size = widgetSize + widgetPadding * 2;
			}

			childDrawList->ChannelsSetCurrent(0);
			ImGui::SetCursorScreenPos(widgetBoxMin);
			ImGui::InvisibleButton("node", widget->size);

			bool32 isDraggingActive = false;
			if (ImGui::IsItemActive() && !uiState.draggingConnection)
			{
				isDraggingActive = true;
			}

			childDrawList->AddRectFilled(vec2f(0.0f, 0.0f), vec2f(1300, 1000), ImColor(255, 0, 0, 128));

			childDrawList->AddRectFilled(widgetBoxMin, widgetBoxMax, ImColor(40, 40, 40), 5);
			childDrawList->AddRect(widgetBoxMin, widgetBoxMax, ImColor(200, 200, 200), 5);

			childDrawList->ChannelsMerge();
			//ImGui::PopID();

			ImGui::PopItemWidth();

			if (isDraggingActive && ImGui::IsMouseDragging(0))
			{
				widget->pos += io.MouseDelta;
				isDraggingNode = true;
			}
		}
		ImGui::EndChild();
	}

	//drawList->ChannelsSetCurrent(0);
	for (uint32 linkIndex = 0; linkIndex < linkCount; linkIndex++)
	{
		if (links[linkIndex].input && links[linkIndex].output)
		{
			vec2f outputPos(0.0f, 0.0f);
			atbNodeHandle inputIndex = links[linkIndex].input - 1;
			vec2f linkStart = uiState.nodeWidgets[inputIndex].outputPos;
			vec2f linkEnd = links[linkIndex].inputPos;

			drawList->AddBezierCurve(linkStart, linkStart + vec2f(50.0f, 0.0f), linkEnd + vec2f(-50.0f, 0.0f), linkEnd, ImColor(200, 200, 0), 3, 20);
		}
	}

	if (!uiState.draggingConnection && !isDraggingNode)
	{
		if (ImGui::IsMouseDown(0) && nodeHovered)
		{
			uiState.draggingConnection = nodeHovered;
			uiState.draggingInput = inputHovered;
			uiState.draggingOutput = outputHovered;
		}
		else if (ImGui::IsMouseDown(2))
		{
			uiState.screenOffset += io.MouseDelta;
		}
	}
	if (potentialConnectionInputNode || potentialConnectionOutputNode)
	{
		vec2f linkStart = potentialConnectionOutputPos;

		vec2f linkEnd = potentialConnectionInputPos;

		drawList->AddBezierCurve(linkStart, linkStart + vec2f(50.0f, 0.0f), linkEnd + vec2f(-50.0f, 0.0f), linkEnd, ImColor(200, 200, 0), 3, 20);
	}

#undef PUSH_LINK

	for (uint32 handleIndex = 0; handleIndex < handlesToDestroyCount; ++handleIndex)
	{
		destroyAnimNode(handlesToDestroy[handleIndex]);
	}
}

void recursiveCalculateNodeRatiosAndSyncGroups(atbNodeHandle handle, float curRatio, animation_tree_blueprint* blueprint, am_model* model)
{
	if (handle > 0)
	{
		uint32 nodeIndex = handle - 1;
		//Note(miko): since an animation clip or pose might be used by multiple nodes, it's final ratio might vary, so we use the ratio difference to add to the sync groups
		real32 prevRatioDif = curRatio - blueprint->nodeRatios[nodeIndex];
		blueprint->nodeRatios[nodeIndex] = maxf(curRatio, blueprint->nodeRatios[nodeIndex]);	//Note: since a node can output into several nodes, we use the highest ratio used

		switch (blueprint->nodeTypes[nodeIndex])
		{
			case AnimationNodeType_Pose:
				{
				pose_node_blueprint* node = (pose_node_blueprint*) blueprint->nodeDatas[nodeIndex];
				if (node->syncGroup && node->selectedClip >= 0 && prevRatioDif > 0)
				{
					//Note(miko): for now a pose is considered to be an animation without any length
					blueprint->syncGroups[node->syncGroup - 1].totalNodeRatio += prevRatioDif;
				}

			}
			break;
			case AnimationNodeType_Clip:
				{
				clip_node_blueprint* node = (clip_node_blueprint*) blueprint->nodeDatas[nodeIndex];
				if (node->syncGroup && node->selectedClip >= 0 && prevRatioDif > 0)
				{
					am_animation_info* animInfo = model->animation.info + node->selectedClip;
					blueprint->syncGroups[node->syncGroup - 1].totalAnimLength += (1.0f / (animInfo->fps) * animInfo->frameCount) * prevRatioDif;
					blueprint->syncGroups[node->syncGroup - 1].totalNodeRatio += prevRatioDif;
				}
			}
			break;
			case AnimationNodeType_Blender2:
				{
				blend2_node_blueprint* node = (blend2_node_blueprint*) blueprint->nodeDatas[nodeIndex];
				recursiveCalculateNodeRatiosAndSyncGroups(node->input[0], curRatio * node->ratio, blueprint, model);
				recursiveCalculateNodeRatiosAndSyncGroups(node->input[1], curRatio * (node->ratio - 1), blueprint, model);
			}
			break;
			case AnimationNodeType_Blender3:
				{
				blend3_node_blueprint* node = (blend3_node_blueprint*) blueprint->nodeDatas[nodeIndex];
				real32 u = node->uv.x;
				real32 v = node->uv.y;
				real32 ratio1 = u * (1 - v);
				real32 ratio2 = (1 - u) * (1 - v);
				real32 ratio3 = v;

				recursiveCalculateNodeRatiosAndSyncGroups(node->input[0], ratio1, blueprint, model);
				recursiveCalculateNodeRatiosAndSyncGroups(node->input[1], ratio2, blueprint, model);
				recursiveCalculateNodeRatiosAndSyncGroups(node->input[2], ratio3, blueprint, model);
			}
			break;
		}
	}
}

void calculateNodeRatios(animation_tree_blueprint* blueprint, am_model* model)
{
	uint32 nodeCount = blueprint->maxAnimNodeCount;
	real32 *curRatio = blueprint->nodeRatios;
	for (uint32 ratioIndex = 0; ratioIndex < nodeCount; ++ratioIndex, ++curRatio)
	{
		curRatio = 0;
	}

	uint32 syncGroupCount = blueprint->maxSyncGroupCount;
	sync_group_blueprint* curGroup = blueprint->syncGroups;
	for (uint32 syncGroupIndex = 0; syncGroupIndex < syncGroupCount; ++syncGroupIndex, ++curGroup)
	{
		curGroup->totalAnimLength = 0;
		curGroup->totalNodeRatio = 0;
	}

	recursiveCalculateNodeRatiosAndSyncGroups(blueprint->outputNodeHandle, 1.0f, blueprint, model);
}

//TODO(miko): optimise this, it does not have to be recursive since all nodes can be updated independently
void recursiveUpdateSyncGroupsAndAnimClipTimes(atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model, real32 dtf)
{
	if (handle > 0)
	{
		uint32 nodeIndex = handle - 1;

		switch (blueprint->nodeTypes[nodeIndex])
		{
			case AnimationNodeType_Clip:
				{
				clip_node_blueprint* node = (clip_node_blueprint*) blueprint->nodeDatas[nodeIndex];
				if (node->selectedClip >= 0)
				{
					am_animation_info* animInfo = model->animation.info + node->selectedClip;
					real32 animDuration = (1.0f / animInfo->fps) * animInfo->frameCount;
					if (node->syncGroup)
					{
						node->animationTime = blueprint->syncGroups[node->syncGroup - 1].animRatio * animDuration;
					}
					else
					{
						if (animDuration > FLT_EPSILON)
						{
							node->animationTime = fmodf(node->animationTime + dtf, animDuration);
						}
						else
						{
							node->animationTime = 0;
						}
					}
				}
			}
			break;
			case AnimationNodeType_Blender2:
				{
				blend2_node_blueprint* node = (blend2_node_blueprint*) blueprint->nodeDatas[nodeIndex];
				recursiveUpdateSyncGroupsAndAnimClipTimes(node->input[0], blueprint, model, dtf);
				recursiveUpdateSyncGroupsAndAnimClipTimes(node->input[1], blueprint, model, dtf);
			}
			break;
			case AnimationNodeType_Blender3:
				{
				blend3_node_blueprint* node = (blend3_node_blueprint*) blueprint->nodeDatas[nodeIndex];
				recursiveUpdateSyncGroupsAndAnimClipTimes(node->input[0], blueprint, model, dtf);
				recursiveUpdateSyncGroupsAndAnimClipTimes(node->input[1], blueprint, model, dtf);
				recursiveUpdateSyncGroupsAndAnimClipTimes(node->input[2], blueprint, model, dtf);
			}
			break;
		}
	}
}

//TODO: add looping toggle to anim groups and anim clips
void updateSyncGroupsAndAnimClipTimes(animation_tree_blueprint* blueprint, am_model* model, real32 dtf)
{
	uint32 syncGroupCount = blueprint->maxSyncGroupCount;
	sync_group_blueprint* curGroup = blueprint->syncGroups;
	for (uint32 syncGroupIndex = 0; syncGroupIndex < syncGroupCount; ++syncGroupIndex, ++curGroup)
	{
		if (curGroup->totalNodeRatio > FLT_EPSILON && curGroup->totalAnimLength > FLT_EPSILON)
		{
			real32 groupDuration = curGroup->totalAnimLength / curGroup->totalNodeRatio;
			curGroup->animRatio += dtf / groupDuration;
			curGroup->animRatio = fmodf(curGroup->animRatio, 1.0f);
		}
		else
		{
			curGroup->animRatio = 0.0f;
		}
	}

	recursiveUpdateSyncGroupsAndAnimClipTimes(blueprint->outputNodeHandle, blueprint, model, dtf);
}

void recursiveProcessNode(atbNodeHandle handle, animation_tree_blueprint* blueprint, am_model* model, am_transform_buffer* resultBuffers, stack_memory_manager* tempMemory)
{
	if (handle > 0)
	{
		uint32 nodeIndex = handle - 1;
		switch (blueprint->nodeTypes[nodeIndex])
		{
			case AnimationNodeType_Pose:
				{
				pose_node_blueprint* node = (pose_node_blueprint*) blueprint->nodeDatas[nodeIndex];
				node->processNode(resultBuffers + nodeIndex, node, handle, blueprint, model, resultBuffers, tempMemory);
			}
			break;
			case AnimationNodeType_Clip:
				{
				clip_node_blueprint* node = (clip_node_blueprint*) blueprint->nodeDatas[nodeIndex];
				node->processNode(resultBuffers + nodeIndex, node, handle, blueprint, model, resultBuffers, tempMemory);
			}
			break;
			case AnimationNodeType_Blender2:
				{
				blend2_node_blueprint* node = (blend2_node_blueprint*) blueprint->nodeDatas[nodeIndex];
				recursiveProcessNode(node->input[0], blueprint, model, resultBuffers, tempMemory);
				recursiveProcessNode(node->input[1], blueprint, model, resultBuffers, tempMemory);
				node->processNode(resultBuffers + nodeIndex, node, handle, blueprint, model, resultBuffers, tempMemory);
			}
			break;
			case AnimationNodeType_Blender3:
				{
				blend3_node_blueprint* node = (blend3_node_blueprint*) blueprint->nodeDatas[nodeIndex];
				recursiveProcessNode(node->input[0], blueprint, model, resultBuffers, tempMemory);
				recursiveProcessNode(node->input[1], blueprint, model, resultBuffers, tempMemory);
				recursiveProcessNode(node->input[2], blueprint, model, resultBuffers, tempMemory);
				node->processNode(resultBuffers + nodeIndex, node, handle, blueprint, model, resultBuffers, tempMemory);
			}
			break;
		}
	}
}

void animation_tree_blueprint::processTree(am_transform_buffer* dst, am_model* model, stack_memory_manager* tempMemory, real32 fdt)
{
	Assert(dst && model && tempMemory);
	Assert(dst->transforms && dst->nodeCount == model->nodes.nodeCount);
	if (outputNodeHandle > 0)
	{
		TEMP_MEMORY_BLOCK(tempMemory);
		uint32 bufferCount = maxAnimNodeCount;
		uint32 sceneNodeCount = model->nodes.nodeCount;

		am_transform_buffer *resultBuffers = pushStructArray(tempMemory, am_transform_buffer, bufferCount);

		for (uint32 bufferIndex = 0; bufferIndex < bufferCount; bufferIndex++)
		{
			if (nodeTypes[bufferIndex] != AnimationNodeType_None)
			{
				resultBuffers[bufferIndex].nodeCount = maxAnimNodeCount;
				resultBuffers[bufferIndex].transforms = pushStructArray(tempMemory, am_transform, sceneNodeCount);
			}
			else
			{
				resultBuffers[bufferIndex].nodeCount = 0;
				resultBuffers[bufferIndex].transforms = NULL;
			}
		}

		calculateNodeRatios(this, model);
		updateSyncGroupsAndAnimClipTimes(this, model, fdt);
		recursiveProcessNode(outputNodeHandle, this, model, resultBuffers, tempMemory);

		memcpy(dst->transforms, resultBuffers[outputNodeHandle - 1].transforms, sizeof(am_transform) * sceneNodeCount);
	}
	else
	{
		memcpy(dst->transforms, model->nodes.transforms, sizeof(am_transform) * model->nodes.nodeCount);
	}
}

enum pose_node_type
{
	pntReferencePose = 0,

	/*Do not change this*/
	pntPoseCount,
};

enum ant_node_type
{
	antWalkForwardAnim = 0,
	antStrafeRightAnim,
	antStrafeLeftAnim,
	antIdleAnim,
	antTailWagAnim,

	/*Do not change this*/
	antAnimCount,
};

enum blend_node_type
{
	bntWalkBlend = 0,
	bntIdleBlend,
	bntTailWagBlend,

	/*Do not change this*/
	bntBlendCount
};

struct ui_state
{
	vec2f walkBlendSelectorPos;
	real32 idleBlendSelectorPos;
	real32 tailWagBlendSelectorPos;
	bool32 isInBoneSelectionMode;
};

#define MAX_TEMP_LOG_LINE_LENGTH 2048
#define MAX_LOG_LINE_LENGTH 256
typedef char log_line[MAX_LOG_LINE_LENGTH];

struct debug_logger
{
	log_line *lines;
	char *tempLogLine; //large buffer so we can break the line up if needed
	uint32 lineStart;
	uint32 lineEnd;
	uint32 lineCount;
	bool windowOpen;
	bool hasAddedLog;

	void init(uint32 maxLineCount, stack_memory_manager* memory)
	{
		uint64 prevUsed = memory->memoryUsed;
		tempLogLine = pushStructArray(memory, char, MAX_TEMP_LOG_LINE_LENGTH);
		lines = pushStructArray(memory, log_line, maxLineCount);
		lineStart = 0;
		lineEnd = 0;
		windowOpen = false;
		hasAddedLog = false;
		if (tempLogLine && lines)
		{
			lineCount = maxLineCount;
			memset(tempLogLine, 0, sizeof(char) * MAX_TEMP_LOG_LINE_LENGTH);
			memset(lines, 0, sizeof(log_line) * maxLineCount);
		}
		else
		{
			lineCount = 0;
			memory->memoryUsed = prevUsed;
		}
	}

	void log(const char* format, ...)
	{
		va_list args;

		va_start(args,format);
		int32 writtenLength = vsnprintf_s(tempLogLine, MAX_TEMP_LOG_LINE_LENGTH, _TRUNCATE, format, args);
		int32 lengthLeft = writtenLength;
		char* logLine = tempLogLine;

		while (lengthLeft > 0)
		{
			//TODO: calculate new head and tail
			uint32 lineLength = min(lengthLeft, MAX_LOG_LINE_LENGTH-1);
			memcpy(lines[lineEnd], logLine, lineLength);
			printf(lines[lineEnd]);
			printf("\n");
			lines[lineEnd][lineLength] = 0;
			lineEnd = (lineEnd + 1) % lineCount;
			lengthLeft -= lineLength;
			if (lineEnd == lineStart)
			{
				lineStart = (lineStart + 1) % lineCount;
			}
		}
		hasAddedLog = true;
		va_end(args);
	}

	void logOpenGLError(const char* tag = NULL)
	{
		GLenum error = GL_NO_ERROR;
		while ((error = glGetError()) != GL_NO_ERROR)
		{
			log("OpenGL Error: %s at %s\n", gluErrorString(error), tag);
		}
	}

	void display(window_state window)
	{
		ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f), ImGuiSetCond_Always);
		ImGui::SetNextWindowSize(ImVec2((real32) window.width, (real32) 100), ImGuiSetCond_Once);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowMinSize, ImVec2((real32) window.width, 100.0f));
		if (ImGui::Begin("Log", &windowOpen, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoBringToFrontOnFocus))
		{
			ImVec2 winSize = ImGui::GetWindowSize();
			winSize.x = (real32) window.width;
			if (winSize.y > window.height * 0.5f)
			{
				winSize.y = window.height * 0.5f;
			}
			ImGui::SetWindowSize(winSize, ImGuiSetCond_Always);

			for (uint32 curPos = lineStart; curPos != lineEnd; curPos = (curPos + 1) % lineCount)
			{
				ImGui::TextUnformatted(lines[curPos]);
			}
			if (hasAddedLog)
			{
				hasAddedLog = false;
				ImGui::SetScrollHere();
			}
		}
		ImGui::End();
		ImGui::PopStyleVar(1);
	}
};

struct debug_renderer
{
	vec3f *vertices;
	vec4f *colors;

	vec3f *pointVertices;
	vec4f *pointColors;

	vec3f *lineVertices;
	vec4f *lineColors;

	vec3f *triangleVertices;
	vec4f *triangleColors;

	uint32 pointIndex;
	uint32 lineIndex;
	uint32 triangleIndex;

	uint32 pointOffset;
	uint32 lineOffset;
	uint32 triangleOffset;

	uint32 maxElements;

	void addPoint(vec3f vert, vec4f color = vec4f(1.0f, 0.0f, 0.0f, 1.0f))
	{
		pointVertices[pointIndex] = vert;
		pointColors[pointIndex] = color;
		pointIndex = (pointIndex + 1) % maxElements;
	}

	void addLine(vec3f vert1, vec3f vert2, vec4f color = vec4f(1.0f, 0.0f, 0.0f, 1.0f))
	{
		lineVertices[lineIndex * 2] = vert1;
		lineVertices[lineIndex * 2 + 1] = vert2;
		lineColors[lineIndex * 2] = color;
		lineColors[lineIndex * 2 + 1] = color;
		lineIndex = (lineIndex + 1) % maxElements;
	}

	void addTriangle(vec3f vert1, vec3f vert2, vec3f vert3, vec4f color = vec4f(1.0f, 0.0f, 0.0f, 1.0f))
	{
		triangleVertices[triangleIndex * 3] = vert1;
		triangleVertices[triangleIndex * 3 + 1] = vert2;
		triangleVertices[triangleIndex * 3 + 2] = vert2;
		triangleColors[triangleIndex * 3] = color;
		triangleColors[triangleIndex * 3 + 1] = color;
		triangleColors[triangleIndex * 3 + 2] = color;
		triangleIndex = (triangleIndex + 1) % maxElements;
	}

	bool32 setup(uint32 maxElementsPerType, stack_memory_manager *memory)
	{
		uint64 oldMemoryUsed = memory->memoryUsed;

		vertices = pushStructArray(memory, vec3f, maxElementsPerType * 6);
		colors = pushStructArray(memory, vec4f, maxElementsPerType * 6);

		if (vertices && colors)
		{
			pointOffset = 0;
			lineOffset = maxElementsPerType;
			triangleOffset = lineOffset + maxElementsPerType * 2;

			pointVertices = vertices + pointOffset;
			pointColors = colors + pointOffset;

			lineVertices = vertices + lineOffset;
			lineColors = colors + lineOffset;

			triangleVertices = vertices + triangleOffset;
			triangleColors = colors + triangleOffset;

			maxElements = maxElementsPerType;
			clear();
			return true;
		}
		else
		{
			memory->memoryUsed = oldMemoryUsed;
			maxElements = 0;
			vertices = 0;
			colors = 0;
			return false;
		}
	}
	void clear()
	{
		memset(vertices, 0, sizeof(vec3f) * maxElements * 6);
		memset(colors, 0, sizeof(vec4f) * maxElements * 6);
	}

	void render()
	{
		glLineWidth(2.0f);
		glPointSize(5.0f);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);

		glVertexPointer(3, GL_FLOAT, 0, vertices);
		glColorPointer(4, GL_FLOAT, 0, colors);
		glDrawArrays(GL_POINTS, pointOffset, maxElements);
		glDrawArrays(GL_LINES, lineOffset, maxElements * 2);
		glDrawArrays(GL_TRIANGLES, triangleOffset, maxElements * 3);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);

		glLineWidth(1.0f);
		glPointSize(1.0f);
	}
};

struct app_state
{
	bool32 isInitialized;
	stack_memory_manager memory;

	stack_memory_manager resourceMemory;
	stack_memory_manager tempMemory;

	game_camera_3d camera;

	am_model* model;

	debug_renderer debugRenderer;
	debug_logger debugLogger;

	bool32 animationUpdateEnabled;
	int32 poseNodeClip[pntPoseCount];
	int32 animNodeClip[antAnimCount];
	real32 animNodeTime[antAnimCount];

	am_texture_manager* textureManager;

	ui_state uiState;

	vec3f walkBlendUVW;
	real32 idleWalkBlendRatio;
	real32 tailWagBlendRatio;

	am_transform_buffer poseNodeBuffers[pntPoseCount];
	am_transform_buffer animNodeBuffers[antAnimCount];
	am_transform_buffer blendNodeBuffers[bntBlendCount];

	bool32 *tailWagBlendMask;

	animation_tree_blueprint treeBlueprint;

	AmuiBlueprint* uiBlueprint;
};

void expandNodeUI(uint32 nodeIndex, am_model* model)
{
	am_node_info *nodeInfo = model->nodes.info + nodeIndex;
	if (ImGui::TreeNode(nodeInfo, "Node %s", model->nodes.names[nodeIndex].name))
	{
		if (nodeInfo->meshRefCount)
		{
			uint32 meshRefCount = nodeInfo->meshRefCount;
			uint32* meshRef = model->nodes.meshIds + nodeInfo->meshRefStart;

			for (uint32 meshRefIndex = 0; meshRefIndex < meshRefCount; meshRefIndex++)
			{
				if (ImGui::TreeNode((void*) meshRef, "Mesh"))
				{
					am_mesh* curMesh = model->meshes + *meshRef;
					ImGui::Text("Vertices: %d", curMesh->vertexCount);
					ImGui::Text("Triangles: %d", curMesh->triangleCount);
					if (curMesh->boneCount)
					{
						ImGui::Text("Skinned Bones: %d", curMesh->boneCount);
					}
					ImGui::TreePop();
				}
			}
		}

		for (uint32 childIndex = 0; childIndex < nodeInfo->childrenCount; childIndex++)
		{
			expandNodeUI(nodeInfo->childrenStart + childIndex, model);
		}
		ImGui::TreePop();
	}
}

void SIMD_mulMatrix(__m128 *dstMat, const __m128 *mat, float multiplier)
{
//Note: matrix has to be 16 byte aligned

	__m128 simdMultiplier = _mm_set_ps1(multiplier);

	dstMat[0] = _mm_mul_ps(mat[0], simdMultiplier);
	dstMat[1] = _mm_mul_ps(mat[1], simdMultiplier);
	dstMat[2] = _mm_mul_ps(mat[2], simdMultiplier);
	dstMat[3] = _mm_mul_ps(mat[3], simdMultiplier);
}

void SIMD_addMatrix(__m128* dstMat, const __m128* matA, const __m128 *matB)
{
	dstMat[0] = _mm_add_ps(matA[0],matB[0]);
	dstMat[1] = _mm_add_ps(matA[1],matB[1]);
	dstMat[2] = _mm_add_ps(matA[2],matB[2]);
	dstMat[3] = _mm_add_ps(matA[3],matB[3]);
}

void SIMD_addMulMatrix(__m128 *dstMat, const __m128 *matA, const __m128 *matB, float multiplierB)
{
	__m128 simdMultiplier = _mm_set_ps1(multiplierB);

	dstMat[0] = _mm_add_ps(matA[0], _mm_mul_ps(matB[0], simdMultiplier));
	dstMat[1] = _mm_add_ps(matA[1], _mm_mul_ps(matB[1], simdMultiplier));
	dstMat[2] = _mm_add_ps(matA[2], _mm_mul_ps(matB[2], simdMultiplier));
	dstMat[3] = _mm_add_ps(matA[3], _mm_mul_ps(matB[3], simdMultiplier));
}

void SIMD_mulVector(vec3f* dstVec, const __m128* mat, const vec3f* srcVec)
{
	__m128 simdVecX = _mm_set_ps1(srcVec->x);
	__m128 simdVecY = _mm_set_ps1(srcVec->y);
	__m128 simdVecZ = _mm_set_ps1(srcVec->z);
	__m128 simdVecW = _mm_set_ps1(1.0f);

	__m128 column0 = _mm_mul_ps(mat[0],simdVecX);
	__m128 column1 = _mm_mul_ps(mat[1],simdVecY);
	__m128 column2 = _mm_mul_ps(mat[2],simdVecZ);
	__m128 column3 = _mm_mul_ps(mat[3],simdVecW);

	//_MM_TRANSPOSE4_PS(column0,column1,column2,column3);
	__m128 total = _mm_add_ps(_mm_add_ps(column0,column1), _mm_add_ps(column2,column3));

	dstVec->x = total.m128_f32[0];
	dstVec->y = total.m128_f32[1];
	dstVec->z = total.m128_f32[2];

	/*
	 x = m00*v.x + m01*v.y + m02*v.z + m03,
	 y = m10*v.x + m11*v.y + m12*v.z + m13,
	 z = m20*v.x + m21*v.y + m22*v.z + m23);
	 */
}

void SIMD_skin(__m128 *dstMat, __m128 *mat0, __m128 *mat1, __m128 *mat2, __m128 *mat3, real32 weight0, real32 weight1, real32 weight2, real32 weight3)
{
	__m128 simdMultiplier0 = _mm_set_ps1(weight0);
	__m128 simdMultiplier1 = _mm_set_ps1(weight1);
	__m128 simdMultiplier2 = _mm_set_ps1(weight2);
	__m128 simdMultiplier3 = _mm_set_ps1(weight3);

	/*
	 dstMat[0] = _mm_add_ps(
	 _mm_add_ps(_mm_mul_ps(mat0[0],simdMultiplier0),_mm_mul_ps(mat1[0],simdMultiplier1)),
	 _mm_add_ps(_mm_mul_ps(mat2[0],simdMultiplier0),_mm_mul_ps(mat2[0],simdMultiplier1))
	 );
	 dstMat[1] = _mm_add_ps(
	 _mm_add_ps(_mm_mul_ps(mat0[1],simdMultiplier0),_mm_mul_ps(mat1[1],simdMultiplier1)),
	 _mm_add_ps(_mm_mul_ps(mat2[1],simdMultiplier0),_mm_mul_ps(mat2[1],simdMultiplier1))
	 );
	 dstMat[2] = _mm_add_ps(
	 _mm_add_ps(_mm_mul_ps(mat0[2],simdMultiplier0),_mm_mul_ps(mat1[2],simdMultiplier1)),
	 _mm_add_ps(_mm_mul_ps(mat2[2],simdMultiplier0),_mm_mul_ps(mat2[2],simdMultiplier1))
	 );
	 dstMat[3] = _mm_add_ps(
	 _mm_add_ps(_mm_mul_ps(mat0[3],simdMultiplier0),_mm_mul_ps(mat1[3],simdMultiplier1)),
	 _mm_add_ps(_mm_mul_ps(mat2[3],simdMultiplier0),_mm_mul_ps(mat2[3],simdMultiplier1))
	 );
	 */

	dstMat[0] = _mm_mul_ps(mat0[0], simdMultiplier0);
	dstMat[1] = _mm_mul_ps(mat0[1], simdMultiplier0);
	dstMat[2] = _mm_mul_ps(mat0[2], simdMultiplier0);
	dstMat[3] = _mm_mul_ps(mat0[3], simdMultiplier0);

	dstMat[0] = _mm_add_ps(dstMat[0], _mm_mul_ps(mat1[0], simdMultiplier1));
	dstMat[1] = _mm_add_ps(dstMat[1], _mm_mul_ps(mat1[1], simdMultiplier1));
	dstMat[2] = _mm_add_ps(dstMat[2], _mm_mul_ps(mat1[2], simdMultiplier1));
	dstMat[3] = _mm_add_ps(dstMat[3], _mm_mul_ps(mat1[3], simdMultiplier1));

	dstMat[0] = _mm_add_ps(dstMat[0], _mm_mul_ps(mat2[0], simdMultiplier2));
	dstMat[1] = _mm_add_ps(dstMat[1], _mm_mul_ps(mat2[1], simdMultiplier2));
	dstMat[2] = _mm_add_ps(dstMat[2], _mm_mul_ps(mat2[2], simdMultiplier2));
	dstMat[3] = _mm_add_ps(dstMat[3], _mm_mul_ps(mat2[3], simdMultiplier2));

	dstMat[0] = _mm_add_ps(dstMat[0], _mm_mul_ps(mat3[0], simdMultiplier3));
	dstMat[1] = _mm_add_ps(dstMat[1], _mm_mul_ps(mat3[1], simdMultiplier3));
	dstMat[2] = _mm_add_ps(dstMat[2], _mm_mul_ps(mat3[2], simdMultiplier3));
	dstMat[3] = _mm_add_ps(dstMat[3], _mm_mul_ps(mat3[3], simdMultiplier3));
}

#define ANIMASTER_DEFAULT_UI_SNAP 10
extern float roundf(float);

void snapCurrentImguiWindow(real32 screenWidth, real32 screenHeight, bool32 clampToScreenEdges = true, real32 snap = ANIMASTER_DEFAULT_UI_SNAP)
{
	//TODO: modify imgui window to enable snapping

	ImVec2 pos = ImGui::GetWindowPos();
	ImVec2 size = ImGui::GetWindowSize();

	if (snap > FLT_EPSILON)
	{
		pos.x = roundf(pos.x / snap) * snap;
		pos.y = roundf(pos.y / snap) * snap;

		size.x = roundf(size.x / snap) * snap;
		size.y = roundf(size.y / snap) * snap;
	}

	if (clampToScreenEdges)
	{
		size.x = minf(screenWidth, size.x);
		size.y = minf(screenWidth, size.y);

		pos.x = maxf(0.0f, minf(screenWidth - size.x, pos.x));
		pos.y = maxf(0.0f, minf(screenHeight - size.y, pos.y));
	}

	if (ImGui::IsWindowBeingMoved())
	{
		//TODO: draw a rectangle of where the window will be clipped
		/*
		 ImVec2 min = pos;
		 ImVec2 max=ImVec2(pos.x+size.x,pos.y+size.y);
		 ImDrawList* drawList = ImGui::GetWindowDrawList();
		 drawList->AddCallback()
		 ImVec4 prevClipRect = drawList->_ClipRectStack;
		 drawList->AddRect(min,max,0xffffff80);
		 */
	}
	else
	{
		ImGui::SetWindowPos(pos, ImGuiSetCond_Always);
	}
}

void UpdateAnimaster(void* memoryBuffer, uint64 memoryBufferSize, window_state window, input_state* input, uint32 frameDif)
{
	sProfiler.beginFrame();

	app_state *appState = AlignCastPtr(memoryBuffer, app_state);
	if (!appState->isInitialized)
	{
		appState->isInitialized = true;
		appState->memory.memory = (uint8*) appState + sizeof(app_state);
		appState->memory.memorySize = (uint64) memoryBuffer + memoryBufferSize - ((uint64) appState->memory.memory);

		printf("Memory Size: %d\n", appState->memory.memorySize);

		appState->resourceMemory.memory = (uint8*) appState->memory.push(Megabytes(50), 16);
		appState->resourceMemory.memorySize = Megabytes(50);
		if (!appState->resourceMemory.memory)
		{
			ShowError("Not enough memory: Failed to allocate space for resources");
			return;
		}

		appState->tempMemory.memory = (uint8*) appState->memory.push(Megabytes(50), 16);
		appState->tempMemory.memorySize = Megabytes(50);
		if (!appState->tempMemory.memory)
		{
			ShowError("Not enough memory: Failed to allocate space for temp operations");
			return;
		}

		appState->textureManager = am_texture_manager::create(1024, &appState->resourceMemory, Megabytes(25));

		if (!appState->textureManager)
		{
			ShowError("Not enough memory: Failed to allocate space for texture manager");
			return;
		}

		appState->camera.farClip = 1000.0f;
		appState->camera.nearClip = 0.1f;
		appState->camera.pos.set(0.0f, 0.0f, 3.0f);
		appState->camera.rotPitch = 0.0f;
		appState->camera.rotYaw = 0.0f;
		appState->camera.fov = 90.0f;

		appState->debugRenderer.setup(30, &appState->memory);
		appState->debugLogger.init(100, &appState->memory);

		appState->model = amLoadModel("data/dragon.fbx", &appState->resourceMemory, &appState->tempMemory, appState->textureManager);

		matrix4x4f matrix = appState->camera.getInvTransform();

		for (uint32 animIndex = 0; animIndex < antAnimCount; animIndex++)
		{
			appState->animNodeTime[animIndex] = 0;
		}

		if (appState->model)
		{
			uint32 animCount = appState->model->animation.animCount;

			appState->poseNodeClip[pntReferencePose] = amFindAnimation(appState->model->animation.names, appState->model->animation.animCount, "Pose_Ref");

			appState->animNodeClip[antWalkForwardAnim] = amFindAnimation(appState->model->animation.names, appState->model->animation.animCount, "Walk_strafe_right");
			appState->animNodeClip[antStrafeRightAnim] = amFindAnimation(appState->model->animation.names, appState->model->animation.animCount, "Walk_strafe_left");
			appState->animNodeClip[antStrafeLeftAnim] = amFindAnimation(appState->model->animation.names, appState->model->animation.animCount, "Walk_forward");
			appState->animNodeClip[antIdleAnim] = amFindAnimation(appState->model->animation.names, appState->model->animation.animCount, "Idle");
			appState->animNodeClip[antTailWagAnim] = amFindAnimation(appState->model->animation.names, appState->model->animation.animCount, "Tail_wag");

			uint32 nodeCount = appState->model->nodes.nodeCount;

			for (uint32 bufferIndex = 0; bufferIndex < pntPoseCount; bufferIndex++)
			{
				appState->poseNodeBuffers[bufferIndex].nodeCount = nodeCount;
				appState->poseNodeBuffers[bufferIndex].transforms = pushStructArray(&appState->resourceMemory, am_transform, nodeCount);
				memcpy(appState->poseNodeBuffers[bufferIndex].transforms, appState->model->nodes.transforms, sizeof(am_transform) * nodeCount);

				if (appState->poseNodeClip[bufferIndex] >= 0)
				{
					am_anim_clip_info_packed packedAnimInfo = amGetPackedAnimationClipInfo(appState->model, appState->poseNodeClip[bufferIndex]);
					amCalculatePoseIntoBuffer(&appState->poseNodeBuffers[bufferIndex], &packedAnimInfo);
				}
			}

			for (uint32 bufferIndex = 0; bufferIndex < antAnimCount; bufferIndex++)
			{
				appState->animNodeBuffers[bufferIndex].nodeCount = nodeCount;
				appState->animNodeBuffers[bufferIndex].transforms = pushStructArray(&appState->resourceMemory, am_transform, nodeCount);
				memcpy(appState->animNodeBuffers[bufferIndex].transforms, appState->model->nodes.transforms, sizeof(am_transform) * nodeCount);
			}

			for (uint32 bufferIndex = 0; bufferIndex < bntBlendCount; bufferIndex++)
			{
				appState->blendNodeBuffers[bufferIndex].nodeCount = nodeCount;
				appState->blendNodeBuffers[bufferIndex].transforms = pushStructArray(&appState->resourceMemory, am_transform, nodeCount);
				memcpy(appState->blendNodeBuffers[bufferIndex].transforms, appState->model->nodes.transforms, sizeof(am_transform) * nodeCount);
			}

			appState->tailWagBlendMask = pushStructArray(&appState->resourceMemory, bool32, nodeCount);
			ZERO_STRUCT_ARRAY(bool32, appState->tailWagBlendMask, nodeCount);

			const char* maskEnableNames[] = { "Tail1", "Tail2", "Tail3", "Tail4", "Tail5", "Tail6", "Tail7", "Tail8" };

			for (uint32 maskNodeIndex = 0; maskNodeIndex < ARRAY_LENGTH(maskEnableNames); ++maskNodeIndex)
			{
				uint32 nodeIndex = amFindNode(appState->model, maskEnableNames[maskNodeIndex]);
				if (nodeIndex != ANIMASTER_NODE_INVALID_INDEX)
				{
					appState->tailWagBlendMask[nodeIndex] = true;
				}
			}

			appState->animationUpdateEnabled = true;

			uint32 animTreeBlueprintMemorySize = Megabytes(2);
			void* animTreeBlueprintMemory = appState->memory.push(animTreeBlueprintMemorySize, 16);
			appState->treeBlueprint.init(animTreeBlueprintMemory, animTreeBlueprintMemorySize, 200, 20);

			appState->treeBlueprint.createAnimNode(AnimationNodeType_Clip, 50, 50);
			appState->treeBlueprint.createAnimNode(AnimationNodeType_Clip, 50, 150);
			appState->treeBlueprint.createAnimNode(AnimationNodeType_Clip, 50, 250);
			appState->treeBlueprint.createAnimNode(AnimationNodeType_Clip, 50, 350);

			appState->treeBlueprint.createAnimNode(AnimationNodeType_Pose, 50, 450);

			appState->treeBlueprint.createAnimNode(AnimationNodeType_Blender3, 350, 50);

			appState->treeBlueprint.createAnimNode(AnimationNodeType_Blender2, 350, 350);

			appState->uiBlueprint = new AmuiBlueprint();
			appState->uiBlueprint->Reset();
		}
	}

	real32 windowWidth = (real32) window.width;
	real32 windowHeight = (real32) window.height;

	debug_renderer &debug = appState->debugRenderer;
	debug_logger &logger = appState->debugLogger;

	TEMP_MEMORY_BLOCK(&appState->memory);
	real32 dtf = frameDif * 0.001f;
	am_model* model = appState->model;

	vec2f mousePos = { input->mouse.x, input->mouse.y };

	matrix4x4f* nodeWorldSpaceMatrices = NULL;
	vec3f *nodeWorldSpacePositions = NULL;

	{
		PROFILE_BLOCK("UI");
		ui_state &uiState = appState->uiState;
		ImGuiIO &io = ImGui::GetIO();

		logger.display(window);

		if (ImGui::Begin("Blend Control", NULL, ImGuiWindowFlags_NoResize))
		{
			snapCurrentImguiWindow(windowWidth, windowHeight);

			bool animEnabled = appState->animationUpdateEnabled;
			if (ImGui::ToggleButton("Animation Play", &animEnabled))
			{
				appState->animationUpdateEnabled = animEnabled;
			}
			ImGui::Spacing();

			real32 width = 100.0f;
			real32 height = 100.0f;

			vec2f mousePos = io.MousePos;
			vec2f selectorMin = ImGui::GetCursorScreenPos();
			vec2f selectorMax(selectorMin.x + width, selectorMin.y + height);

			ImDrawList* drawList = ImGui::GetWindowDrawList();
			vec2f triangularBlendPoints[3] = {

			selectorMin + vec2f { width * 1.0f, height * 1.0f },
					selectorMin + vec2f(width * 0.0f, height * 1.0f),
					selectorMin + vec2f { width * 0.5f, height * 0.0f },
			};
			ImGui::InvisibleButton("WalkSelector", ImVec2(100.0f, 100.0f));
			if (ImGui::IsItemActive())
			{
				appState->walkBlendUVW = cartesianToBarycentric(mousePos, triangularBlendPoints[0], triangularBlendPoints[1], triangularBlendPoints[2]);
				uiState.walkBlendSelectorPos = barycentricToCartesian(appState->walkBlendUVW, triangularBlendPoints[0], triangularBlendPoints[1], triangularBlendPoints[2]) - selectorMin; //mousePos - selectorMin;
			}

			if (ImGui::IsItemHovered())
			{
				drawList->AddRectFilled(selectorMin, selectorMax, 0xffaaaaaa);
			}
			else
			{
				drawList->AddRectFilled(selectorMin, selectorMax, 0xff888888);
			}

			drawList->PushClipRect(selectorMin, selectorMax);

			drawList->AddPolyline((ImVec2*) triangularBlendPoints, 3, 0xffffffff, true, 2, false);

			vec2f selectorWorldPos = uiState.walkBlendSelectorPos + selectorMin;
			drawList->AddCircle(selectorWorldPos, 3, 0xff000000, 8, 2);

			drawList->PopClipRect();

			ImGui::Text("UVW: %f, %f,%f", appState->walkBlendUVW.x, appState->walkBlendUVW.y, appState->walkBlendUVW.z);

			ImGui::Separator();
			if (ImGui::SliderFloat("Idle Ratio", &uiState.idleBlendSelectorPos, 0.0f, 1.0f))
			{
				appState->idleWalkBlendRatio = uiState.idleBlendSelectorPos;
			}

			ImGui::Separator();

			ImGui::Text("Additive");

			int32 selectedAdditiveAnim = appState->animNodeClip[antTailWagAnim] + 1;
			if (ImGui::Combo("Animation", &selectedAdditiveAnim, modelAnimClipNameGetterForUI, appState->model, appState->model->animation.animCount + 1))
			{
				int32 selectedAnimation = selectedAdditiveAnim - 1;
				if (selectedAnimation >= 0 && (uint32) selectedAnimation < appState->model->animation.animCount)
				{
					appState->animNodeClip[antTailWagAnim] = selectedAnimation;
				}
				else
				{
					appState->animNodeClip[antTailWagAnim] = -1;
				}
				memcpy(appState->animNodeBuffers[antTailWagAnim].transforms, appState->model->nodes.transforms, sizeof(am_transform) * appState->model->nodes.nodeCount);
			}

			int32 selectedAdditivePoseRef = appState->poseNodeClip[pntReferencePose] + 1;

			if (ImGui::Combo("Ref Pose", &selectedAdditivePoseRef, modelAnimClipNameGetterForUI, appState->model, appState->model->animation.animCount + 1))
			{
				int32 selectedPose = selectedAdditivePoseRef - 1;
				if (selectedPose >= 0 && (uint32) selectedPose < appState->model->animation.animCount)
				{
					appState->poseNodeClip[pntReferencePose] = selectedPose;
					memcpy(appState->poseNodeBuffers[pntReferencePose].transforms, appState->model->nodes.transforms, sizeof(am_transform) * appState->model->nodes.nodeCount);

					am_anim_clip_info_packed packedAnimInfo = amGetPackedAnimationClipInfo(appState->model, appState->poseNodeClip[pntReferencePose]);
					amCalculatePoseIntoBuffer(&appState->poseNodeBuffers[pntReferencePose], &packedAnimInfo);
				}
				else
				{
					appState->poseNodeClip[pntReferencePose] = -1;
					memcpy(appState->poseNodeBuffers[pntReferencePose].transforms, appState->model->nodes.transforms, sizeof(am_transform) * appState->model->nodes.nodeCount);
				}
			}

			if (ImGui::SliderFloat("Ratio", &uiState.tailWagBlendSelectorPos, 0.0f, 1.0f))
			{
				appState->tailWagBlendRatio = uiState.tailWagBlendSelectorPos;
			}

			bool selected = uiState.isInBoneSelectionMode;
			if (ImGui::ToggleButton("Show Bone Mask Overlay", &selected))
			{
				uiState.isInBoneSelectionMode = selected;
			}

		}
		ImGui::End();

		if (ImGui::Begin("Blueprint", NULL))
		{
			appState->treeBlueprint.displayUI(appState->model, &appState->tempMemory);
		}
		ImGui::End();

		if (ImGui::Begin("Profiler", NULL))
		{
			snapCurrentImguiWindow(windowWidth, windowHeight);
		}
		ImGui::End();

		if (ImGui::Begin("Info"))
		{
			snapCurrentImguiWindow(windowWidth, windowHeight);
			if (ImGui::TreeNode("Model"))
			{

				uint32 nodeCount = model->nodes.nodeCount;
				am_node_info* nodeInfo = model->nodes.info;

				ImGui::Text("Node Count: %d", nodeCount);
				if (nodeCount > 0)
				{
					expandNodeUI(0, model);
				}
				ImGui::TreePop();
			}
		}
		ImGui::End();

		appState->uiBlueprint->Update();
	}

	if (model && model->nodes.nodeCount > 0)
	{
		PROFILE_BLOCK("Blending");

		uint32 matrixCount = model->nodes.nodeCount;

		matrix4x4f* nodeFinalLocalMatrices = NULL;

		nodeFinalLocalMatrices = pushStructArrayAlign(&appState->tempMemory, matrix4x4f, matrixCount, 16);
		nodeWorldSpaceMatrices = pushStructArrayAlign(&appState->tempMemory, matrix4x4f, matrixCount, 16);

		memcpy(nodeFinalLocalMatrices, model->nodes.matrices, matrixCount * sizeof(matrix4x4f));

		uint32 nodeCount = model->nodes.nodeCount;
		bool32 useOldTree = false;

		am_transform_buffer finalAnimTreeBuffer;
		finalAnimTreeBuffer.nodeCount = nodeCount;
		finalAnimTreeBuffer.transforms = pushStructArray(&appState->tempMemory, am_transform, nodeCount);

		if (useOldTree)
		{
			bool32 walkAnimOk = true;

			if (appState->animationUpdateEnabled)
			{
				appState->animNodeTime[antWalkForwardAnim] += dtf;
				appState->animNodeTime[antStrafeRightAnim] += dtf;
				appState->animNodeTime[antStrafeLeftAnim] += dtf;
				appState->animNodeTime[antIdleAnim] += dtf;
				appState->animNodeTime[antTailWagAnim] += dtf;
			}
			uint32 usedBufferIndex = 0;

			am_transform_buffer* poseNodeBuffer = appState->poseNodeBuffers;
			am_transform_buffer* animNodeBuffer = appState->animNodeBuffers;
			am_transform_buffer* blendNodeBuffer = appState->blendNodeBuffers;

			am_transform_buffer& finalAnimTreeBuffer = blendNodeBuffer[bntTailWagBlend];

			for (uint32 animIndex = 0; animIndex < antAnimCount; animIndex++)
			{
				if (appState->animNodeClip[animIndex] >= 0)
				{
					am_anim_clip_info_packed packedAnimInfo = amGetPackedAnimationClipInfo(appState->model, appState->animNodeClip[animIndex]);
					amCalculateAnimatedPoseIntoBuffer(&animNodeBuffer[animIndex], &packedAnimInfo, appState->animNodeTime[animIndex]);
				}
			}

			real32 walkForwardRatio = appState->walkBlendUVW.x;
			real32 walkRightRatio = appState->walkBlendUVW.y;

			for (uint32 nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
			{
				amBlendTransform(blendNodeBuffer[bntWalkBlend].transforms[nodeIndex], animNodeBuffer[antStrafeLeftAnim].transforms[nodeIndex], animNodeBuffer[antStrafeRightAnim].transforms[nodeIndex],
						walkRightRatio);
				amBlendTransform(blendNodeBuffer[bntWalkBlend].transforms[nodeIndex], blendNodeBuffer[bntWalkBlend].transforms[nodeIndex], animNodeBuffer[antWalkForwardAnim].transforms[nodeIndex],
						walkForwardRatio);
			}

			real32 idleRatio = appState->idleWalkBlendRatio;

			for (uint32 nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
			{
				amBlendTransform(blendNodeBuffer[bntIdleBlend].transforms[nodeIndex], blendNodeBuffer[bntWalkBlend].transforms[nodeIndex], animNodeBuffer[antIdleAnim].transforms[nodeIndex],
						idleRatio);
			}

			real32 tailWagRatio = appState->tailWagBlendRatio;
			for (uint32 nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
			{
				if (appState->tailWagBlendMask[nodeIndex])
				{
					amBlendAdditiveTransform(
							blendNodeBuffer[bntTailWagBlend].transforms[nodeIndex],
							blendNodeBuffer[bntIdleBlend].transforms[nodeIndex],
							animNodeBuffer[antTailWagAnim].transforms[nodeIndex],
							poseNodeBuffer[pntReferencePose].transforms[nodeIndex],
							tailWagRatio);
				}
				else
				{
					blendNodeBuffer[bntTailWagBlend].transforms[nodeIndex] = blendNodeBuffer[bntIdleBlend].transforms[nodeIndex];
				}
			}

			memcpy(finalAnimTreeBuffer.transforms, blendNodeBuffer[bntTailWagBlend].transforms, sizeof(am_transform) * nodeCount);
		}
		else
		{
			appState->treeBlueprint.processTree(&finalAnimTreeBuffer, model, &appState->tempMemory, dtf);
		}

		for (uint32 nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
		{
			nodeFinalLocalMatrices[nodeIndex].makeIdentity();
			nodeFinalLocalMatrices[nodeIndex].translate(finalAnimTreeBuffer.transforms[nodeIndex].pos);
			nodeFinalLocalMatrices[nodeIndex] *= finalAnimTreeBuffer.transforms[nodeIndex].rot.getRotationMatrix();
			nodeFinalLocalMatrices[nodeIndex].scale(finalAnimTreeBuffer.transforms[nodeIndex].scale);
		}

		nodeWorldSpaceMatrices[0] = nodeFinalLocalMatrices[0];
		//nodeWorldSpaceMatrices[0].scale(0.01f);
		for (uint32 boneIndex = 1; boneIndex < model->nodes.nodeCount; boneIndex++)
		{
			uint32 parent = model->nodes.info[boneIndex].parent;
			nodeWorldSpaceMatrices[boneIndex] = nodeWorldSpaceMatrices[parent] * nodeFinalLocalMatrices[boneIndex];
		}

		nodeWorldSpacePositions = pushStructArray(&appState->tempMemory, vec3f, nodeCount);

		vec3f nilVec = vec3f(0.0f, 0.0f, 0.0f);

		for (uint32 nodeIndex = 0; nodeIndex < nodeCount; nodeIndex += 1)
		{
			SIMD_mulVector(nodeWorldSpacePositions + nodeIndex, (__m128 *) (nodeWorldSpaceMatrices + nodeIndex), &nilVec);
		}

		if (appState->uiState.isInBoneSelectionMode)
		{
			/*
			 * Separate all overlapping bones.
			 * 1-for each bone find a bone that overlaps (are colinear)
			 * 2-move the bone and all chidren of the second bone up a bit
			 */

			debug.clear();
			real32 minDif = FLT_EPSILON;
			for (uint32 nodeIndex = 0; nodeIndex < nodeCount; ++nodeIndex)
			{
				vec3f head0 = nodeWorldSpacePositions[nodeIndex];
				vec3f tail0 = nodeWorldSpacePositions[model->nodes.info[nodeIndex].parent];
				vec3f head0Tail0Dif = tail0 - head0;

				if ((head0 - tail0).lengthSquared() > minDif)
				{
					for (uint32 auxNodeIndex = nodeIndex + 1; auxNodeIndex < nodeCount; ++auxNodeIndex)
					{
						vec3f head1 = nodeWorldSpacePositions[auxNodeIndex];
						vec3f tail1 = nodeWorldSpacePositions[model->nodes.info[auxNodeIndex].parent];
						vec3f head0Head1Dif = head1 - head0;
						vec3f head0Tail1Dif = tail1 - head0;
						if (
						//if they're colinear
						head0Tail0Dif.cross(head0Head1Dif).lengthSquared() <= minDif
								&& head0Tail0Dif.cross(head0Tail1Dif).lengthSquared() <= minDif
								&& head0Head1Dif.cross(head0Tail1Dif).lengthSquared() <= minDif
								//if they're overlapping
//								&&
//								(
//										//Check if either head or tail are inside our range
//										head0Tail0Dif.dot(head0Head1Dif)>0
//										&&
//										head0Tail0Dif.dot(head0Tail1Dif)<head0Tail0Dif.dot(head0Tail0Dif)
//								)
								&&
								(tail1 - head1).lengthSquared() > minDif //Is it a collapsed node?
										)
						{
							//colinear and overlapping, just nudge it over
							/*
							 logger.log(
							 "myBone.dot(myBone) = %f\n"
							 "myBone.dot(otherHead) = %f\n"
							 "myBone.dot(otherTail) = %f",
							 head0Tail0Dif.dot(head0Tail0Dif),
							 head0Tail0Dif.dot(head0Head1Dif),
							 head0Tail0Dif.dot(head0Tail1Dif)
							 );*/

							/*vec3f offset(0.0f,0.1f,0.0f);
							 debug.addLine(head0+offset,tail0,vec4f(0.0f,0.0f,1.0f,1.0f));
							 debug.addLine(head1+offset, tail1,vec4f(1.0f,0.0f,0.0f,1.0f));
							 debug.addLine(head0+offset, head0,vec4f(0.0f,0.0f,0.0f,1.0f));
							 debug.addLine(tail0+offset, tail1,vec4f(1.0f,1.0f,1.0f,1.0f));
							 */

							nodeWorldSpacePositions[auxNodeIndex].y += 1.0f;

							//logger.log("Node %s is in conflict with node %s, moving it up",model->nodes.names[auxNodeIndex].name,model->nodes.names[nodeIndex].name);
						}
					}
				}
			}

			//logger.log("\nEND\n");
		}
	}

	real32 screenRatio = 1.0f;

	if (window.height != 0 && window.width != 0)
	{
		screenRatio = (real32) window.width / (real32) window.height;
	}
	vec2f mouseCameraPos = { (mousePos.x / window.width) * screenRatio * 2.0f - 1.0f * screenRatio, ((window.height - mousePos.y) / window.height) * 2.0f - 1.0f };
	vec3f cameraRayOrigin = appState->camera.cameraToWorldSpace(mouseCameraPos.x, mouseCameraPos.y);
	vec3f cameraRayDirection = (cameraRayOrigin - appState->camera.pos).normalize();

	if (!ImGui::GetIO().WantCaptureMouse)
	{
		if (input->button.camera.isDown)
		{
			real32 sensitivity = 0.5f;
			appState->camera.rotPitch -= (input->mouse.y - input->mouse.prevY) * sensitivity;
			appState->camera.rotYaw -= (input->mouse.x - input->mouse.prevX) * sensitivity;

			appState->camera.rotPitch = clamp(-90, 90, appState->camera.rotPitch);
		}
		else if (appState->uiState.isInBoneSelectionMode)
		{
			if (input->button.select.wasPressed())
			{
				if (model)
				{
					real32 selectOverlappingDistance = 4.0f;
					real32 selectOverlappingDistanceSquare = selectOverlappingDistance * selectOverlappingDistance;
					real32 selectStrictDistance = 10.0f;
					real32 selectStrictDistanceSquare = selectStrictDistance * selectStrictDistance;
					real32 selectRelaxedDistance = 20.0f;
					real32 selectRelaxedDistanceSquare = selectRelaxedDistance * selectRelaxedDistance;

					vec3f closestPointOnNode;
					vec3f closestPointOnCameraRay;

					real32 closestTimeOnNode;
					real32 closestTimeOnCameraRay;

					int32 closestSelectedBoneToCamera = -1;
					real32 closestSelectedBoneDistanceToCamera = FLT_MAX;

					int32 closestSelectedBoneToMouse = -1;
					real32 closestSelectedBoneDistanceToMouse = FLT_MAX;

					uint32 nodeCount = model->nodes.nodeCount;

					matrix4x4f camProjection = appState->camera.getFrustumMatrix((real32) screenRatio);
					matrix4x4f camModelview = appState->camera.getInvTransform();

					matrix4x4f cameraToScreenSpaceMat;
					cameraToScreenSpaceMat.makeIdentity();
					cameraToScreenSpaceMat.m00 = (real32) window.width * 0.5f; //
					cameraToScreenSpaceMat.m11 = -(real32) window.height * 0.5f; //

					cameraToScreenSpaceMat.m03 = (real32) window.width * 0.5f;
					cameraToScreenSpaceMat.m13 = (real32) window.height * 0.5f;

					matrix4x4f worldToWindowSpaceMat = cameraToScreenSpaceMat * camProjection * camModelview;

					vec4f windowSpaceCameraRayOrigin = worldToWindowSpaceMat * cameraRayOrigin;
					windowSpaceCameraRayOrigin /= windowSpaceCameraRayOrigin.w;

					struct select_candidate
					{
						uint32 boneIndex;
						real32 distanceToCamera;
						real32 distanceToMouseSq;
					};
					TEMP_MEMORY_BLOCK(&appState->tempMemory);

					select_candidate* candidates = pushStructArray(&appState->tempMemory, select_candidate, nodeCount);
					uint32 candidateCount = 0;

					//int32 nodeIndex = amFindNode(model,"Tail1");
					for (uint32 nodeIndex = 1; nodeIndex < nodeCount; nodeIndex += 1)
					{
						vec3f head = nodeWorldSpacePositions[nodeIndex];
						vec3f tail = nodeWorldSpacePositions[model->nodes.info[nodeIndex].parent];
						vec3f headTailDif = tail - head;
						if (closestPointsOnLine(head, headTailDif, cameraRayOrigin, cameraRayDirection, closestPointOnNode, closestPointOnCameraRay, closestTimeOnNode, closestTimeOnCameraRay))
						{
							bool gotHit = false;
							vec4f screenSpaceClosestPointOnNode = worldToWindowSpaceMat * vec4f(closestPointOnNode);
							vec4f screenSpaceClosestPointOnCameraRay = worldToWindowSpaceMat * vec4f(closestPointOnCameraRay);

							screenSpaceClosestPointOnNode /= screenSpaceClosestPointOnNode.w;
							screenSpaceClosestPointOnCameraRay /= screenSpaceClosestPointOnCameraRay.w;

							vec2f screenSpaceDif(
									screenSpaceClosestPointOnNode.x - screenSpaceClosestPointOnCameraRay.x,
									screenSpaceClosestPointOnNode.y - screenSpaceClosestPointOnCameraRay.y
											);

							if (screenSpaceDif.lengthSquared() < selectRelaxedDistanceSquare)
							{
								if (closestTimeOnNode > 0 && closestTimeOnNode < 1.0f && closestTimeOnCameraRay > 0)
								{
									real32 distanceToCamera = closestTimeOnCameraRay;
									real32 distanceToMouseSq = screenSpaceDif.lengthSquared();

									candidates[candidateCount].boneIndex = nodeIndex;
									candidates[candidateCount].distanceToCamera = distanceToCamera;
									candidates[candidateCount].distanceToMouseSq = distanceToMouseSq;
									++candidateCount;
								}
							}
						}
						else // they're parallel or zero vectors
						{
							//TODO(miko): treat it like a point
						}
					}

					int32 selectedNode = -1;

					/*
					 * Selection has 3 tiers
					 * 1-Try to find the topmost bone that would overlap all beneath it
					 * 2-If none is found, use the closest bone to the mouse within the strict range.
					 * 3-If no bone is within the strict range, just use the topmost bone under the mouse
					 */

					int32 overlappingClosestNodeIndex = -1;
					real32 overlappingClosestDistToCamera = FLT_MAX;

					int32 strictClosestNodeIndex = -1;
					real32 strictClosestDistToMouseSq = FLT_MAX;

					int32 relaxedClosestNodeIndex = -1;
					real32 relaxedClosestDistToCamera = FLT_MAX;

					for (uint32 candidateIndex = 0; candidateIndex < candidateCount; candidateIndex++)
					{
						if (candidates[candidateIndex].distanceToCamera < overlappingClosestDistToCamera &&
								candidates[candidateIndex].distanceToMouseSq < selectOverlappingDistanceSquare
										)
						{
							overlappingClosestNodeIndex = candidates[candidateIndex].boneIndex;
							overlappingClosestDistToCamera = candidates[candidateIndex].distanceToCamera;
						}

						if (candidates[candidateIndex].distanceToMouseSq < selectStrictDistanceSquare
								&& candidates[candidateIndex].distanceToMouseSq < strictClosestDistToMouseSq)
						{
							strictClosestNodeIndex = candidates[candidateIndex].boneIndex;
							strictClosestDistToMouseSq = candidates[candidateIndex].distanceToMouseSq;
						}

						if (candidates[candidateIndex].distanceToCamera < relaxedClosestDistToCamera)
						{
							relaxedClosestNodeIndex = candidates[candidateIndex].boneIndex;
							relaxedClosestDistToCamera = candidates[candidateIndex].distanceToCamera;
						}
					}

					selectedNode = overlappingClosestNodeIndex >= 0 ? overlappingClosestNodeIndex :
																		(
																		strictClosestNodeIndex >= 0 ? strictClosestNodeIndex : relaxedClosestNodeIndex
																		);

					if (selectedNode >= 0)
					{
						appState->tailWagBlendMask[selectedNode] = !appState->tailWagBlendMask[selectedNode];

						if (appState->tailWagBlendMask[selectedNode])
						{
							logger.log("Enabled node %d", selectedNode);
						}
						else
						{
							logger.log("Disabled node %d", selectedNode);
						}
					}
				}
			}
		}
	}

	real32 camSpeed = 10.0f; //m/s
	vec3f camForward = appState->camera.getForward();
	vec3f camRight = appState->camera.getRight();
	vec3f camUp = appState->camera.getUp();

	if (!ImGui::GetIO().WantCaptureKeyboard)
	{
		if (input->button.back.isPressed())
		{
			appState->camera.pos -= camForward * dtf * camSpeed;
		}
		if (input->button.forward.isPressed())
		{
			appState->camera.pos += camForward * dtf * camSpeed;
		}
		if (input->button.left.isPressed())
		{
			appState->camera.pos -= camRight * dtf * camSpeed;
		}
		if (input->button.right.isPressed())
		{
			appState->camera.pos += camRight * dtf * camSpeed;
		}
		if (input->button.down.isPressed())
		{
			appState->camera.pos -= camUp * dtf * camSpeed;
		}
		if (input->button.up.isPressed())
		{
			appState->camera.pos += camUp * dtf * camSpeed;
		}
	}

	if (window.width != 0 && window.height != 0)
	{
		logger.logOpenGLError("Render begin");
		PROFILE_BLOCK("RENDER");

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);
		glViewport(0, 0, window.width, window.height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		float widthRatio = (float) window.width / (float) window.height;
		//glFrustum(-right, right, -top, top, appState->camera.nearClip, appState->camera.farClip);
		matrix4x4f frustumMatrix = appState->camera.getFrustumMatrix(widthRatio);
		glLoadMatrixf(frustumMatrix.me);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glClearColor(0.5f, 0.5f, 0.6f, 1.0f);
		glClearDepth(1.0f);
		glClearStencil(0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

		//glTranslatef(0.0,0.0,-3.0f);
		matrix4x4f camMat = appState->camera.getInvTransform();
		glLoadMatrixf(camMat.me);

		glBegin(GL_LINES);

		vec3f ovec = { 0.0f, 0.0f, 0.0f };
		vec3f xvec = { 1.0f, 0.0f, 0.0f };
		vec3f yvec = { 0.0f, 1.0f, 0.0f };
		vec3f zvec = { 0.0f, 0.0f, 1.0f };

		glColor3fv(xvec.me);
		glVertex3fv(ovec.me);
		glVertex3fv(xvec.me);

		glColor3fv(yvec.me);
		glVertex3fv(ovec.me);
		glVertex3fv(yvec.me);

		glColor3fv(zvec.me);
		glVertex3fv(ovec.me);
		glVertex3fv(zvec.me);
		glEnd();

		if (model)
		{
			PROFILE_BLOCK("Drawing Model");
			//todo(miko): vertex skinning
			//printf("Model bone count: %d\n",model->bones.boneCount);
			uint32 matrixCount = model->nodes.nodeCount;
			if (matrixCount)
			{
				glColor3f(1.0f, 1.0f, 1.0f);
				glEnable(GL_CULL_FACE);
				glCullFace(GL_BACK);
				glEnable(GL_TEXTURE_2D);
				uint32 nodeCount = model->nodes.nodeCount;
				am_node_info* nodeInfo = model->nodes.info;
				glEnableClientState(GL_VERTEX_ARRAY);
				glEnableClientState(GL_NORMAL_ARRAY);
				for (uint32 nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++, nodeInfo++)
				{
					if (nodeInfo->meshRefCount)
					{
						glPushMatrix();
						glMultMatrixf(nodeWorldSpaceMatrices[nodeIndex].me);

						uint32 meshRefCount = nodeInfo->meshRefCount;
						uint32* meshRef = model->nodes.meshIds + nodeInfo->meshRefStart;

						for (uint32 meshRefIndex = 0; meshRefIndex < meshRefCount; meshRefIndex++)
						{
							am_mesh* curMesh = model->meshes + *meshRef;
							bool usingTexCoordArray = false;
							appState->textureManager->bindTexture(model->materials[curMesh->material].binding[0].texHandle);
							if (curMesh->uvs[0])
							{
								usingTexCoordArray = true;
								glEnableClientState(GL_TEXTURE_COORD_ARRAY);
							}

							if (curMesh->boneCount)
							{
								PROFILE_BLOCK("Rendering skinned model");
								glPopMatrix();
								matrix4x4f* boneMats = pushStructArrayAlign(&appState->tempMemory, matrix4x4f, curMesh->boneCount, 16);

								uint32 boneCount = curMesh->boneCount;
								am_bone* curBone = curMesh->bones;
								matrix4x4f *curBoneMat = boneMats;
								{
									PROFILE_BLOCK("Bind Mat");
									for (uint32 boneId = 0; boneId < boneCount; boneId++, curBone++, curBoneMat++)
									{
										*curBoneMat = nodeWorldSpaceMatrices[curBone->node] * curBone->bindMat;
									}
								}
								Assert(((uint64 )boneMats) % 16 == 0);

								vec3f* vertices = pushStructArray(&appState->tempMemory, vec3f, curMesh->vertexCount);

								uint32 vertCount = curMesh->vertexCount;
								vec3f *curVert = vertices;
								vec3f *srcVert = (vec3f*) curMesh->vertices;
								float *curVertWeight = curMesh->vertWeights;
								uint8 *curVertBoneId = curMesh->vertBoneIds;
								{
									__m128 *simdBoneMats = (__m128 *) boneMats;
									__m128 simdTempMat[4];

									PROFILE_BLOCK("Skinning");

									//uint64 start = __rdtsc();
									for (uint32 vertIndex = 0; vertIndex < vertCount; vertIndex++, curVert++, srcVert++/*, curVertWeight += 4, curVertBoneId += 4*/)
									{
										uint32 weightId = vertIndex * 4;

										SIMD_mulMatrix(simdTempMat, (__m128 *) (boneMats + curVertBoneId[weightId + 0]), curVertWeight[weightId + 0]);
										SIMD_addMulMatrix(simdTempMat, simdTempMat, (__m128 *) (boneMats + curVertBoneId[weightId + 1]), curVertWeight[weightId + 1]);
										SIMD_addMulMatrix(simdTempMat, simdTempMat, (__m128 *) (boneMats + curVertBoneId[weightId + 2]), curVertWeight[weightId + 2]);
										SIMD_addMulMatrix(simdTempMat, simdTempMat, (__m128 *) (boneMats + curVertBoneId[weightId + 3]), curVertWeight[weightId + 3]);
										SIMD_mulVector(curVert, simdTempMat, srcVert);

										/*
										 matrix4x4f mat0 = boneMats[curVertBoneId[weightId + 0]] * curVertWeight[weightId + 0];
										 matrix4x4f mat1 = boneMats[curVertBoneId[weightId + 1]] * curVertWeight[weightId + 1];
										 matrix4x4f mat2 = boneMats[curVertBoneId[weightId + 2]] * curVertWeight[weightId + 2];
										 matrix4x4f mat3 = boneMats[curVertBoneId[weightId + 3]] * curVertWeight[weightId + 3];

										 *curVert = (mat0+mat1+mat2+mat3) * *srcVert;
										 */
									}
									//uint64 end = __rdtsc();
									//printf("ticksL %llu\n",end-start);
								}

								{
									PROFILE_BLOCK("Actual rendering");
									glVertexPointer(3, GL_FLOAT, 0, vertices);
									glNormalPointer(GL_FLOAT, 0, curMesh->normals);
									if (usingTexCoordArray)
									{
										glTexCoordPointer(curMesh->uvElementCount[0], GL_FLOAT, 0, curMesh->uvs[0]);
									}
									glDrawElements(GL_TRIANGLES, curMesh->triangleCount * 3, GL_UNSIGNED_INT, curMesh->triangles);
								}

								glPushMatrix();
								glMultMatrixf(nodeWorldSpaceMatrices[nodeIndex].me);

							}
							else
							{
								glVertexPointer(3, GL_FLOAT, 0, curMesh->vertices);
								glNormalPointer(GL_FLOAT, 0, curMesh->normals);
								if (usingTexCoordArray)
								{
									glTexCoordPointer(curMesh->uvElementCount[0], GL_FLOAT, 0, curMesh->uvs[0]);
								}
								glDrawElements(GL_TRIANGLES, curMesh->triangleCount * 3, GL_UNSIGNED_INT, curMesh->triangles);
							}
							if (usingTexCoordArray)
							{
								glDisableClientState(GL_TEXTURE_COORD_ARRAY);
							}
						}
						glPopMatrix();
					}
				}
				appState->textureManager->bindTexture(0);

				glDisableClientState(GL_VERTEX_ARRAY);
				glDisableClientState(GL_NORMAL_ARRAY);
			}
		}

		glClear(GL_DEPTH_BUFFER_BIT);

		/*
		 * Draw overlay
		 */

		if (appState->uiState.isInBoneSelectionMode && model)
		{
			glEnable(GL_DEPTH_TEST);
			glPointSize(2.0f);
			glLineWidth(2.0f);
			glColor3f(1.0f, 1.0f, 0.0f);

			uint32 nodeCount = model->nodes.nodeCount;

			uint16 *nodeBodyIndices = pushStructArray(&appState->tempMemory, uint16, nodeCount*2);

			uint16 *activeNodeBodyIndices = pushStructArray(&appState->tempMemory, uint16, nodeCount*2);
			uint16 *activeNodeHeadIndices = pushStructArray(&appState->tempMemory, uint16, nodeCount*2);

			uint32 activeNodeCout = 0;

			for (uint32 nodeIndex = 1; nodeIndex < nodeCount; nodeIndex += 1)
			{
				nodeBodyIndices[nodeIndex * 2] = (uint16) nodeIndex;
				nodeBodyIndices[nodeIndex * 2 + 1] = (uint16) model->nodes.info[nodeIndex].parent;
				if (appState->tailWagBlendMask[nodeIndex])
				{
					activeNodeHeadIndices[activeNodeCout] = (uint16) nodeIndex;

					activeNodeBodyIndices[activeNodeCout * 2] = (uint16) nodeIndex;
					activeNodeBodyIndices[activeNodeCout * 2 + 1] = (uint16) model->nodes.info[nodeIndex].parent;

					++activeNodeCout;
				}
			}

			glEnableClientState(GL_VERTEX_ARRAY);

			glVertexPointer(3, GL_FLOAT, 0, nodeWorldSpacePositions);

			glPointSize(11.0f);
			glLineWidth(7.0f);
			glColor3f(1.0f, 1.0f, 0.0f);
			glDrawElements(GL_LINES, activeNodeCout * 2, GL_UNSIGNED_SHORT, activeNodeBodyIndices);
			glDrawElements(GL_POINTS, activeNodeCout, GL_UNSIGNED_SHORT, activeNodeHeadIndices);

			glEnable(GL_POLYGON_OFFSET_LINE);
			glEnable(GL_POLYGON_OFFSET_POINT);

			glDepthFunc(GL_LEQUAL);
			glPointSize(5.0f);
			glLineWidth(3.0f);
			glColor3f(0.0f, 0.0f, 0.4f);
			glDrawElements(GL_LINES, nodeCount * 2, GL_UNSIGNED_SHORT, nodeBodyIndices);
			glDrawArrays(GL_POINTS, 0, nodeCount);

			glDisable(GL_POLYGON_OFFSET_LINE);
			glDisable(GL_POLYGON_OFFSET_POINT);

			glDisableClientState(GL_VERTEX_ARRAY);

			glLineWidth(1.0f);
			glPointSize(1.0f);

			logger.logOpenGLError("Draw active nodes");
		}

		vec3f camera1UnitOffsetRay = cameraRayOrigin - cameraRayDirection * 4;
		glColor3f(1.0f, 1.0f, 1.0f);
		logger.logOpenGLError("Draw mouse axis");

		glDisable(GL_DEPTH_TEST);
		logger.logOpenGLError("Disable depth");

		debug.render();

		logger.logOpenGLError("Debug render");

		glViewport(0, 0, window.width, window.height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, window.width, window.height, 0, -1.0f, 1.0f);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		logger.logOpenGLError("Setup 2D");

		glColor3f(1.0f, 0.0f, 0.0f);
		glBegin(GL_LINES);
		glVertex2f(mousePos.x - 10, mousePos.y);
		glVertex2f(mousePos.x + 10, mousePos.y);

		glVertex2f(mousePos.x, mousePos.y - 10);
		glVertex2f(mousePos.x, mousePos.y + 10);
		glEnd();

		glColor3f(1.0f, 1.0f, 1.0f);
		logger.logOpenGLError("Draw mouse pos");
	}

	appState->tempMemory.clear();

	sProfiler.endFrame();

	glFinish();

	if (ImGui::Begin("Profiler"))
	{
		snapCurrentImguiWindow(windowWidth, windowHeight);
		for (uint32 entryIndex = 0; entryIndex < sProfiler.entryCount; entryIndex++)
		{
			real64 elapsedMillis = (sProfiler.entries[entryIndex].endTicks - sProfiler.entries[entryIndex].startTicks) / (getCPUFrequency() / 1000.0);
			ImGui::Text("%s ms: %f\n", sProfiler.entryNames[entryIndex], (real32) elapsedMillis);
		}
	}
	ImGui::End();
	logger.logOpenGLError("Frame End");
	//printf("\n\n\n\n");
}

