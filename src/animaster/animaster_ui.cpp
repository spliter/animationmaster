/*
 * animaster_ui.cpp
 *
 *  Created on: 25 Apr 2017
 *      Author: MikoKuta
 */

#include "animaster_ui.h"

static const uint32 InvalidIO = (uint32) -1;


AmuiBlueprint::~AmuiBlueprint()
{
	for (node_iterator nodeIter = nodes.begin(); !nodes.empty(); nodeIter = nodes.erase(nodeIter))
	{
		AmuiNode* node = *nodeIter;
		delete node;
	}
	connections.clear();
}

void AmuiBlueprint::Reset()
{
	for (node_iterator nodeIter = nodes.begin(); !nodes.empty(); nodeIter = nodes.erase(nodeIter))
	{
		AmuiNode* node = *nodeIter;
		delete node;
	}
	screenOffset.set(0.0f, 0.0f);

	activeConnectionNode = NULL;
	activeConnectionOutputIndex = InvalidIO;
	activeConnectionInputIndex = InvalidIO;

	draggingNode = NULL;

	nodes.push_back(new AmuiTestNode("Test1", 100, 100));
	nodes.push_back(new AmuiTestNode("Test2", 100, 300));
	nodes.push_back(new AmuiTestNode("Test3", 100, 500));
	nodes.push_back(new AmuiTestNode("Test4", 100, 700));

	for (node_iterator nodeIter = nodes.begin(); nodeIter != nodes.end(); ++nodeIter)
	{
		AmuiNode* node = *nodeIter;
		node->initFromData();
	}
}

void drawConnection(vec2f connectionSourcePos, vec2f connectionDestPos, AmuiConnectionType type)
{
	//todo(miko): use type for coloring connections
	vec2f linkStart = connectionSourcePos;
	vec2f linkEnd = connectionDestPos;

	float connectionControlNodeExtension = 100.0f;

	if(linkStart.x>linkEnd.x)
	{
		connectionControlNodeExtension+= (linkStart.x - linkEnd.x)*0.25f;
		//connectionControlNodeExtension = minf(connectionControlNodeExtension,200);
	}

	ImGui::GetWindowDrawList()->AddBezierCurve(
							linkStart, linkStart + vec2f(connectionControlNodeExtension, 0.0f),
							linkEnd + vec2f(-connectionControlNodeExtension, 0.0f), linkEnd,
							ImColor(255, 255, 255), 2.0f
							);
}

void AmuiBlueprint::Update()
{
	if (ImGui::Begin("Animaster Blueprint",NULL,ImGuiWindowFlags_NoScrollbar|ImGuiWindowFlags_NoScrollWithMouse))
	{
		ImDrawList* windowDrawList = ImGui::GetWindowDrawList();

		vec2f nodePadding(10.0f, 10.0f);

		vec2f widgetWorldBoxMin(0.0f, 0.0f);
		vec2f widgetWorldBoxMax(0.0f, 0.0f);

		float lineHeight = ImGui::GetItemsLineHeightWithSpacing();
		float ioSize = lineHeight;
		float ioVOffset = -2;
		float itemSpacing = ImGui::GetStyle().ItemSpacing.x;

		vec2f windowPos = ImGui::GetWindowPos();

//		AmuiNode* activeConnectionInputNode = NULL;
//		uint32 activeConnectionInputIndex = (uint32)-1;
//
//		AmuiNode* activeConnectionOutputNode = NULL;
//		uint32 activeConnectionOutputIndex= (uint32)-1;

		AmuiNode* hoveredNode = NULL;
		uint32 hoveredInputIndex = InvalidIO;
		uint32 hoveredOutputIndex = InvalidIO;

		for (node_iterator nodeIter = nodes.begin(); nodeIter != nodes.end(); ++nodeIter)
		{
			AmuiNode* node = *nodeIter;

			uint32 inputCount = node->inputs.size();
			uint32 outputCount = node->outputs.size();

			ImGui::SetCursorPos(node->pos + screenOffset);

			widgetWorldBoxMin = ImGui::GetCursorScreenPos();

			if (ImGui::BeginChild(node->name.c_str(), node->size))
			{
				ImDrawList* childDrawList = ImGui::GetWindowDrawList();
				childDrawList->ChannelsSplit(2);

				//Foreground
				{
					childDrawList->ChannelsSetCurrent(1);

					ImGui::SetCursorPos(nodePadding);

					ImGui::BeginGroup(); //Node content group
					{
						ImGui::BeginGroup();
						ImGui::AlignFirstTextHeightToWidgets();
						ImGui::Text(node->name.c_str());
						ImGui::EndGroup();
						vec2f headerMax = vec2f(ImGui::GetItemRectMax()) - widgetWorldBoxMin;
						childDrawList->AddRect(ImGui::GetItemRectMin(),ImGui::GetItemRectMax(),0xffff0000);
						//Inputs
						vec2f inputsStartPos = ImGui::GetCursorPos();
						ImGui::BeginGroup();
						ImGui::PushID("inputs");
						{
							vec2f inputsCenterStartPos = inputsStartPos + vec2f(ioSize * 0.5f) + ioVOffset;
							for (uint32 inputIndex = 0; inputIndex < inputCount; inputIndex++)
							{
								ImGui::PushID(inputIndex);
								AmuiSocket& input = node->inputs[inputIndex];
								ImGui::SetCursorPos(inputsStartPos + vec2f(0, lineHeight * inputIndex));
								//ImGui::Dummy(vec2f(ioSize));
								ImU32 color = ImColor(200, 200, 200);
								ImGui::InvisibleButton("", vec2f(ioSize));
								bool checkRect = activeConnectionNode && activeConnectionNode != node && activeConnectionOutputIndex != InvalidIO;
								bool checkButton = !activeConnectionNode;
								if ((checkRect && ImGui::IsItemHoveredRect()) || (checkButton && (ImGui::IsItemHovered() || ImGui::IsItemActive())))
								{
									color = ImColor(255, 255, 255);
									hoveredNode = node;
									hoveredInputIndex = inputIndex;
									hoveredOutputIndex = InvalidIO;
								}
								vec2f inputPos = inputsCenterStartPos + vec2f(0.0f, lineHeight * inputIndex);
								input.position = inputPos;

								childDrawList->AddCircleFilled(inputPos + widgetWorldBoxMin, ioSize * 0.4f, color);
								ImGui::SetCursorPos(inputsStartPos + vec2f(ioSize, lineHeight * inputIndex));
								ImGui::AlignFirstTextHeightToWidgets();
								if (input.name)
								{
									ImGui::Text(input.name);
								}
								ImGui::PopID();
							}
						}
						ImGui::PopID();
						ImGui::EndGroup();
						vec2f inputMax = vec2f(ImGui::GetItemRectMax()) - widgetWorldBoxMin;
						childDrawList->AddRect(ImGui::GetItemRectMin(),ImGui::GetItemRectMax(),0xffff00ff);

						//Body
						vec2f bodyStartPos = vec2f(inputMax.x + itemSpacing, inputsStartPos.y);
						ImGui::SetCursorPos(bodyStartPos);
						ImGui::BeginGroup();
						ImGui::PushID("body");
						{
							ImGui::Button("Test",vec2f(120.0f,120.0f));
						}
						ImGui::PopID();
						ImGui::EndGroup();

						childDrawList->AddRect(ImGui::GetItemRectMin(),ImGui::GetItemRectMax(),0xff0000ff);

						vec2f bodyMax = vec2f(ImGui::GetItemRectMax()) - widgetWorldBoxMin;

						float outputsOffset = maxf(headerMax.x, bodyMax.x);

						vec2f outputsStartPos = inputsStartPos + vec2f(outputsOffset, 0.0f);
						//Outputs
						ImGui::SetCursorPos(outputsStartPos);
						ImGui::BeginGroup();
						ImGui::PushID("outputs");
						{
							vec2f outputsCenterStartPos = outputsStartPos + vec2f(ioSize * 0.5f) + ioVOffset;

							float longestTextWidth = 0.0f;
							//find longest name
							for (uint32 outputIndex = 0; outputIndex < outputCount; outputIndex++)
							{

								AmuiSocket& output = node->outputs[outputIndex];
								if (output.name)
								{
									vec2f textSize = ImGui::CalcTextSize(output.name, NULL, true, false);
									if (textSize.x > longestTextWidth)
									{
										longestTextWidth = textSize.x;
									}
								}
							}
							for (uint32 outputIndex = 0; outputIndex < outputCount; outputIndex++)
							{
								ImGui::PushID(outputIndex);
								AmuiSocket& output = node->outputs[outputIndex];
								ImGui::SetCursorPos(outputsStartPos + vec2f(longestTextWidth + itemSpacing, lineHeight * outputIndex));
								//ImGui::Dummy(vec2f(ioSize));

								ImU32 color = ImColor(200, 200, 200);

								ImGui::InvisibleButton("", vec2f(ioSize));

								bool checkRect = activeConnectionNode && activeConnectionNode != node && activeConnectionInputIndex != InvalidIO;
								bool checkButton = !activeConnectionNode;
								if ((checkRect && ImGui::IsItemHoveredRect()) || (checkButton && (ImGui::IsItemHovered() || ImGui::IsItemActive())))
								{
									color = ImColor(255, 255, 255);
									hoveredNode = node;
									hoveredInputIndex = InvalidIO;
									hoveredOutputIndex = outputIndex;
								}

								vec2f outputPos = outputsCenterStartPos + vec2f(longestTextWidth + itemSpacing, lineHeight * outputIndex);
								output.position = outputPos;
								childDrawList->AddCircleFilled(outputPos + widgetWorldBoxMin, ioSize * 0.4f, color);

								vec2f textSize = ImGui::CalcTextSize(output.name, NULL, true, false);
								ImGui::SetCursorPos(outputsStartPos + vec2f(longestTextWidth - textSize.x, lineHeight * outputIndex));
								ImGui::AlignFirstTextHeightToWidgets();
								if (output.name)
								{
									ImGui::Text(output.name);
								}

								ImGui::PopID();
							}
						}
						ImGui::PopID();
						ImGui::EndGroup();
						childDrawList->AddRect(ImGui::GetItemRectMin(),ImGui::GetItemRectMax(),0xff00ffff);

					}
					ImGui::EndGroup(); //Node content group

					node->size.set(ImGui::GetItemRectSize());
					node->size += nodePadding * 2.0f;
					widgetWorldBoxMax = widgetWorldBoxMin + node->size;
				}

				//Background
				{
					ImGui::SetCursorPos(vec2f(0.0f, 0.0f));
					ImGui::InvisibleButton("background", node->size);
					if (ImGui::IsItemHovered() || ImGui::IsItemActive())
					{
						if (!hoveredNode)
						{
							printf("Hovering over node body\n");
							hoveredNode = node;
							hoveredInputIndex = InvalidIO;
							hoveredOutputIndex = InvalidIO;
						}
					}

					childDrawList->ChannelsSetCurrent(0);

					childDrawList->AddRectFilled(widgetWorldBoxMin, widgetWorldBoxMax, ImColor(40, 40, 40), 5);
					childDrawList->AddRect(widgetWorldBoxMin, widgetWorldBoxMax, ImColor(200, 200, 200), 5);
				}

				childDrawList->ChannelsMerge();
			}
			ImGui::EndChild();
		}

		ImGuiIO& io = ImGui::GetIO();
		if (io.MouseDown[0])
		{
			if (hoveredNode)
			{
				printf("\n");
				printf("hoveredNode: %s\n", hoveredNode->name.c_str());
				printf("hoveredInput: %d\n", hoveredInputIndex);
				printf("hoveredOutput: %d\n", hoveredOutputIndex);
			}

			if (draggingNode)
			{
				draggingNode->pos += io.MouseDelta;
			}
			else
			{
				if (!activeConnectionNode)
				{
					if (hoveredNode)
					{
						if (hoveredInputIndex != InvalidIO)
						{
							activeConnectionNode = hoveredNode;
							activeConnectionInputIndex = hoveredInputIndex;
						}
						else if (hoveredOutputIndex != InvalidIO)
						{
							activeConnectionNode = hoveredNode;
							activeConnectionOutputIndex = hoveredOutputIndex;
						}
						else
						{
							draggingNode = hoveredNode;
						}
					}
				}
				else
				{
					vec2f linkStart = io.MousePos;
					vec2f linkEnd = io.MousePos;
					AmuiConnectionType type = AmuiConnectionType_None;

					if (activeConnectionInputIndex != InvalidIO)
					{
						AmuiSocket &activeInput = activeConnectionNode->inputs[activeConnectionInputIndex];
						type = activeInput.type;
						linkEnd = windowPos + screenOffset + activeConnectionNode->pos + activeInput.position;
						if (hoveredNode && hoveredOutputIndex != InvalidIO)
						{
							AmuiSocket &hoveredOutput = hoveredNode->outputs[hoveredOutputIndex];
							if (hoveredOutput.type == activeInput.type)
							{
								linkStart = windowPos + screenOffset + hoveredNode->pos + hoveredOutput.position;
							}
						}
					}
					else if (activeConnectionOutputIndex != InvalidIO)
					{
						AmuiSocket &activeOutput = activeConnectionNode->outputs[activeConnectionOutputIndex];
						type = activeOutput.type;
						linkStart = windowPos + screenOffset + activeConnectionNode->pos + activeOutput.position;
						if (hoveredNode && hoveredInputIndex != InvalidIO)
						{
							AmuiSocket &hoveredInput = hoveredNode->inputs[hoveredInputIndex];
							if (hoveredInput.type == activeOutput.type)
							{
								linkEnd = windowPos + screenOffset + hoveredNode->pos + hoveredInput.position;
							}
						}
					}
					drawConnection(linkStart,linkEnd,type);
				}
			}
		}
		else
		{
			draggingNode = NULL;
			AmuiNode* srcNode = NULL;
			uint32 srcNodeOutputIndex = InvalidIO;
			AmuiNode* dstNode = NULL;
			uint32 dstNodeInputIndex = InvalidIO;

			bool32 addConnection = false;
			if (activeConnectionNode)
			{
				if (activeConnectionInputIndex != InvalidIO)
				{
					if (hoveredNode && hoveredOutputIndex != InvalidIO)
					{
						if (hoveredNode->outputs[hoveredOutputIndex].type == activeConnectionNode->inputs[activeConnectionInputIndex].type)
						{
							srcNode = hoveredNode;
							srcNodeOutputIndex = hoveredOutputIndex;
							dstNode = activeConnectionNode;
							dstNodeInputIndex = activeConnectionInputIndex;

							addConnection = true;
						}
					}
				}
				else if (activeConnectionOutputIndex != InvalidIO)
				{
					if (hoveredNode && hoveredInputIndex != InvalidIO)
					{
						if (hoveredNode->inputs[hoveredInputIndex].type == activeConnectionNode->outputs[activeConnectionOutputIndex].type)
						{
							printf("Connecting output to input\n");
							srcNode = activeConnectionNode;
							srcNodeOutputIndex = activeConnectionOutputIndex;
							dstNode = hoveredNode;
							dstNodeInputIndex = hoveredInputIndex;
							addConnection = true;
						}
					}
				}

				if (addConnection)
				{
					AddConnection(srcNode, srcNodeOutputIndex, dstNode, dstNodeInputIndex);
				}

				activeConnectionNode = NULL;
				activeConnectionInputIndex = InvalidIO;
				activeConnectionOutputIndex = InvalidIO;
			}
		}

		for (connection_iterator conIter = connections.begin(); conIter != connections.end(); ++conIter)
		{
			AmuiConnection& connection = *conIter;

			vec2f linkStart = windowPos + connection.srcNode->pos + screenOffset + connection.srcNode->outputs[connection.srcOutput].position;
			vec2f linkEnd = windowPos + connection.dstNode->pos + screenOffset + connection.dstNode->inputs[connection.dstInput].position;

			drawConnection(linkStart,linkEnd,connection.srcNode->outputs[connection.srcOutput].type);
		}

		if(io.MouseDown[1])
		{
			if(!hoveredNode && !activeConnectionNode)
			{
				screenOffset+=io.MouseDelta;
			}
		}

	}
	ImGui::End();
}

bool AmuiBlueprint::AddConnection(AmuiNode* srcNode, uint32 srcNodeOutputIndex, AmuiNode* dstNode, uint32 dstNodeInputIndex)
{
	for (connection_iterator conIter = connections.begin(); conIter != connections.end(); ++conIter)
	{
		if (conIter->dstNode == dstNode && conIter->dstInput == dstNodeInputIndex)
		{
			connections.erase(conIter);
			break;
		}
	}

	AmuiConnection connection;
	connection.srcNode = srcNode;
	connection.srcOutput = srcNodeOutputIndex;
	connection.dstInput = dstNodeInputIndex;
	connection.dstNode = dstNode;
	connection.type = srcNode->outputs[srcNodeOutputIndex].type;

	connections.push_back(connection);

	return true;
}
