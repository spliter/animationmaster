/*
 * animaster_ui.h
 *
 *  Created on: 25 Apr 2017
 *      Author: MikoKuta
 */

#pragma once

#include <list>
#include <vector>

enum AmuiConnectionType
{
	AmuiConnectionType_None=-1,
	AmuiConnectionType_Test=0,


	AmuiConnectionType_Count
};

struct AmuiSocket
{
public:
	vec2f position;
	AmuiConnectionType type;
	const char* name;
};

class AmuiNode
{
protected:
	virtual uint32 getInputCount(){return 0;};
	virtual AmuiConnectionType getInputType(uint32 input){return AmuiConnectionType_None;}
	virtual const char* getInputName(uint32 input){return "";}

	virtual uint32 getOutputCount(){return 0;};
	virtual AmuiConnectionType getOutputType(uint32 output){return AmuiConnectionType_None;}
	virtual const char* getOutputName(uint32 output){return "";}

public:

	std::string name;
	typedef std::vector<AmuiSocket> ioInfoVector;

	ioInfoVector inputs;
	ioInfoVector outputs;

	vec2f pos;
	vec2f size;

	AmuiNode(const std::string& nodeName,float x, float y):
			name(nodeName),
			size(0.0f,0.0f)
	{
		pos.x = x;
		pos.y = y;
	}

	virtual void displayContents(){};

	virtual void initFromData()
	{
		uint32 inputCount = getInputCount();
		uint32 outputCount = getOutputCount();

		AmuiSocket tempInfo;
		tempInfo.name = NULL;
		tempInfo.position.set(0.0f,0.0f);
		tempInfo.type = AmuiConnectionType_None;

		inputs.resize(inputCount);
		outputs.resize(outputCount);

		for(uint32 inputIndex = 0; inputIndex<inputCount; inputIndex++)
		{
			AmuiSocket& input = inputs[inputIndex];
			input.position.set(0.0f,0.0f);
			input.name = getInputName(inputIndex);
			input.type = getInputType(inputIndex);
		}

		for(uint32 outputIndex = 0; outputIndex<outputCount; outputIndex++)
		{
			AmuiSocket& output = outputs[outputIndex];
			output.position.set(0.0f,0.0f);
			output.name = getOutputName(outputIndex);
			output.type = getOutputType(outputIndex);
		}
	}

	virtual ~AmuiNode(){};
};

class AmuiTestNode: public AmuiNode
{
public:
	virtual uint32 getInputCount(){return 2;}
	virtual AmuiConnectionType getInputType(uint32 input){return AmuiConnectionType_Test;}
	virtual const char* getInputName(uint32 input)
	{
		switch(input)
		{
			case 0: return "input0";
			case 1: return "input1";
			default: return "";
		}
	}

	virtual uint32 getOutputCount(){return 1;}
	virtual AmuiConnectionType getOutputType(uint32 output){return AmuiConnectionType_Test;}
	virtual const char* getOutputName(uint32 output)
	{
		return "output";
	}

	AmuiTestNode(const std::string& nodeName,float x, float y):
		AmuiNode(nodeName,x,y)
	{

	}
	virtual ~AmuiTestNode(){}
};

struct AmuiConnection
{
	AmuiConnectionType type;

	AmuiNode* srcNode;
	uint32 srcOutput;

	AmuiNode* dstNode;
	uint32 dstInput;
};

class AmuiBlueprint
{
public:

	virtual ~AmuiBlueprint();
	virtual void Reset();
	virtual void Update();

	virtual bool AddConnection(AmuiNode* srcNode, uint32 srcNodeOutputIndex, AmuiNode* dstNode, uint32 dstNodeInputIndex);

	typedef std::list<AmuiNode*>::iterator node_iterator;
	typedef std::list<AmuiConnection>::iterator connection_iterator;
	std::list<AmuiNode*> nodes;
	std::list<AmuiConnection> connections;

	vec2f screenOffset;

	AmuiNode* activeConnectionNode;
	uint32 activeConnectionOutputIndex;
	uint32 activeConnectionInputIndex;

	AmuiNode* draggingNode;
};
