/*
 * am_model.cpp
 *
 *  Created on: 5 Feb 2017
 *      Author: MikoKuta
 */

#include "am_model.h"
#include "../../shared/memory_manager.h"
#include "../../shared/types.h"
#include "../../shared/utils.h"
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/matrix4x4.h>
#include <float.h>
inline uint32 amFindNode(am_model* model, const char* name)
{
	uint32 nodeCount = model->nodes.nodeCount;
	am_name* nodeName = model->nodes.names;
	for (uint32 nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++, nodeName++)
	{
		if (strcmp(name, nodeName->name) == 0)
		{
			return nodeIndex;
		}
	}
	return ANIMASTER_NODE_INVALID_INDEX;
}

int amFindAnimation(am_name* animNames, uint32 animCount, const char* animName)
{
	am_name* curAnimName = animNames;
	for (uint32 animIndex = 0; animIndex < animCount; animIndex++, curAnimName++)
	{
		if (strcmp(curAnimName->name, animName) == 0)
		{
			return animIndex;
		}
	}
	return -1;
}

void amCalculatePoseIntoBuffer(am_transform_buffer* dstBuffer, am_anim_clip_info_packed* info)
{
	uint32 nodeCount = info->nodeCount;
	am_transform* srcTransform = info->frames;
	am_transform* dstTransform = dstBuffer->transforms;
	for (uint32 nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
	{
		uint32 nodeId = info->nodeIds[nodeIndex];
		if (nodeId != NODE_INVALID)
		{
			dstTransform[nodeId] = srcTransform[nodeIndex];
		}
	}
}

void amCalculateAnimatedPoseIntoBuffer(am_transform_buffer* dstBuffer, am_anim_clip_info_packed* info, float animationTime)
{
	real32 framef = animationTime * info->fps;
	uint32 frameu = (uint32) framef;
	real32 frameRatio = framef - frameu;
	uint32 curFrame = frameu % info->frameCount;
	uint32 nextFrame = (curFrame + 1) % info->frameCount;

	uint32 nodeCount = info->nodeCount;
	for (uint32 nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
	{
		uint32 nodeId = info->nodeIds[nodeIndex];
		if (nodeId != NODE_INVALID)
		{
			am_transform prevTransform = info->frames[curFrame * nodeCount + nodeIndex];
			am_transform nextTransform = info->frames[nextFrame * nodeCount + nodeIndex];

			amBlendTransform(dstBuffer->transforms[nodeId], prevTransform, nextTransform, frameRatio);
		}
	}
}

const char* GetFilename(const char* path)
{
	const char* pos = strrchr(path, '/');
	if (pos)
	{
		if (pos[1] != 0)
		{
			return pos + 1;
		}
		return path;
	}

	pos = strrchr(path, '\\');
	if (pos)
	{
		if (pos[1] != 0)
		{
			return pos + 1;
		}
		return path;
	}
	return path;
}

void countAssimpNodesAndMeshRefs_recursive(aiNode* node, uint32& nodeCount, uint32& meshRefCount)
{
	if (!node)
	{
		return;
	}
	nodeCount++;
	meshRefCount += node->mNumMeshes;
	uint32 childCount = node->mNumChildren;
	for (uint32 childIndex = 0; childIndex < childCount; childIndex++)
	{
		countAssimpNodesAndMeshRefs_recursive(node->mChildren[childIndex], nodeCount, meshRefCount);
	}
}

void countAssimpNodesAndMeshRefs(aiNode* node, uint32& nodeCount, uint32& meshRefCount)
{
	meshRefCount = 0;
	nodeCount = 0;
	countAssimpNodesAndMeshRefs_recursive(node, nodeCount, meshRefCount);
}

void countAssimpAnimFramesAndNodeRefs(aiAnimation ** const anim, uint32 animCount, uint32& animFrameCount, uint32& animNodeRefCount, uint32 fps)
{
	animFrameCount = 0;
	animNodeRefCount = 0;
	for (uint32 animIndex = 0; animIndex < animCount; animIndex++)
	{
		animFrameCount += ((uint32) (anim[animIndex]->mDuration) + 1) * anim[animIndex]->mNumChannels; //+1 because assimp adds 1 extra key at the end to its duration.
		animNodeRefCount += anim[animIndex]->mNumChannels;
	}
}

inline void convertAssimpToAnimasterMatrix(am_matrix& dst, aiMatrix4x4& src)
{
	float *srcData = &src.a1;
	dst.m00 = (real32) srcData[0];
	dst.m10 = (real32) srcData[1];
	dst.m20 = (real32) srcData[2];
	dst.m30 = (real32) srcData[3];
	dst.m01 = (real32) srcData[4];
	dst.m11 = (real32) srcData[5];
	dst.m21 = (real32) srcData[6];
	dst.m31 = (real32) srcData[7];
	dst.m02 = (real32) srcData[8];
	dst.m12 = (real32) srcData[9];
	dst.m22 = (real32) srcData[10];
	dst.m32 = (real32) srcData[11];
	dst.m03 = (real32) srcData[12];
	dst.m13 = (real32) srcData[13];
	dst.m23 = (real32) srcData[14];
	dst.m33 = (real32) srcData[15];
	dst.transpose();

//	printf("Parsed matrix:\n"
//			"%f %f %f %f\n"
//			"%f %f %f %f\n"
//			"%f %f %f %f\n"
//			"%f %f %f %f\n",
//			dst.me[0], dst.me[1], dst.me[2], dst.me[3],
//			dst.me[4], dst.me[5], dst.me[6], dst.me[7],
//			dst.me[8], dst.me[9], dst.me[10], dst.me[11],
//			dst.me[12], dst.me[13], dst.me[14], dst.me[15]
//			);

}

inline void convertAssimpToAnimasterTransform(am_transform& dst, aiMatrix4x4& src)
{
	aiVector3D position;
	aiQuaternion rotation;
	aiVector3D scale;
	src.Decompose(scale, rotation, position);
	dst.pos.set(position.x, position.y, position.z);
	dst.rot.set(rotation.x, rotation.y, rotation.z, rotation.w);
	dst.scale.set(scale.x, scale.y, scale.z);

//	printf("Parsed transform: s{%f, %f, %f} r{%f, %f, %f, %f} p{%f, %f, %f}\n",
//			scale.x, scale.y, scale.z,
//			rotation.x, rotation.y, rotation.z, rotation.w,
//			position.x, position.y, position.z
//			);
}

inline void convertAssimpToAnimasterNodeName(am_name& dst, aiString& src)
{
	strcpy_s(dst.name, ANIMASTER_NODE_NAME_MAX_LENGTH, src.C_Str());
}

inline void convertAssimpToAnimasterAnimationName(am_name& dst, aiString & src)
{
	const char* breakPos = strchr(src.C_Str(), '|');
	if (breakPos && breakPos[1])
	{
		strcpy_s(dst.name, ANIMASTER_NODE_NAME_MAX_LENGTH, breakPos + 1);
	}
	else
	{
		strcpy_s(dst.name, ANIMASTER_NODE_NAME_MAX_LENGTH, src.C_Str());
	}
}

bool32 convertAssimpToAnimasterMaterials(am_model* model, const aiScene* scene, stack_memory_manager* resourceMemoryManager, am_texture_manager* textureManager)
{
	//todo(miko): finish this
	uint64 resourceOldUsed = resourceMemoryManager->memoryUsed;

#undef ABORT_OP
#define ABORT_OP()\
		tempMemoryManager->memoryUsed = tempOldUsed;\
		resourceMemoryManager->memoryUsed = resourceOldUsed;\
		return false;

	aiMaterial** assimpMaterials = scene->mMaterials;
	uint32 matCount = scene->mNumMaterials;

	am_material* curMaterial = model->materials;
	for (uint32 matIndex = 0; matIndex < matCount; matIndex++, curMaterial++)
	{
		uint32 diffuseTexCount = assimpMaterials[matIndex]->GetTextureCount(aiTextureType_DIFFUSE);
		diffuseTexCount = diffuseTexCount > ANIMASTER_TEXTURE_MAX_BINDINGS ? ANIMASTER_TEXTURE_MAX_BINDINGS : diffuseTexCount;

		for (uint32 texIndex = 0; texIndex < diffuseTexCount; texIndex++)
		{
			aiString texPath;
			assimpMaterials[matIndex]->GetTexture(aiTextureType_DIFFUSE, texIndex, &texPath);
			curMaterial->binding[texIndex].texHandle = textureManager->getTexture(texPath.C_Str()).handle;
			printf("Loading texture for material %d, texIndex %d, %s has %s\n", matIndex, texIndex, texPath.C_Str(), curMaterial->binding[texIndex].texHandle ? "succeeded" : "failed");
		}
	}

	return true;
}

void convertAssimpToAnimasterNodes(am_model* model, aiNode* node, uint32 currentNode, uint32 &nextFreeNodeIndex, uint32 &nextFreeMeshId)
{

	//printf("Converting node %d {%s}", currentNode, node->mName.C_Str());
	//children
	am_node_info* info = model->nodes.info + currentNode;
	info->childrenStart = (uint16) nextFreeNodeIndex;
	info->childrenCount = (uint16) node->mNumChildren;
	info->meshRefStart = (uint16) nextFreeMeshId;
	info->meshRefCount = (uint16) node->mNumMeshes;
	nextFreeNodeIndex += node->mNumChildren;
	nextFreeMeshId += node->mNumMeshes;
	//note: we assume parent is already set by parent
	convertAssimpToAnimasterMatrix(model->nodes.matrices[currentNode], node->mTransformation);
	convertAssimpToAnimasterTransform(model->nodes.transforms[currentNode], node->mTransformation);
	convertAssimpToAnimasterNodeName(model->nodes.names[currentNode], node->mName);

	uint32 childStart = info->childrenStart;
	uint32 meshRefCount = info->meshRefCount;
	uint32 childCount = info->childrenCount;
	for (uint32 meshIdIndex = 0; meshIdIndex < meshRefCount; meshIdIndex++)
	{
		//Note(miko): were guaranteed that the mesh indices are the same for the animaster model and aiScene
		model->nodes.meshIds[info->meshRefStart + meshIdIndex] = node->mMeshes[meshIdIndex];
	}
	for (uint32 childIndex = 0; childIndex < childCount; childIndex++)
	{
		model->nodes.info[childStart + childIndex].parent = (uint16) currentNode;
		convertAssimpToAnimasterNodes(model, node->mChildren[childIndex], childStart + childIndex, nextFreeNodeIndex, nextFreeMeshId);
	}
}

bool32 convertAssimpToAnimasterNodes(am_model* model, aiNode* node)
{
	model->nodes.info[0].parent = ANIMASTER_NODE_INVALID_INDEX;
	uint32 nextFreeMeshId = 0;
	uint32 nextFreeNodeIndex = 1;
	convertAssimpToAnimasterNodes(model, node, 0, nextFreeNodeIndex, nextFreeMeshId);

	return true;
}

uint32 countAssimpMeshTriangles(aiMesh* mesh)
{
	uint32 triangleCount = 0;
	uint32 faceCount = mesh->mNumFaces;
	aiFace* face = mesh->mFaces;
	for (uint32 faceId = 0; faceId < faceCount; faceId++, face++)
	{
		if (face->mNumIndices == 3)
		{
			triangleCount++;
		}
	}
	return triangleCount;
}

//Note: update the types if MAX_ANIMASTER_VERTEX_BONES changes
void pushAnimasterWeight(uint32 dstVertexId, real32* dstWeights, uint8* dstBoneIds, real32 srcWeight, uint8 srcBoneId)
{
	uint32 weightStart = dstVertexId * ANIMASTER_VERTEX_MAX_BONES;
	uint32 weightEnd = weightStart + ANIMASTER_VERTEX_MAX_BONES;
	uint32 lesserWeightIndex = weightStart;
	real32 lesserWeight = dstWeights[weightStart];
	for (uint32 weightIndex = weightStart; weightIndex <= weightEnd; weightIndex++)
	{
		if (dstWeights[weightIndex] == 0.0f)
		{
			dstWeights[weightIndex] = srcWeight;
			dstBoneIds[weightIndex] = srcBoneId;
			break;
		}
		else if (dstWeights[weightIndex] < lesserWeight)
		{
			lesserWeight = dstWeights[weightIndex];
			lesserWeightIndex = weightIndex;
		}
	}
	if (lesserWeight < srcWeight)
	{
		dstWeights[lesserWeightIndex] = srcWeight;
		dstBoneIds[lesserWeightIndex] = srcBoneId;
	}
}

inline void cleanupAnimasterWeights(real32 *vertWeights, uint8* vertWeightIds)
{
	real32 weightSum = 0.0f;

	real32 *curWeight = vertWeights;
	for (uint32 weightIndex = 0; weightIndex < ANIMASTER_VERTEX_MAX_BONES; weightIndex++, curWeight++)
	{
		weightSum += *curWeight;
	}

	if (weightSum > FLT_EPSILON)
	{
		real32 oneOverSum = 1.0f / weightSum;
		curWeight = vertWeights;
		for (uint32 weightIndex = 0; weightIndex < ANIMASTER_VERTEX_MAX_BONES; weightIndex++, curWeight++)
		{
			*curWeight *= oneOverSum;
		}
	}
	else
	{
		vertWeights[0] = 1.0f;
		vertWeightIds[0] = 0;

		curWeight = vertWeights;
		uint8* curWeightId = vertWeightIds;
		for (uint32 weightIndex = 1; weightIndex < ANIMASTER_VERTEX_MAX_BONES; weightIndex++, curWeight++, curWeightId++)
		{
			*curWeight = 0.0f;
			curWeightId = 0;
		}
	}
}

inline void convertAssimpToAnimasterUvs(real32* dest, aiVector3D* src, uint32 uvElementCount, uint32 uvCount)
{
	switch (uvElementCount)
	{
		case 1:
			{
			real32* dstUv = dest;
			aiVector3D* srcUv = src;
			for (uint32 uvIndex = 0; uvIndex < uvCount; ++uvIndex, ++dstUv, ++srcUv)
			{
				*dstUv = srcUv->x;
			}
		}
		break;
		case 2:
			{
			real32* dstUv = dest;
			aiVector3D* srcUv = src;
			for (uint32 uvIndex = 0; uvIndex < uvCount; ++uvIndex, dstUv += 2, ++srcUv)
			{
				dstUv[0] = srcUv->x;
				dstUv[1] = srcUv->y;
			}
		}
		break;
		case 3:
			{
			memcpy(dest, src, sizeof(real32) * 3 * uvCount);
		}
		break;
		default:
			{
			memset(dest, 0, sizeof(real32) * uvElementCount * uvCount);
		}
		break;
	}
}

bool32 convertAssimpToAnimasterMeshes(am_model* model, const aiScene* scene, stack_memory_manager* resourceMemoryManager)
{
	uint64 resourceOldUsed = resourceMemoryManager->memoryUsed;

#undef ABORT_OP
#define ABORT_OP()\
			resourceMemoryManager->memoryUsed = resourceOldUsed;\
			return false;

	uint32 meshCount = scene->mNumMeshes;
	am_mesh* curMesh = model->meshes;
	for (uint32 meshId = 0; meshId < meshCount; meshId++, curMesh++)
	{
		aiMesh *assimpMesh = scene->mMeshes[meshId];
		ZERO_STRUCT(am_mesh, curMesh);
		curMesh->vertexCount = assimpMesh->mNumVertices;
		curMesh->triangleCount = countAssimpMeshTriangles(assimpMesh);

		curMesh->vertices = pushStructArray(resourceMemoryManager, real32, 3 * curMesh->vertexCount);
		curMesh->normals = pushStructArray(resourceMemoryManager, real32, 3 * curMesh->vertexCount);
		curMesh->binormals = pushStructArray(resourceMemoryManager, real32, 3 * curMesh->vertexCount);
		curMesh->tangents = pushStructArray(resourceMemoryManager, real32, 3 * curMesh->vertexCount);


		curMesh->material = (uint16)assimpMesh->mMaterialIndex;

		if (assimpMesh->mNumBones > 0 && curMesh->vertexCount)
		{
			//printf(" ASSSIMP MESH HAS BONESSSSSS\n\n\n\n\n\n\n\n\n\n\n\n\n");
			uint32 boneCount = assimpMesh->mNumBones <= ANIMASTER_MESH_MAX_BONES ? assimpMesh->mNumBones : ANIMASTER_MESH_MAX_BONES;
			curMesh->boneCount = (uint8) boneCount;
			curMesh->bones = pushStructArray(resourceMemoryManager, am_bone, curMesh->boneCount);
			curMesh->vertWeights = pushStructArray(resourceMemoryManager, real32, curMesh->vertexCount * ANIMASTER_VERTEX_MAX_BONES);
			curMesh->vertBoneIds = pushStructArray(resourceMemoryManager, uint8, curMesh->vertexCount * ANIMASTER_VERTEX_MAX_BONES);

			ABORT_IF(!curMesh->vertWeights || !curMesh->vertBoneIds);

			ZERO_STRUCT_ARRAY(real32, curMesh->vertWeights, curMesh->vertexCount*ANIMASTER_VERTEX_MAX_BONES);
			ZERO_STRUCT_ARRAY(uint8, curMesh->vertBoneIds, curMesh->vertexCount*ANIMASTER_VERTEX_MAX_BONES);

			am_bone* curBone = curMesh->bones;
			for (uint8 boneIndex = 0; boneIndex < boneCount; boneIndex++, curBone++)
			{
				aiBone* assimpBone = assimpMesh->mBones[boneIndex];
				uint32 weightCount = assimpBone->mNumWeights;
				aiVertexWeight *assimpWeight = assimpBone->mWeights;
				curBone->node = amFindNode(model, assimpBone->mName.C_Str());
				convertAssimpToAnimasterMatrix(curBone->bindMat, assimpBone->mOffsetMatrix);

				for (uint32 weightIndex = 0; weightIndex < weightCount; weightIndex++, assimpWeight++)
				{
					pushAnimasterWeight(assimpWeight->mVertexId, curMesh->vertWeights, curMesh->vertBoneIds, assimpWeight->mWeight, boneIndex);
				}
			}
			//cleanup
			uint32 vertWeightCount = curMesh->vertexCount * ANIMASTER_VERTEX_MAX_BONES;
			for (uint32 weightIndex = 0; weightIndex < vertWeightCount; weightIndex += ANIMASTER_VERTEX_MAX_BONES)
			{
				cleanupAnimasterWeights(curMesh->vertWeights + weightIndex, curMesh->vertBoneIds + weightIndex);
			}
		}

		curMesh->triangles = pushStructArray(resourceMemoryManager, uint32, 3 * curMesh->triangleCount);

		if (curMesh->vertexCount)
		{
			ABORT_IF(!curMesh->vertices || !curMesh->normals || !curMesh->binormals || !curMesh->tangents);
		}

		ABORT_IF(curMesh->triangleCount && !curMesh->triangles);

		memcpy(curMesh->vertices, assimpMesh->mVertices, sizeof(real32) * 3 * curMesh->vertexCount);
		memcpy(curMesh->normals, assimpMesh->mNormals, sizeof(real32) * 3 * curMesh->vertexCount);

		real32** curUvs = curMesh->uvs;
		uint32 uvCount = 0;
		for (uint32 uvId = 0; uvId < ANIMASTER_MESH_MAX_UVS; uvId++)
		{
			//Note(Miko): we only accept uvs that have 2 elements, for simplicity
			if (assimpMesh->mTextureCoords[uvId])
			{
				uint32 uvElemCount = assimpMesh->mNumUVComponents[uvId];
				curMesh->uvs[uvId] = pushStructArray(resourceMemoryManager, real32, uvElemCount * curMesh->vertexCount);
				ABORT_IF(curMesh->vertexCount && !curMesh->uvs[uvId]);
				convertAssimpToAnimasterUvs(curMesh->uvs[uvId], assimpMesh->mTextureCoords[uvId], uvElemCount, curMesh->vertexCount);
				//memcpy(curMesh->uvs[uvId], assimpMesh->mTextureCoords[uvId], sizeof(real32) * uvElemCount * curMesh->vertexCount);
				curMesh->uvElementCount[uvId] = (uint8) uvElemCount;
			}
		}

		if (assimpMesh->mBitangents && assimpMesh->mTangents)
		{
			memcpy(curMesh->binormals, assimpMesh->mBitangents, sizeof(real32) * 3 * curMesh->vertexCount);
			memcpy(curMesh->tangents, assimpMesh->mTangents, sizeof(real32) * 3 * curMesh->vertexCount);
		}
		uint32 faceCount = assimpMesh->mNumFaces;
		aiFace* assimpFace = assimpMesh->mFaces;
		uint32 *triangles = curMesh->triangles;
		for (uint32 faceIndex = 0; faceIndex < faceCount; faceIndex++, assimpFace++)
		{
			if (assimpFace->mNumIndices == 3)
			{
				triangles[0] = assimpFace->mIndices[0];
				triangles[1] = assimpFace->mIndices[1];
				triangles[2] = assimpFace->mIndices[2];

				triangles += 3;
			}
		}
	}

	return true;
}

enum transform_param
{
	tp_position,
	tp_rotation,
	tp_scale
};
template<typename T> void findNextAndPrevKeys(T* keys, uint32 keyCount, real64 time, uint32 &keyA, uint32 &keyB, uint32 startIndexHint)
{
	if (time <= keys[0].mTime)
	{
		keyA = 0;
		keyB = 0;
		return;
	}

	if (time >= keys[keyCount - 1].mTime)
	{
		keyA = keyCount - 1;
		keyB = keyCount - 1;
		return;
	}

	if (keyCount < 2)
	{
		keyA = 0;
		keyB = 0;
	}

	startIndexHint = startIndexHint < keyCount - 1 ? startIndexHint : 0;
	uint32 prevKey = startIndexHint;

	for (uint32 keyIndex = startIndexHint; keyIndex < keyCount; prevKey = keyIndex, keyIndex++)
	{
		if (keys[keyIndex].mTime > time)
		{
			keyA = prevKey;
			keyB = keyIndex;
			return;
		}
	}

	if (startIndexHint != 0)
	{
		prevKey = 0;
		for (uint32 keyIndex = 1; keyIndex <= startIndexHint; prevKey = keyIndex, keyIndex++)
		{
			if (keys[keyIndex].mTime > time)
			{
				keyA = prevKey;
				keyB = keyIndex;
				return;
			}
		}
	}

	//it should never enter here but in any case:
	keyA = keyCount - 2;
	keyB = keyCount - 1;
}

template<transform_param param> void copyTransform(am_transform &dst, const am_transform& src);

template<> void copyTransform<tp_position>(am_transform &dst, const am_transform& src)
{
	//printf("copying pos %f, %f, %f\n", src.pos.x, src.pos.y, src.pos.z);
	dst.pos = src.pos;
}
template<> void copyTransform<tp_rotation>(am_transform &dst, const am_transform& src)
{
//	printf("copying rot %f, %f, %f, %f\n", src.rot.x, src.rot.y, src.rot.z, src.rot.w);
	dst.rot = src.rot;
}
template<> void copyTransform<tp_scale>(am_transform &dst, const am_transform& src)
{
//	printf("copying scale %f, %f, %f\n", src.scale.x, src.scale.y, src.scale.z);
	dst.scale = src.scale;
}

template<typename T, transform_param param> void copyFrameTransform(am_transform &dst, const T& src);

template<> void copyFrameTransform<aiVectorKey, tp_position>(am_transform &dst, const aiVectorKey& src)
{
	dst.pos.x = src.mValue.x;
	dst.pos.y = src.mValue.y;
	dst.pos.z = src.mValue.z;
}

template<> void copyFrameTransform<aiVectorKey, tp_scale>(am_transform &dst, const aiVectorKey& src)
{
	dst.scale.x = src.mValue.x;
	dst.scale.y = src.mValue.y;
	dst.scale.z = src.mValue.z;
}

template<> void copyFrameTransform<aiQuatKey, tp_rotation>(am_transform &dst, const aiQuatKey& src)
{
	dst.rot.x = src.mValue.x;
	dst.rot.y = src.mValue.y;
	dst.rot.z = src.mValue.z;
	dst.rot.w = src.mValue.w;
}

template<typename T>
void interpolateKeys(T& dstKey, const T &prevKey, const T &nextKey, real64 ratio);

template<>
void interpolateKeys<aiVectorKey>(aiVectorKey &dstKey, const aiVectorKey &prevKey, const aiVectorKey &nextKey, real64 ratio)
{
	Assimp::Interpolator<aiVectorKey> interpolator;
	interpolator(dstKey.mValue, prevKey, nextKey, (real32) ratio);
}

template<>
void interpolateKeys<aiQuatKey>(aiQuatKey &dstKey, const aiQuatKey &prevKey, const aiQuatKey &nextKey, real64 ratio)
{
	Assimp::Interpolator<aiQuatKey> interpolator;
	interpolator(dstKey.mValue, prevKey, nextKey, (real32) ratio);
}

template<typename T, transform_param param>
void copyFrameBlendTransform(am_transform &dst, const T &prevKey, const T &nextKey, real64 ratio)
{
	T curKey;
	interpolateKeys(curKey, prevKey, nextKey, ratio);
	copyFrameTransform<T, param>(dst, curKey);
}

template<typename T, transform_param param> bool convertAssimpToAnimasterFrames(
		am_transform* frames, uint32 frameCount, uint32 stride,
		aiNodeAnim* assimpChannel,
		T* assimpKeys, uint32 keyCount,
		am_transform* nodeTransform)
{
	if (!nodeTransform)
	{
		return false;
	}
	uint32 prevKey = 0;
	uint32 nextKey = 0;
	if (keyCount > 0)
	{
		for (uint32 frameIndex = 0; frameIndex < frameCount; frameIndex++, frames += stride)
		{
			real64 time = (real64) frameIndex;
			findNextAndPrevKeys(assimpKeys, keyCount, time, prevKey, nextKey, prevKey);
			if (time < assimpKeys[prevKey].mTime)
			{
				if (assimpChannel->mPreState == aiAnimBehaviour_DEFAULT)
				{
					copyTransform<param>(*frames, *nodeTransform);
				}
				else
				{
					copyFrameTransform<T, param>(*frames, assimpKeys[prevKey]);
				}
			}
			else if (time > assimpKeys[nextKey].mTime)
			{
				if (assimpChannel->mPostState == aiAnimBehaviour_DEFAULT)
				{
					copyTransform<param>(*frames, *nodeTransform);
				}
				else
				{
					copyFrameTransform<T, param>(*frames, assimpKeys[nextKey]);
				}
			}
			else
			{
				if (assimpKeys[nextKey].mTime - assimpKeys[prevKey].mTime > FLT_EPSILON)
				{
					real64 ratio = (time - assimpKeys[prevKey].mTime) / (assimpKeys[nextKey].mTime - assimpKeys[prevKey].mTime);
					copyFrameBlendTransform<T, param>(*frames, assimpKeys[prevKey], assimpKeys[nextKey], ratio);
				}
				else
				{
					copyFrameTransform<T, param>(*frames, assimpKeys[prevKey]);
				}
			}
		}
	}
	else
	{
		for (uint32 frameIndex = 0; frameIndex < frameCount; frameIndex++, frames += stride)
		{
			copyTransform<param>(*frames, *nodeTransform);
		}
	}
	return true;
}

bool32 convertAssimpToAnimasterAnimations(am_model* model, const aiScene* scene, uint32 fps, stack_memory_manager* resourceMemoryManager)
{
	uint64 resourceOldUsed = resourceMemoryManager->memoryUsed;

#undef ABORT_OP
#define ABORT_OP()\
			resourceMemoryManager->memoryUsed = resourceOldUsed;\
			return false;

	uint32 animCount = scene->mNumAnimations;
	uint32 lastFreeFrame = 0;
	uint32 lastFreeNodeRef = 0;
	for (uint32 animIndex = 0; animIndex < animCount; animIndex++)
	{
		aiAnimation* assimpAnim = scene->mAnimations[animIndex];

		convertAssimpToAnimasterAnimationName(model->animation.names[animIndex], assimpAnim->mName);
		//strcpy_s(model->animation.names[animIndex].name,MAX_ANIMASTER_NODE_NAME_LENGTH,assimpAnim->mName.C_Str());
		uint32 channelCount = assimpAnim->mNumChannels;

		real64 animDuration = assimpAnim->mDuration;
		am_animation_info* animInfo = model->animation.info + animIndex;
		animInfo->nodeIdsStart = lastFreeNodeRef;
		animInfo->nodeCount = channelCount;
		animInfo->frameStart = lastFreeFrame;
		animInfo->fps = (uint32) assimpAnim->mTicksPerSecond;
		if (animInfo->fps == 0)
		{
			animInfo->fps = 30;
		}
		animInfo->frameCount = (uint32) (animDuration) + 1;

		lastFreeNodeRef += channelCount;
		lastFreeFrame += animInfo->frameCount * channelCount;

		uint32 frameCount = animInfo->frameCount;
		uint32 nodeRefIndex = animInfo->nodeIdsStart;
		am_transform* frames = model->animation.frames + animInfo->frameStart;

		for (uint32 channelIndex = 0; channelIndex < channelCount; channelIndex++, nodeRefIndex++)
		{
			aiNodeAnim* assimpChannel = assimpAnim->mChannels[channelIndex];
			uint32 nodeRef = amFindNode(model, assimpChannel->mNodeName.C_Str());
			ABORT_IF(nodeRef == NODE_INVALID);
			model->animation.nodeIds[nodeRefIndex] = nodeRef;

			convertAssimpToAnimasterFrames<aiVectorKey, tp_position>(
					frames + channelIndex, frameCount, channelCount,
					assimpChannel, assimpChannel->mPositionKeys, assimpChannel->mNumPositionKeys,
					model->nodes.transforms + nodeRef);
			convertAssimpToAnimasterFrames<aiVectorKey, tp_scale>(
					frames + channelIndex, frameCount, channelCount,
					assimpChannel, assimpChannel->mScalingKeys, assimpChannel->mNumScalingKeys,
					model->nodes.transforms + nodeRef);
			convertAssimpToAnimasterFrames<aiQuatKey, tp_rotation>(
					frames + channelIndex, frameCount, channelCount,
					assimpChannel, assimpChannel->mRotationKeys, assimpChannel->mNumRotationKeys,
					model->nodes.transforms + nodeRef);
		}
	}

	return true;
}

am_model* amLoadModel(const char* meshFile, stack_memory_manager* resourceMemoryManager, stack_memory_manager* tempMemoryManager, am_texture_manager* textureManager)
{
	uint64 resourceOldUsed = resourceMemoryManager->memoryUsed;
	uint64 tempOldUsed = tempMemoryManager->memoryUsed;

#undef ABORT_OP
#define ABORT_OP()\
			tempMemoryManager->memoryUsed = tempOldUsed;\
			resourceMemoryManager->memoryUsed = resourceOldUsed;\
			return NULL

	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(meshFile,
			aiProcess_GenNormals |
					aiProcess_CalcTangentSpace |
					aiProcess_Triangulate |
					aiProcess_JoinIdenticalVertices
					);
	if (scene)
	{
		am_model* model = pushStruct(resourceMemoryManager, am_model);

		model->meshCount = scene->mNumMeshes;
		uint32 nodeCount = 0;
		uint32 meshRefCount = 0;
		uint32 animNodeRefCount = 0;
		uint32 animFrameCount = 0;
		uint32 animFps = 30;

		countAssimpNodesAndMeshRefs(scene->mRootNode, nodeCount, meshRefCount);
		countAssimpAnimFramesAndNodeRefs(scene->mAnimations, scene->mNumAnimations, animFrameCount, animNodeRefCount, animFps);

		//todo(miko): clean up what's done here and what's allocated inside
		model->nodes.nodeCount = nodeCount;
		model->nodes.meshIdCount = meshRefCount;
		model->animation.animCount = scene->mNumAnimations;
		model->animation.frameCount = animFrameCount;
		model->animation.nodeIdCount = animNodeRefCount;
		model->materialCount = scene->mNumMaterials;

		model->meshes = pushStructArray(resourceMemoryManager, am_mesh, model->meshCount);

		model->nodes.info = pushStructArray(resourceMemoryManager, am_node_info, model->nodes.nodeCount);
		model->nodes.matrices = pushStructArray(resourceMemoryManager, am_matrix, model->nodes.nodeCount);
		model->nodes.names = pushStructArray(resourceMemoryManager, am_name, model->nodes.nodeCount);
		model->nodes.transforms = pushStructArray(resourceMemoryManager, am_transform, model->nodes.nodeCount);
		model->nodes.meshIds = pushStructArray(resourceMemoryManager, uint32, model->nodes.meshIdCount);

		model->animation.names = pushStructArray(resourceMemoryManager, am_name, model->animation.animCount);
		model->animation.info = pushStructArray(resourceMemoryManager, am_animation_info, model->animation.animCount);
		model->animation.frames = pushStructArray(resourceMemoryManager, am_transform, model->animation.frameCount);
		model->animation.nodeIds = pushStructArray(resourceMemoryManager, uint32, model->animation.nodeIdCount);

		model->materials = pushStructArray(resourceMemoryManager, am_material, model->materialCount);

		ABORT_IF(!model->meshes && model->meshCount);
		if (model->nodes.nodeCount)
		{
			ABORT_IF(!model->nodes.info);
			ABORT_IF(!model->nodes.matrices);
			ABORT_IF(!model->nodes.names);
			ABORT_IF(!model->nodes.transforms);
		}
		ABORT_IF(!model->nodes.meshIds && model->nodes.meshIdCount);

		if (model->animation.animCount)
		{
			ABORT_IF(!model->animation.names);
			ABORT_IF(!model->animation.info);
		}
		ABORT_IF(!model->animation.frames && model->animation.frameCount);
		ABORT_IF(!model->animation.nodeIds && model->animation.nodeIdCount);

		ABORT_IF(!model->materials && model->materialCount);

		bool32 matSuccess = convertAssimpToAnimasterMaterials(model, scene, resourceMemoryManager, textureManager);
		ABORT_IF(!matSuccess);
		bool32 nodeSuccess = convertAssimpToAnimasterNodes(model, scene->mRootNode);
		ABORT_IF(!nodeSuccess);
		bool32 meshSuccess = convertAssimpToAnimasterMeshes(model, scene, resourceMemoryManager);
		ABORT_IF(!meshSuccess);
		bool32 animSuccess = convertAssimpToAnimasterAnimations(model, scene, animFps, resourceMemoryManager);
		ABORT_IF(!animSuccess);

		printf("Successfuly loaded model %s\n", meshFile);
		tempMemoryManager->memoryUsed = tempOldUsed;
		return model;
	}
	else
	{
		printf("Failed to load model %s\n", meshFile);
		printf("[ERROR]\"%s\"\n", importer.GetErrorString());
	}

	ABORT_OP()
;}
