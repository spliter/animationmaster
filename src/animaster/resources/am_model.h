/*
 * am_model.h
 *
 *  Created on: 5 Feb 2017
 *      Author: MikoKuta
 */

#pragma once

#include "shared/types.h"
#include "shared/utils.h"
#include "math/vec3f.h"
#include "math/quaternionf.h"
#include "math/matrix4x4f.h"

#define ANIMASTER_TEXTURE_MAX_BINDINGS 8

#define ANIMASTER_VERTEX_MAX_BONES 4
#define ANIMASTER_MESH_MAX_BONES 64
#define ANIMASTER_MESH_MAX_UVS 8

#define ANIMASTER_NODE_NAME_MAX_LENGTH 32
#define ANIMASTER_NODE_INVALID_INDEX 0xffffffff


typedef matrix4x4f am_matrix;

struct am_material
{
	struct
	{
		texture_handle texHandle;
		real32 x, y, width, height; //allows the binding to be part of atlas
	} binding[ANIMASTER_TEXTURE_MAX_BINDINGS];
	//note: this needs a reference to shader, and some binding info
};

struct am_bone
{
	uint32 node;
	matrix4x4f bindMat;
};


struct am_mesh
{
	real32* vertices; //3 coords
	real32* normals; //3 coords
	real32* binormals; //3 coords
	real32* tangents; //3 coords
	real32* uvs[ANIMASTER_MESH_MAX_UVS];

	real32* vertWeights; // 4 weights per vertex
	uint8* vertBoneIds; // 4 bone ids per vertex, update type if MAX_ANIMASTER_MESH_BONES changes

	uint32* triangles; //sets of 3 vertex ids

	uint8 uvElementCount[ANIMASTER_MESH_MAX_UVS]; //how big each of the uvs is

	uint32 vertexCount;
	uint32 triangleCount;

	am_bone* bones;
	uint8 boneCount; // limited by max value of am_bone_index, update type if MAX_ANIMASTER_MESH_BONES changes

	uint16 material;
};

struct am_node_info
{
	uint32 parent;
	uint32 childrenStart; //on which index in the scene do the children start
	uint32 meshRefStart; //on which index in the am_model.nodes.meshInstances does the mesh indices start

	uint16 childrenCount;
	uint16 meshRefCount;
};

struct am_name
{
	char name[ANIMASTER_NODE_NAME_MAX_LENGTH];
};

struct am_transform
{
	vec3f pos;
	vec3f scale;
	quaternionf rot;
};

struct am_animation_info
{
	uint32 frameStart;
	uint32 frameCount;
	uint32 nodeIdsStart;
	uint32 nodeCount;
	uint32 fps;
};

struct am_anim_clip_info_packed
{
	am_name* name;
	uint32 frameCount;
	uint32 nodeCount;
	uint32 fps;
	am_transform* frames; //transforms are packed per frame, then per bone
	uint32* nodeIds;
};

struct am_model
{
	struct
	{
		am_name* names;
		am_node_info* info;
		am_transform* transforms;
		am_matrix* matrices;
		uint32* meshIds;
		uint32 meshIdCount;
		uint32 nodeCount;
	} nodes;

	//TODO(miko): somehow change this to allow animation streaming
	struct
	{
		am_name* names;
		am_animation_info* info;
		am_transform* frames; //transforms are packed per animation then per frame, then per bone
		uint32* nodeIds; //which nodes does each animation reference

		uint32 animCount; // how many animations
		uint32 frameCount; // total frame count in all animations for this mesh
		uint32 nodeIdCount; //number of nodes referenced by all animations
	} animation;
	am_mesh* meshes;

	am_material* materials;
	uint32 materialCount;

	//note: materials should be stored somewhere else, as a separate resource
	uint32 meshCount;
};


struct am_transform_buffer
{
	uint32 nodeCount;
	am_transform* transforms;
};

uint32 amFindNode(am_model* model, const char* name);
int amFindAnimation(am_name* animNames, uint32 animCount, const char* animName);
void amCalculateAnimatedPoseIntoBuffer(am_transform_buffer* dstBuffer, am_anim_clip_info_packed* info, float animationTime);
am_model* amLoadModel(const char* meshFile, stack_memory_manager* resourceMemoryManager, stack_memory_manager* tempMemoryManager, am_texture_manager* textureManager);

inline am_transform amGetBoneTransformFromMatrix(const matrix4x4f& mat)
{
	am_transform result = { };
	result.pos = mat.getTranslation();
	result.rot = quaternionf::getRotation(mat);
	result.scale = mat.getScale();
	return result;
}

inline void amBlendTransform(am_transform &result, const am_transform &A, const am_transform &B, real32 ratio)
{
	result.pos = A.pos + (B.pos - A.pos) * ratio;
	result.rot = slerp(A.rot, B.rot, ratio);
	result.scale = A.scale + (B.scale - A.scale) * ratio;
}

inline void amBlendAdditiveTransform(am_transform &result, const am_transform &prevTransform, const am_transform &additiveTransform, const am_transform &referenceTransform, real32 ratio)
{
	result.pos = prevTransform.pos + (additiveTransform.pos - referenceTransform.pos) * ratio;
	result.rot = prevTransform.rot *
				slerp(
					quaternionf::identity(),
					additiveTransform.rot*referenceTransform.rot.conjugate(),
					ratio
				);
	result.scale = prevTransform.scale + (additiveTransform.scale - referenceTransform.scale) * ratio;
}

inline am_anim_clip_info_packed amGetPackedAnimationClipInfo(am_model* model, uint32 animationIndex)
{
	Assert(animationIndex<model->animation.animCount);
	am_anim_clip_info_packed result={};
	am_animation_info* info = model->animation.info + animationIndex;
	result.fps = info->fps;
	result.frameCount = info->frameCount;
	result.nodeCount = info->nodeCount;
	result.frames = model->animation.frames + info->frameStart;
	result.nodeIds = model->animation.nodeIds + info->nodeIdsStart;
	result.name = model->animation.names + animationIndex;
	return result;
}
