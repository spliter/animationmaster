/*
 * TextureManager.cpp
 *
 *  Created on: 2 Feb 2017
 *      Author: MikoKuta
 */

#include <gl/GL.h>
#include <IL/il.h>
#include <IL/ilut.h>
#include "shared/types.h"
#include "shared/utils.h"
#include "shared/memory_manager.h"
#include "am_texture_manager.h"


struct texture_native_info
{
	GLuint glHandle;
	uint32 width, height;
};

#undef ABORT_IF
#define ABORT_IF(x)
//#define ABORT_IF(x) if(x){printf("%s:%s failed test: \"%s\", aborting",GetFilename(__FILE__),STRINGIZE(__LINE__),STRINGIZE(x));ABORT_OP();}

am_texture_manager* am_texture_manager::create(uint32 maxTextures, stack_memory_manager* memManager, uint32 textureMemorySize)
{
	uint64 memoryOldUsed = memManager->memoryUsed;
#undef ABORT_OP
#define ABORT_OP()\
				manager->memoryUsed = memoryOldUsed;\
				return NULL;

	am_texture_manager* texManager = pushStruct(memManager, am_texture_manager);
	ABORT_IF(!texManager);

	ilInit();
	ilutInit();

	texManager->textureCount = maxTextures + 1;

	texManager->memory.clear();
	texManager->memory.memory = (uint8*) memManager->push(textureMemorySize, 4);
	ABORT_IF(!texManager->memory.memory);

	texManager->memory.memorySize = textureMemorySize;
	texManager->memory.memoryUsed = 0;

	texManager->filenames = pushStructArray(&texManager->memory, texture_filename, texManager->textureCount);
	texManager->textures = pushStructArray(&texManager->memory, texture_native_info, texManager->textureCount);
	ABORT_IF(!texManager->textures);

	ZERO_STRUCT_ARRAY(texture_filename, texManager->filenames, texManager->textureCount);
	ZERO_STRUCT_ARRAY(texture_native_info, texManager->textures, texManager->textureCount);
	ABORT_IF(glGetError()!=GL_NO_ERROR);

	//setting it here explicitly just because it's a special case
	texManager->textures[0].glHandle = 0;
	texManager->textures[0].width = 0;
	texManager->textures[0].height = 0;

	return texManager;
}

void am_texture_manager::clear(am_texture_manager* texManager)
{
	for (uint32 texIndex = 1; texIndex < textureCount; texIndex++)
	{
		if (textures[texIndex].glHandle)
		{
			glDeleteTextures(1, &textures[texIndex].glHandle);
		}
	}
}

texture_info am_texture_manager::getTexture(const char* filename, bool32 loadIfNotLoaded, bool32 unique)
{
	texture_info result = { };
	if (!unique)
	{
		for (uint32 texIndex = 0; texIndex < textureCount; texIndex++)
		{
			if (strcmp(filename, filenames[texIndex]) == 0)
			{
				result.handle = texIndex;
				result.width = textures[texIndex].width;
				result.height = textures[texIndex].height;
				break;
			}
		}
	}

	if (unique || loadIfNotLoaded)
	{
		char realPath[MAX_TEXTURE_FILENAME_LENGTH];
		/* Note(Miko): for now we just assume that all texture paths should be in data/ folder
		 * for now we simply assume that any path not containing "/" or "\\" is just a file in data
		 * TODO(Miko): find a better system for this, something that recognizes between relative paths and absolutes
		 */
		if(strchr(filename,'\\') || strchr(filename,'/'))
		{
			strcpy_s(realPath,MAX_TEXTURE_FILENAME_LENGTH,filename);
		}
		else
		{
			char dataPath[]="data/";
			int dataPathLength = strlen(dataPath);
			strcpy_s(realPath,MAX_TEXTURE_FILENAME_LENGTH,"data/");
			strcpy_s(realPath+dataPathLength,MAX_TEXTURE_FILENAME_LENGTH-dataPathLength,filename);
		}

		//TODO(miko): load texture;
		for(uint32 texIndex = 1; texIndex<textureCount; texIndex++)
		{
			if(textures[texIndex].glHandle==0)
			{
				textures[texIndex].glHandle = ilutGLLoadImage(realPath);
				if(textures[texIndex].glHandle!=0)
				{
					glBindTexture(GL_TEXTURE_2D,textures[texIndex].glHandle);

					GLint texWidth = 0;
					GLint texHeight = 0;
					glGetTexLevelParameteriv(GL_TEXTURE_2D,0,GL_TEXTURE_WIDTH,&texWidth);
					glGetTexLevelParameteriv(GL_TEXTURE_2D,0,GL_TEXTURE_HEIGHT,&texHeight);
					textures[texIndex].width = texWidth;
					textures[texIndex].height = texHeight;

					result.handle = texIndex;
					result.width = texWidth;
					result.height = texHeight;

					glBindTexture(GL_TEXTURE_2D,0);
					strcpy_s(filenames[texIndex],MAX_TEXTURE_FILENAME_LENGTH,filename);
				}
				else
				{
					printf("Texture failed to load: %s\n", realPath);
				}
				break;
			}
		}
	}
	return result;
}

void am_texture_manager::unloadTexture(texture_handle handle)
{
	if (handle > 0 && handle < textureCount)
	{
		if (textures[handle].glHandle)
		{
			glDeleteTextures(1, &textures[handle].glHandle);
			filenames[handle][0] = 0; //no need to clear entire texture fname
			ZERO_STRUCT(texture_native_info, textures + handle);
			return;
		}
	}
}

void am_texture_manager::bindTexture(uint32 handle)
{
	if (handle >= 0 && handle < textureCount)
	{
		glBindTexture(GL_TEXTURE_2D,textures[handle].glHandle);
	}
}
