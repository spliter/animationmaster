/*
 * imgui_animaster.h
 *
 *  Created on: 11 Mar 2017
 *      Author: MikoKuta
 */
#pragma once

#include <Windows.h>
#include "imgui.h"
#include <gl/GL.h>
#include "../shared/types.h"
#include "../math/vec2f.h"

namespace ImGui
{

namespace Animaster
{
	void draw_list(ImDrawData* data);

	const int MouseButtonCount = 3;
	struct
	{
		float x, y;
		int scroll;
		bool buttonDown[MouseButtonCount];
		int32 buttonSwitchCount[MouseButtonCount];
		uint32 mouseButtonFlags;
	} sMouseState;

	struct
	{
		bool lAltDown,rAltDown;
		bool lWinDown,rWinDown;
	} sKeyState;

	uint32 sFontTexture;

	LPCSTR sCursorResources[ImGuiMouseCursor_Count_] = {
			IDC_ARROW,
			IDC_IBEAM,
			IDC_ARROW,
			IDC_SIZENS,
			IDC_SIZEWE,
			IDC_SIZENESW,
			IDC_SIZENWSE
	};

	HCURSOR sCursors[ImGuiMouseCursor_Count_];

	void init(HWND window)
	{

		ImGuiIO &io = ImGui::GetIO();

		RECT windowClientRect = {};
		GetClientRect(window,&windowClientRect);

		io.DisplaySize.x = (float)(windowClientRect.right-windowClientRect.left);
		io.DisplaySize.y = (float)(windowClientRect.bottom-windowClientRect.top);

		io.RenderDrawListsFn = draw_list;

		io.KeyMap[ImGuiKey_Tab] = VK_TAB;
		io.KeyMap[ImGuiKey_LeftArrow] = VK_LEFT;
		io.KeyMap[ImGuiKey_RightArrow] = VK_RIGHT;
		io.KeyMap[ImGuiKey_UpArrow] = VK_UP;
		io.KeyMap[ImGuiKey_DownArrow] = VK_DOWN;
		io.KeyMap[ImGuiKey_PageUp] = VK_PRIOR;
		io.KeyMap[ImGuiKey_PageDown] = VK_NEXT;
		io.KeyMap[ImGuiKey_Home] = VK_HOME;
		io.KeyMap[ImGuiKey_End] = VK_END;
		io.KeyMap[ImGuiKey_Delete] = VK_DELETE;
		io.KeyMap[ImGuiKey_Backspace] = VK_BACK;
		io.KeyMap[ImGuiKey_Enter] = VK_RETURN;
		io.KeyMap[ImGuiKey_Escape] = VK_ESCAPE;
		io.KeyMap[ImGuiKey_A] = 'A';
		io.KeyMap[ImGuiKey_C] = 'C';
		io.KeyMap[ImGuiKey_V] = 'V';
		io.KeyMap[ImGuiKey_X] = 'X';
		io.KeyMap[ImGuiKey_Y] = 'Y';
		io.KeyMap[ImGuiKey_Z] = 'Z';


		for(uint32 mbuttonIndex = 0; mbuttonIndex<MouseButtonCount; mbuttonIndex++)
		{
			sMouseState.buttonDown[mbuttonIndex] = false;
			sMouseState.buttonSwitchCount[mbuttonIndex] = false;
			io.MouseDown[mbuttonIndex] = false;
		}

		sMouseState.mouseButtonFlags = 0;

		sFontTexture = 0;
		glEnable(GL_TEXTURE_2D);
		glGenTextures(1,&sFontTexture);
		if(sFontTexture)
		{
			glBindTexture(GL_TEXTURE_2D,sFontTexture);
			unsigned char *fontData = 0;
			int fontTexWidth = 0;
			int fontTexHeight = 0;
			int bpp = 0;
			io.Fonts->GetTexDataAsRGBA32(&fontData,&fontTexWidth,&fontTexHeight,&bpp);
			if(fontData)
			{
				glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA8,fontTexWidth,fontTexHeight,0,GL_RGBA,GL_UNSIGNED_BYTE,fontData);
			}
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glBindTexture(GL_TEXTURE_2D,0);
			io.Fonts->TexID = (void*)&sFontTexture;
		}

		io.Fonts->ClearInputData();
		io.Fonts->ClearTexData();


		for(uint32 cursorIndex = 0; cursorIndex<ImGuiMouseCursor_Count_; cursorIndex++)
		{
			sCursors[cursorIndex] = LoadCursor(NULL,sCursorResources[cursorIndex]);
		}
	}

	void process_event(UINT message, WPARAM wParam, LPARAM lParam)
	{
		ImGuiIO &io = ImGui::GetIO();
		switch(message)
		{
			case WM_LBUTTONDOWN:
			case WM_LBUTTONUP:
			{
				bool isDown = message==WM_LBUTTONDOWN;
				if(sMouseState.buttonDown[0]!=isDown)
				{
					sMouseState.buttonSwitchCount[0]++;
				}
				sMouseState.buttonDown[0] = isDown;
			}
			break;

			case WM_RBUTTONDOWN:
			case WM_RBUTTONUP:
			{
				bool isDown = message==WM_RBUTTONDOWN;
				if(sMouseState.buttonDown[1]!=isDown)
				{
					sMouseState.buttonSwitchCount[1]++;
				}
				sMouseState.buttonDown[1] = isDown;
			}
			break;

			case WM_MBUTTONDOWN:
			case WM_MBUTTONUP:
			{
				bool isDown = message==WM_MBUTTONDOWN;
				if(sMouseState.buttonDown[2]!=isDown)
				{
					sMouseState.buttonSwitchCount[2]++;
				}
				sMouseState.buttonDown[2] = isDown;
			}
			break;

			case WM_MOUSEWHEEL:
			{
				sMouseState.scroll += GET_WHEEL_DELTA_WPARAM(wParam);
			}
			break;


			case WM_KEYDOWN:
			case WM_KEYUP:
			{
				bool isDown = (message==WM_KEYDOWN);
				io.KeysDown[wParam] = isDown;
				switch(wParam)
				{
					case VK_CONTROL: 	io.KeyCtrl = isDown;break;
					case VK_MENU: 		io.KeyAlt= isDown;break;
					case VK_SHIFT:		io.KeyShift = isDown;break;
					case VK_LWIN:		io.KeySuper = isDown;break;
				}
			}
			break;

			case WM_CHAR:
			{
				if(wParam>0 && wParam<=0xffff)//make sure it fits in a short
				{
					io.AddInputCharacter((ImWchar)wParam);
				}
			}
			break;
		}
	}

	void update(HWND window, vec2f mousePos, float dtf)
	{
		ImGuiIO &io = ImGui::GetIO();
		RECT windowClientRect = {};
		GetClientRect(window,&windowClientRect);

		io.DeltaTime = dtf;

		io.DisplaySize.x = (float)(windowClientRect.right-windowClientRect.left);
		io.DisplaySize.y = (float)(windowClientRect.bottom-windowClientRect.top);

		io.MousePos.x = mousePos.x;
		io.MousePos.y = mousePos.y;

		for(uint32 mbuttonIndex = 0; mbuttonIndex<MouseButtonCount; mbuttonIndex++)
		{
			io.MouseDown[mbuttonIndex] = sMouseState.buttonDown[mbuttonIndex] || sMouseState.buttonSwitchCount[mbuttonIndex]>0;
			sMouseState.buttonSwitchCount[mbuttonIndex] = 0;
		}

		io.MouseWheel = (float)sMouseState.scroll / (float)WHEEL_DELTA;

		sMouseState.scroll = 0;

		ImGui::NewFrame();
	}

	void onFrameEnd(HWND window, int mouseScreenX, int mouseScreenY)
	{
		ImGui::Render();
		ImGuiMouseCursor desiredCursor = ImGui::GetMouseCursor();

		RECT clientRect;
		GetClientRect(window,&clientRect);

		if(	mouseScreenX>clientRect.left &&
			mouseScreenX<clientRect.right &&
			mouseScreenY>clientRect.top &&
			mouseScreenY<clientRect.bottom
			)
		{
			if(desiredCursor!=ImGuiMouseCursor_None)
			{
				ShowCursor(true);
				SetCursor(sCursors[desiredCursor]);
			}
			else
			{
				ShowCursor(false);
			}
		}
	}

	void draw_list(ImDrawData* data)
	{
		if(data->CmdListsCount==0)
		{
			return;
		}

		ImGuiIO &io = ImGui::GetIO();

		int32 displayWidth = (int32)(io.DisplaySize.x * io.DisplayFramebufferScale.x);
		int32 displayHeight = (int32)(io.DisplaySize.y * io.DisplayFramebufferScale.y);

		if(displayWidth<=0 || displayHeight<=0)
		{
			return;
		}

		data->ScaleClipRects(io.DisplayFramebufferScale);

		GLint lastViewport[4];
		GLint lastScissors[4];
		GLint lastTexture;

		glGetIntegerv(GL_VIEWPORT,lastViewport);
		glGetIntegerv(GL_SCISSOR_BOX,lastScissors);
		glGetIntegerv(GL_TEXTURE_BINDING_2D,&lastTexture);

		glViewport(0,0,displayWidth,displayHeight);

		glPushAttrib(GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT | GL_TRANSFORM_BIT);



		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_SCISSOR_TEST);

		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);


		glMatrixMode(GL_TEXTURE);
		glPushMatrix();
		glLoadIdentity();

		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		glOrtho(0.0f,io.DisplaySize.x,io.DisplaySize.y,0.0f,-1.0f,1.0f);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadIdentity();

		uint32 cmdListCount = data->CmdListsCount;
		for(uint32 cmdListIndex = 0; cmdListIndex<cmdListCount; ++cmdListIndex)
		{
			const ImDrawList* cmdList = data->CmdLists[cmdListIndex];

			const uint8* vertexData = (uint8*)(&cmdList->VtxBuffer.front());
			const ImDrawIdx* indices = &cmdList->IdxBuffer.front();

			glVertexPointer(2,GL_FLOAT,sizeof(ImDrawVert),(void*)(vertexData+offsetof(ImDrawVert,pos)));
			glTexCoordPointer(2,GL_FLOAT,sizeof(ImDrawVert),(void*)(vertexData+offsetof(ImDrawVert,uv)));
			glColorPointer(4,GL_UNSIGNED_BYTE,sizeof(ImDrawVert),(void*)(vertexData+offsetof(ImDrawVert,col)));


			const ImDrawCmd* drawCmd = &cmdList->CmdBuffer[0];
			for(int32 cmdIndex = 0; cmdIndex<cmdList->CmdBuffer.size(); ++cmdIndex,++drawCmd)
			{
				if(drawCmd->UserCallback)
				{
					drawCmd->UserCallback(cmdList, drawCmd);
				}
				else
				{
					uint32 texture = *((uint32*)(drawCmd->TextureId));
					glBindTexture(GL_TEXTURE_2D,texture);
					glScissor(
							(GLint)drawCmd->ClipRect.x,
							(GLint)(displayHeight - drawCmd->ClipRect.w),
							(GLint)(drawCmd->ClipRect.z-drawCmd->ClipRect.x),
							(GLint)(drawCmd->ClipRect.w-drawCmd->ClipRect.y));
					glDrawElements(GL_TRIANGLES,drawCmd->ElemCount,GL_UNSIGNED_SHORT,indices);

				}
				indices+=drawCmd->ElemCount;
			}
		}

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		glMatrixMode(GL_TEXTURE);
		glPopMatrix();

		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glLoadIdentity();


		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
		glLoadIdentity();

		glViewport(lastViewport[0],lastViewport[1],lastViewport[2],lastViewport[3]);
		glScissor(lastScissors[0],lastScissors[1],lastScissors[2],lastScissors[3]);
		glBindTexture(GL_TEXTURE_2D,lastTexture);

		glPopAttrib();

	}




}//Animaster

}//ImGUI
