#include <Windows.h>
#include <Windowsx.h>
#include <stdlib.h>
#include <stdio.h>
#include <gl/GL.h>
#include <io.h>
#include <fcntl.h>

#include "math/mathUtils.h"
#include "shared/types.h"
#include "shared/utils.h"
#include "shared/memory_manager.h"
#include "animaster/animaster.h"
#include "imgui/imgui_animaster.h"

//Compilation Unit
#include "platform_utils_win32.cpp"
#include "animaster/animaster.cpp"
#include "pugixml/pugixml.cpp"
#include "imgui/imgui.cpp"
#include "imgui/imgui_draw.cpp"
#include "math/mathutils.cpp"
#include "math/vec_math_utils.cpp"


void ShowError(const char* text)
{
	MessageBoxA(0,text,"ERROR",MB_OK);
	terminate();
}

struct win32_app_state
{
	HWND mainWindow;
	HDC mainDeviceContext;
	HGLRC mainGlContext;

	bool isRunning;

	uint32 mouseButtonState;

	input_state input;
};

global_variable const int defaultWidth = 800;
global_variable const int defaultHeight = 600;

static win32_app_state appState={};

LRESULT CALLBACK processWindowMessages(HWND hWindow, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
		case WM_CLOSE:
		{
			appState.isRunning = false;
		}
		break;

		default:
			return DefWindowProc(hWindow, message, wParam, lParam);
	}
	return 0;
}

#define HANDLE_BUTTON_STATE(dstButton, down) {appState.input.button.dstButton.switchCount++;appState.input.button.dstButton.isDown = down;}

#define HANDLE_MOUSE_BUTTON_STATE(buttonIndex, down) {if(isDown){appState.mouseButtonState|=1<<buttonIndex; SetCapture(appState.mainWindow);}else{appState.mouseButtonState&=~(1<<buttonIndex); if(!appState.mouseButtonState){ReleaseCapture();}}}

void processPendingMessages()
{
	MSG message = { };
	while (PeekMessage(&message, 0, 0, 0, PM_REMOVE))
	{
		switch(message.message)
		{
	//		case WM_MOUSEMOVE:
	//		{
	//			RECT windowRect;
	//			GetWindowRect(appState.mainWindow,&windowRect);
	//			appState.input.mouse.x = (real32)(GET_X_LPARAM(lParam));
	//			appState.input.mouse.y = (real32)(GET_Y_LPARAM(lParam));
	//		}
	//		break;
			case WM_LBUTTONDOWN:
			case WM_LBUTTONUP:
			{
				bool isDown = message.message==WM_LBUTTONDOWN;
				HANDLE_BUTTON_STATE(camera,isDown);

				HANDLE_MOUSE_BUTTON_STATE(0,isDown);
			}
			break;
			case WM_RBUTTONDOWN:
			case WM_RBUTTONUP:
			{
				bool isDown = message.message==WM_RBUTTONDOWN;
				HANDLE_BUTTON_STATE(select,isDown);
				HANDLE_MOUSE_BUTTON_STATE(1,isDown);
			}
			break;
			case WM_MBUTTONDOWN:
			case WM_MBUTTONUP:
			{
				bool isDown = message.message==WM_MBUTTONDOWN;
				HANDLE_MOUSE_BUTTON_STATE(2,isDown);
			}
			break;
			case WM_KEYDOWN:
			case WM_KEYUP:
			{
				bool isDown = message.message==WM_KEYDOWN;
				switch(message.wParam)
				{
					case VK_ESCAPE:
					{
						if(isDown)
						{
							appState.isRunning = false;
						}
					}
					break;
					case 'W':HANDLE_BUTTON_STATE(forward,isDown);break;
					case 'S':HANDLE_BUTTON_STATE(back,isDown);break;
					case 'A':HANDLE_BUTTON_STATE(left,isDown);break;
					case 'D':HANDLE_BUTTON_STATE(right,isDown);break;
					case VK_SPACE:HANDLE_BUTTON_STATE(up,isDown);break;
					case VK_SHIFT:
					case VK_CONTROL:HANDLE_BUTTON_STATE(down,isDown);break;
				}
			}
			break;

			case WM_CLOSE:
			{
				appState.isRunning = false;
			}
			break;

			default:
			{
				TranslateMessage(&message);
				DispatchMessage(&message);
			}
			break;
		}
		ImGui::Animaster::process_event(message.message,message.wParam,message.lParam);
	}
}


void createConsoleWindow()
{
	AllocConsole();
	int outConsole;
	int inConsole;
	HANDLE stdOutHandle;
	HANDLE stdInHandle;
	FILE *outFile;
	FILE *inFile;
	stdOutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	stdInHandle = GetStdHandle(STD_INPUT_HANDLE);
	outConsole = _open_osfhandle((long) stdOutHandle, _O_TEXT);
	inConsole = _open_osfhandle((long) stdInHandle, _O_TEXT);

	outFile = _fdopen(outConsole, "w");
	*stdout = *outFile;
	setvbuf(stdout, NULL, _IONBF, 0);

	inFile = _fdopen(inConsole, "r");
	*stdin = *inFile;
	setvbuf(stdout, NULL, _IONBF, 0);

	HWND consoleWindow = GetConsoleWindow();
	SetWindowPos(consoleWindow, NULL, 0, 0, 0, 0, SWP_NOSIZE);
}

int CALLBACK WinMain(
		_In_ HINSTANCE 	hInstance,
		_In_ HINSTANCE 	hPrevInstance,
		_In_ LPSTR		pCommandLine,
		_In_ int		windowFlags)
{
	//AllocConsole();
	window_state windowState = {};

	createConsoleWindow();

	HANDLE curThread = GetCurrentThread();
	DWORD_PTR affinityMask = 1;

	if(!SetThreadAffinityMask(curThread,affinityMask))
	{
		printf("Couldn't set thread affinity\n");
	}

	const char windowClassName[]  = "AniMasterWindow";
	WNDCLASSEX windowClass={};
	windowClass.cbSize = sizeof(WNDCLASSEX);
	windowClass.style = CS_HREDRAW | CS_VREDRAW;
	windowClass.lpfnWndProc = processWindowMessages;
	windowClass.hInstance = hInstance;
	windowClass.hIcon = LoadIcon(hInstance,MAKEINTRESOURCE(IDI_APPLICATION));
	windowClass.hCursor = LoadCursor(hInstance,IDC_ARROW);
	windowClass.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	windowClass.lpszClassName = windowClassName;
	RegisterClassEx(&windowClass);

	DWORD windowStyle = WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_MAXIMIZE;

	RECT windowRect =
		{ };
	windowRect.left = 200;
	windowRect.top = 200;
	windowRect.right = 200 + defaultWidth;
	windowRect.bottom = 200 + defaultHeight;
	AdjustWindowRect(&windowRect, windowStyle, false);

	appState.mainWindow = CreateWindowEx(
			WS_EX_LEFT,windowClassName,"Window Test",
			windowStyle,
			windowRect.left,windowRect.top,
			windowRect.right - windowRect.left,windowRect.bottom - windowRect.top,
			NULL,NULL,hInstance,NULL);
	if(!appState.mainWindow)
	{
		MessageBoxA(0,"Could not create main window","ERROR",MB_OK);
		return 1;
	}

	appState.mainDeviceContext = GetDC(appState.mainWindow);
	if(!appState.mainDeviceContext)
	{
		MessageBoxA(0,"Could not get device context","ERROR",MB_OK);
		return 1;
	}
	PIXELFORMATDESCRIPTOR bufferFormat = {};
	bufferFormat.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	bufferFormat.nVersion = 1;
	bufferFormat.dwFlags = PFD_DOUBLEBUFFER | PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
	bufferFormat.iPixelType = PFD_TYPE_RGBA;
	bufferFormat.cColorBits = 32;
	bufferFormat.cDepthBits = 24;
	bufferFormat.cStencilBits = 8;

	int pixelFormat = ChoosePixelFormat(appState.mainDeviceContext, &bufferFormat);
	if(!SetPixelFormat(appState.mainDeviceContext,pixelFormat,&bufferFormat))
	{
		MessageBoxA(0,"Could not set pixel format","ERROR",MB_OK);
		return 1;
	}
	appState.mainGlContext = wglCreateContext(appState.mainDeviceContext);

	if(!appState.mainGlContext)
	{
		MessageBoxA(0,"Could not create GL context","ERROR",MB_OK);
		return 1;
	}

	if(!wglMakeCurrent(appState.mainDeviceContext,appState.mainGlContext))
	{
		MessageBoxA(0,"Could not make GL context current","ERROR",MB_OK);
		return 1;
	}
	appState.isRunning = true;
	UpdateWindow(appState.mainWindow);

	LARGE_INTEGER cpuFrequency;
	LARGE_INTEGER prevCpuCounter;
	LARGE_INTEGER curCpuCounter;

	QueryPerformanceFrequency(&cpuFrequency);
	QueryPerformanceCounter(&prevCpuCounter);

	uint64 appMemorySize = Megabytes(512);
	void* appMemoryBuffer = VirtualAlloc(NULL,(size_t)appMemorySize,MEM_COMMIT|MEM_RESERVE,PAGE_READWRITE );

	if(!appMemoryBuffer)
	{
		LPSTR errorMessage;
		FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
				"Could not alloc memory for app",
				GetLastError(),
				LANG_USER_DEFAULT,
				(LPSTR)&errorMessage,
				0,NULL
				);
		MessageBoxA(0, errorMessage,"MEMORY ERROR",MB_OK);
		return 1;
	}

	ImGui::Animaster::init(appState.mainWindow);

	POINT mousePos;
	uint32 targetFramerate = 60;
	uint32 targetFrameDuration = 1000/targetFramerate;
	static PAINTSTRUCT windowPaint;
	while(appState.isRunning)
	{
		appState.input.resetSwitchCounts();
		appState.input.mouse.prevX = appState.input.mouse.x;
		appState.input.mouse.prevY = appState.input.mouse.y;
		processPendingMessages();
		ImGui::Animaster::update(appState.mainWindow,_vec2f(appState.input.mouse.x,appState.input.mouse.y),(float)(targetFrameDuration/1000.0f));

		RECT windowRect = {};
		GetClientRect(appState.mainWindow,&windowRect);

		windowState.x = windowRect.left;
		windowState.y = windowRect.top;
		windowState.width  = windowRect.right - windowRect.left;
		windowState.height = windowRect.bottom - windowRect.top;

		GetCursorPos(&mousePos);
		ScreenToClient(appState.mainWindow, &mousePos);

		appState.input.mouse.x = (real32) mousePos.x;
		appState.input.mouse.y = (real32) mousePos.y;

		UpdateAnimaster(appMemoryBuffer,appMemorySize,windowState,&appState.input,targetFrameDuration);

		ImGui::Animaster::onFrameEnd(appState.mainWindow,mousePos.x,mousePos.y);

		glFinish();
		SwapBuffers(appState.mainDeviceContext);
		BeginPaint(appState.mainWindow,&windowPaint);
		EndPaint(appState.mainWindow,&windowPaint);

		QueryPerformanceCounter(&curCpuCounter);

		uint64 cpuCounterDif = curCpuCounter.QuadPart - prevCpuCounter.QuadPart;
		prevCpuCounter = curCpuCounter;

		uint64 frameDurationMs = cpuCounterDif / (cpuFrequency.QuadPart/1000);
		if(frameDurationMs<targetFrameDuration)
		{
			Sleep((DWORD)(targetFrameDuration - frameDurationMs));
		}
	}


	wglMakeCurrent(NULL,NULL);
	wglDeleteContext(appState.mainGlContext);
	ReleaseDC(appState.mainWindow,appState.mainDeviceContext);
	DestroyWindow(appState.mainWindow);
	return 0;
}


