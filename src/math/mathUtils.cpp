/*
 * lbBaseMath.cpp
 *
 *  Created on: 12-04-2011
 *      Author: Spliter
 */

#include "mathUtils.h"

real32 lerpDeg(real32 start,real32 end,real32 amt)
{
	real32 dif=end-start;
	dif = fmodf(dif,360);
	if(dif>180.0)
	{
		dif-=360.0;
	}
	else if (dif<-180.0)
	{
		dif+=360.0;
	}
	return start+dif*amt;
}

real32 lerpRad(real32 start,real32 end,real32 amt)
{
	real32 PI2 = (real32)(M_PI*2);
	real32 dif=end-start;
	dif = fmodf(dif,PI2);
	if(dif>M_PI)
	{
		dif-=PI2;
	}
	else if (dif<-M_PI)
	{
		dif+=PI2;
	}
	return start+dif*amt;
}

real32 direction(real32 x,real32 y)
{
	return atan2(y,x);
}

real32 directionDeg(real32 x,real32 y)
{
	return (real32)(atan2(y,x)*RAD_TO_DEG_CONST);
}

real32 lengthDirX(real32 deg,real32 length)
{
	return (real32)(cos(deg*DEG_TO_RAD_CONST)*length);
}

real32 lengthDirY(real32 deg,real32 length)
{
	return (real32)(sin(deg*DEG_TO_RAD_CONST)*length);
}

real32 angleDifDeg(real32 start, real32 end)
{
	real32 dif = end - start;
	dif = fmodf(dif,360);
	while (dif < -180) dif += 360;
	while (dif > 180) dif -= 360;

	return dif;
}

real32 angleDifRad(real32 start, real32 end)
{
	real32 PI2 = (real32)(M_PI*2);
	real32 dif=end-start;
	dif = fmodf(dif,PI2);
	while(dif<-M_PI)dif+=PI2;
	while(dif>M_PI)dif-=PI2;
	return dif;
}
