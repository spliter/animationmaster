/*
 * lbBaseMath.h
 *
 *  Created on: 11-04-2011
 *      Author: Spliter
 */

#ifndef LBBASEMATH_H_
#define LBBASEMATH_H_
#define _USE_MATH_DEFINES
#include <math.h>
#include "../shared/types.h"

#define DEG_TO_RAD_CONST 0.0174532925199432957692369076848
#define RAD_TO_DEG_CONST 57.295779513082320876798154814105

inline real32 degToRad(real32 deg){return (real32)(deg*DEG_TO_RAD_CONST);}
inline real32 radToDeg(real32 rad){return (real32)(rad*RAD_TO_DEG_CONST);}
inline real32 sqr(real32 x){return x*x;}
inline real32 slowdown(real32 x){return (2-x)*x;}
inline real32 speedup(real32 x){return x*x;}
inline real32 smoothstep(real32 x){return x*x*(3-2*x);}
inline real32 smoothstep(real32 edge1, real32 edge2, real32 ratio){real32 x=(ratio-edge1)/(edge2-edge1);return smoothstep(x)*(edge2-edge1)+edge1;}
inline real32 smoothstepN(real32 edge1, real32 edge2, real32 ratio){real32 x=(ratio-edge1)/(edge2-edge1);return smoothstep(x);}
inline real32 lerp(real32 start,real32 end,real32 amt){return start+(end-start)*amt;}
inline int32  lerpi(int32 start,int32 end,real32 amt){return (int32)(start+(end-start)*amt);}
inline real32 trilerp(real32 a,real32 b,real32 c,real32 u,real32 v){return (a-c)*u+(b-c)*v+c;}
inline bool32 inRange(real32 start,real32 end,real32 x){return x>start && x<end;}
inline real32 clamp(real32 min,real32 max,real32 x){real32 aux=(x>min?x:min);return (aux<max?aux:max);}
inline int32 clampi(int32 min,int32 max,int32 x){int32 aux=(x>min?x:min);return (aux<max?aux:max);}
inline real32 normalize(real32 start,real32 end,real32 x){return clamp(0,1,(x-start)/(end-start));}
inline real32 normalizei(int32 start,int32 end,int32 x){return clamp(0,1,(x-start)/(real32)(end-start));}
inline real32 distance(real32 x1,real32 y1,real32 x2,real32 y2){return sqrt(sqr(x2-x1)+sqr(y2-y1));}
inline int32 sign(real32 x){return (x > 0) ? 1 : ((x < 0) ? -1 : 0);}
inline real32 maxf(real32 a,real32 b) {return (a)>(b)?(a):(b);}
inline real32 minf(real32 a,real32 b) {return (a)<(b)?(a):(b);}
inline int32 maxi(int32 a,int32 b) {return (a)>(b)?(a):(b);}
inline int32 mini(int32 a,int32 b) {return (a)<(b)?(a):(b);}

real32 lerpDeg(real32 start,real32 end,real32 amt);
real32 lerpRad(real32 start,real32 end,real32 amt);

real32 direction(real32 x,real32 y);
real32 directionDeg(real32 x,real32 y);

real32 lengthDirX(real32 deg,real32 length);
real32 lengthDirY(real32 deg,real32 length);

real32 angleDifRad(real32 start, real32 end);//[-PI,+PI]
real32 angleDifDeg(real32 start, real32 end);//[-180,+180]

#endif /* LBBASEMATH_H_ */
