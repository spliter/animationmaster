/*
 * lbVec2f.h
 *
 *  Created on: 11-04-2011
 *      Author: Spliter
 */

#ifndef LBVEC2F_H_
#define LBVEC2F_H_
#include "mathUtils.h"
#include <float.h>

struct vec2f
{
public:
	union
	{
		real32 me[2];
		struct
		{
			real32 x;
			real32 y;
		};
	};

	vec2f()
	{
	}

	vec2f(const vec2f& other)
	{
		x = other.x;
		y = other.y;
	}

	vec2f(real32 val)
	{
		x = val;
		y = val;
	}

	vec2f(real32 x, real32 y)
	{
		this->x = x;
		this->y = y;
	}

	vec2f& set(real32 _x,real32 _y);
	vec2f& set(const vec2f& b);
	vec2f& set(real32 _x);

	real32 length() const;
	real32 lengthSquared() const;
	real32 direction() const;
	real32 directionDeg() const;

	vec2f& normalize();
	vec2f normalized() const;

	vec2f& makePositiveAnglePerpendicular();
	vec2f getPositiveAnglePerpendicular() const;
	vec2f& makeNegativeAnglePerpendicular();
	vec2f getNegativeAnglePerpendicular() const;

	vec2f& scale(real32 scale);
	vec2f scaled(real32 scale) const;

	vec2f& stretch(real32 newLength);
	vec2f stretched(real32 newLength) const;


	real32 dot(const vec2f& b) const;
	real32 cross(const vec2f& b) const;

	vec2f& rotate(real32 rad);
	vec2f rotated(real32 rad) const;

	vec2f& rotateDeg(real32 deg);
	vec2f rotatedDeg(real32 deg) const;

	vec2f& reflect(const vec2f& n);
	vec2f reflected(const vec2f& n) const;

	//piecewise operators:

	bool operator==(const vec2f& rh) const;
	bool operator!=(const vec2f& rh) const;

	vec2f& operator*=(const vec2f& rh);
	vec2f& operator/=(const vec2f& rh);
	vec2f& operator+=(const vec2f& rh);
	vec2f& operator-=(const vec2f& rh);

	vec2f operator*(const vec2f& rh) const;
	vec2f operator/(const vec2f& rh) const;
	vec2f operator+(const vec2f& rh) const;
	vec2f operator-(const vec2f& rh) const;

	vec2f operator-() const;

	//real32 operators:

	vec2f& operator=(real32 rh);
	vec2f& operator*=(real32 rh);
	vec2f& operator/=(real32 rh);

	vec2f operator*(real32 rh) const;
	vec2f operator/(real32 rh) const;

	real32 operator[](uint32 index);
};

vec2f operator*(const real32& lh,const vec2f& rh);
vec2f operator/(const real32& lh,const vec2f& rh);
inline real32 getAngle(const vec2f& from, const vec2f& to){return acosf(from.dot(to));}

inline static vec2f _vec2f(real32 x, real32 y)
{
	vec2f result;
	result.set(x,y);
	return result;
}

inline vec2f& vec2f::set(real32 _x, real32 _y)
{
	x = _x;
	y = _y;
	return *this;
}

inline vec2f& vec2f::set(const vec2f& b)
{
	x = b.x;
	y = b.y;
	return *this;
}

inline vec2f& vec2f::set(real32 _x)
{
	x = _x;
	y = _x;

	return *this;
}

real32 vec2f::length() const
{
	return sqrt(sqr(x) + sqr(y));
}

real32 vec2f::lengthSquared() const
{
	return sqr(x) + sqr(y);
}

real32 vec2f::direction() const
{
	return atan2(y, x);
}
real32 vec2f::directionDeg() const
{
	return (real32) (atan2(y, x) * RAD_TO_DEG_CONST);
}

vec2f& vec2f::normalize()
{
	real32 d = length();
	if (d < FLT_EPSILON)
	{
		x = 1.0;
		y = 0.0;
	}
	else
	{
		x = x / d;
		y = y / d;
	}
	return *this;
}
vec2f vec2f::normalized() const
{
	real32 d = length();
	if (d < FLT_EPSILON)
	{
		vec2f result = {1.0, 0.0};
		return result;
	}
	else
	{
		vec2f result = {x / d, y / d};
		return result;
	}
}

vec2f& vec2f::makePositiveAnglePerpendicular()
{
	real32 aux = x;
	x = y;
	y = -aux;
	return *this;
}
vec2f vec2f::getPositiveAnglePerpendicular() const
{
	vec2f result = {y, -x};
	return result;
}

vec2f& vec2f::makeNegativeAnglePerpendicular()
{
	real32 aux = x;
	x = -y;
	y = aux;
	return *this;
}

vec2f vec2f::getNegativeAnglePerpendicular() const
{
	vec2f result = {-y, x};
	return result;
}

vec2f& vec2f::scale(real32 scale)
{
	x *= scale;
	y *= scale;
	return *this;
}
vec2f vec2f::scaled(real32 scale) const
					{
	vec2f result = {x * scale, y * scale};
	return result;
}

vec2f& vec2f::stretch(real32 newLength)
{
	normalize();
	x *= newLength;
	y *= newLength;
	return *this;
}

vec2f vec2f::stretched(real32 newLength) const
						{
	real32 d = length();
	if (d < FLT_EPSILON)
	{
		vec2f result = {newLength, 0.0};
		return result;
	}
	else
	{
		vec2f result = {x / d * newLength, y / d * newLength};
		return result;
	}
}

real32 vec2f::dot(const vec2f& b) const
					{
	return x * b.x + y * b.y;
}

real32 vec2f::cross(const vec2f& b) const
					{
	return x * b.y - y * b.x;
}

vec2f& vec2f::rotate(real32 rad)
{
	real32 c = cos(rad);
	real32 s = sin(rad);
	real32 ax = x;
	x = c * x - s * y;
	y = c * y + s * ax;
	return *this;
}
vec2f vec2f::rotated(real32 rad) const
						{
	real32 c = cos(rad);
	real32 s = sin(rad);
	vec2f result = {c * x - s * y, c * y + s * x};
	return result;
}

vec2f& vec2f::rotateDeg(real32 deg)
{
	return rotate((real32) (deg * DEG_TO_RAD_CONST));
}
vec2f vec2f::rotatedDeg(real32 deg) const
						{

	return rotated((real32) (deg * DEG_TO_RAD_CONST));
}
vec2f& vec2f::reflect(const vec2f& n)
{
	real32 d = 2 * dot(n);
	(*this) -= d * n;
	return *this;
}
vec2f vec2f::reflected(const vec2f& n) const
						{
	real32 d = 2 * dot(n);
	vec2f result = {x - d * n.x, y - d * n.y};
	return result;
}

//piecewise operators:
vec2f& vec2f::operator*=(const vec2f& rh)
{
	x *= rh.x;
	y *= rh.y;
	return *this;
}
vec2f& vec2f::operator/=(const vec2f& rh)
{
	x /= rh.x;
	y /= rh.y;
	return *this;
}
vec2f& vec2f::operator+=(const vec2f& rh)
{
	x += rh.x;
	y += rh.y;
	return *this;
}
vec2f& vec2f::operator-=(const vec2f& rh)
{
	x -= rh.x;
	y -= rh.y;
	return *this;
}

vec2f vec2f::operator*(const vec2f& rh) const
						{
	vec2f result = {x * rh.x, y * rh.y};
	return result;
}

vec2f vec2f::operator/(const vec2f& rh) const
						{
	vec2f result = {x / rh.x, y / rh.y};
	return result;
}
vec2f vec2f::operator+(const vec2f& rh) const
						{
	vec2f result = {x + rh.x, y + rh.y};
	return result;
}
vec2f vec2f::operator-(const vec2f& rh) const
						{
	vec2f result = {x - rh.x, y - rh.y};
	return result;
}

vec2f vec2f::operator-() const
{
	vec2f result = {-x, -y};
	return result;
}

vec2f& vec2f::operator=(real32 rh)
{
	x = rh;
	y = rh;
	return *this;
}

vec2f& vec2f::operator*=(real32 rh)
{
	x *= rh;
	y *= rh;
	return *this;
}

vec2f& vec2f::operator/=(real32 rh)
{
	x /= rh;
	y /= rh;
	return *this;
}

vec2f vec2f::operator*(real32 rh) const
						{
	vec2f result = {x * rh, y * rh};
	return result;
}
vec2f vec2f::operator/(real32 rh) const
						{
	vec2f result = {x / rh, y / rh};
	return result;
}

real32 vec2f::operator[](uint32 index)
{
	return me[index];
}

vec2f operator*(const real32& lh, const vec2f& rh)
{
	vec2f result = {lh * rh.x, lh * rh.y};
	return result;
}
vec2f operator/(const real32& lh, const vec2f& rh)
{
	vec2f result = {lh / rh.x, lh / rh.y};
	return result;
}


#endif /* LBVEC2F_H_ */
