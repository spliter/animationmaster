/*
 * vecMathUtils.cpp
 *
 *  Created on: 4 Apr 2017
 *      Author: MikoKuta
 */

#include "vec_math_utils.h"

vec2f barycentricToCartesian(const vec3f &uvw, const vec2f &a, const vec2f &b, const vec2f &c)
{
	vec2f result = { };
	result = uvw.x * a + uvw.y * b + uvw.z * c;
	return result;
}

vec3f cartesianToBarycentric(vec2f p, const vec2f &a, const vec2f &b, const vec2f &c)
{
	vec3f result = { };

	vec2f v0 = b - a, v1 = c - a, v2 = c - b;

	vec2f v0Normal = v0.getPositiveAnglePerpendicular().normalize();
	vec2f v1Normal = -v1.getPositiveAnglePerpendicular().normalize();
	vec2f v2Normal = v2.getPositiveAnglePerpendicular().normalize();

	if (v0.cross(v1) < 0)
	{
		v0Normal *= -1.0f;
		v1Normal *= -1.0f;
		v2Normal *= -1.0f;
	}

	float normalDot = v0Normal.dot(p - a);
	if (normalDot > 0)
	{
		p -= v0Normal * normalDot;
	}

	normalDot = v1Normal.dot(p - a);
	if (normalDot > 0)
	{
		p -= v1Normal * normalDot;
	}

	normalDot = v2Normal.dot(p - c);
	if (normalDot > 0)
	{
		p -= v2Normal * normalDot;
	}

	vec2f vdif = p - a;

	float d00 = v0.dot(v0);
	float d01 = v0.dot(v1);
	float d11 = v1.dot(v1);
	float d20 = vdif.dot(v0);
	float d21 = vdif.dot(v1);

	real32 denomInv = 1.0f / (d00 * d11 - d01 * d01);

	result.y = (d11 * d20 - d01 * d21) * denomInv;
	result.z = (d00 * d21 - d01 * d20) * denomInv;

	result.y = clamp(0.0f, 1.0f, result.y);
	result.z = clamp(0.0f, 1.0f, result.z);
	if (result.y + result.z > 1.0f)
	{
		real32 totalLength = result.y + result.z;
		result.y /= totalLength;
		result.z /= totalLength;
	}

	result.x = 1.0f - result.y - result.z;

	return result;
}


bool32 closestPointsOnLine(const vec3f &lineStartA, const vec3f &lineDirA, const vec3f &lineStartB, const vec3f &lineDirB, vec3f& outClosestPointOnA, vec3f& outClosestPointOnB, real32 &outClosestTimeOnA, real32 &outClosestTimeOnB)
{
	real32 dirAonAProjection = lineDirA.dot(lineDirA);
	real32 dirAonBProjection = lineDirA.dot(lineDirB);
	real32 dirBonBProjection = lineDirB.dot(lineDirB);

	real32 dif = dirAonAProjection*dirBonBProjection - dirAonBProjection * dirAonBProjection;

	if(dif>FLT_EPSILON)
	{
		vec3f startDif = lineStartA - lineStartB;
		real32 startDifOnAProj = lineDirA.dot(startDif);
		real32 startDifOnBProj = lineDirB.dot(startDif);

		real32 lineAClosestTime = (dirAonBProjection*startDifOnBProj - dirBonBProjection * startDifOnAProj) / dif;
		real32 lineBClosestTime = (dirAonAProjection*startDifOnBProj - dirAonBProjection * startDifOnAProj) / dif;

		outClosestPointOnA = lineStartA+lineDirA*lineAClosestTime;
		outClosestPointOnB = lineStartB+lineDirB*lineBClosestTime;
		outClosestTimeOnA = lineAClosestTime;
		outClosestTimeOnB = lineBClosestTime;
		return true;
	}
	else
	{
		outClosestPointOnA = lineStartA;
		outClosestPointOnB = lineStartB;
		outClosestTimeOnA = 0.0f;
		outClosestTimeOnB = 0.0f;
		return false;
	}
}

