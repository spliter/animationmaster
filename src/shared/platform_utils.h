/*
 * platform_utils.h
 *
 *  Created on: 18 Mar 2017
 *      Author: MikoKuta
 */

#pragma once

#include "types.h"

uint64 getCpuFrequency();//Returns the CPU ticks frequency, if not supported uses 1000 to give millisecond frequency
uint64 getTicks();//get Ticks, whatever the direct measurement of CPU is, if not supported, uses milliseconds
uint64 getMicroseconds();//microseconds since launch, returns getMilliseconds()*1000 if microsecond precision is not supported
uint64 getMilliseconds();//milliseconds since launch
